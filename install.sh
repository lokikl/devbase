#!/usr/bin/env bash
set -e

INSTALL_PATH=/usr/local/bin/devbase

if [[ ! -f $INSTALL_PATH ]]; then
  echo "Sym link to $INSTALL_PATH"
  sudo ln -s `pwd`/devbase $INSTALL_PATH
else
  echo "Already installed"
fi
