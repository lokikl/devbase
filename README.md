Checkout this repository

`./install.sh`

`mkdir myapp1 && cd myapp1`

`vi devbase.conf`

```
project_name: myapp1
image_tag: myapp1
dev_port: 18080
framework: sinatra
sinatra_options:         <optional>
  gendb: true
  identity: true
  mongodb: true
  mongodb_express_port: 7011
assets: true
```

`devbase`

Done! Please follow the project README to bootstrap the project.


Todo
----

Separate vendor.js into a new webpack child to not re-generate on changes. (pointup)
