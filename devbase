#!/usr/bin/env ruby

require 'ostruct'
require 'erb'
require 'yaml'
require 'fileutils'

if `which direnv` == ''
  puts "CLI direnv is required but missing, aborted."
  exit 1
end

script_path = __FILE__
$script_dir = File.dirname(File.readlink(script_path))

if ARGV[0] == 'init'
  if File.file?('devbase.conf')
    puts "devbase.conf already exists."
    exit 0
  end
  project_name = File.basename(Dir.pwd).gsub('-', '_')
  conf_script = File.read(File.join($script_dir, 'devbase.conf.example'))
  conf_script.gsub!('<your-app-name>', project_name)
  File.write('devbase.conf', conf_script)
  puts "Please edit devbase.conf and run devbase to bootstrap the project"
  exit 0

elsif ARGV[0] === 'clear'
  `ls -a1 | grep -vF 'devbase.conf' | sort | tail -n +3 | xargs rm -rf`
  exit 0
end

unless File.file?('devbase.conf')
  puts "devbase.conf expected but not found, may consider running devbase init"
  exit 0
end

$config = YAML.load(File.read('devbase.conf'))
$config['volumes'] ||= []

if ARGV[0] == 'cp'
  if ARGV[1].to_s == 'all-staged'
    cmd = "git diff --name-only --staged | xargs -d'\n' -I% devbase cp '%'"
    system(cmd)
    exit 0
  end

  if ARGV[1].to_s == ''
    puts "Usage: devbase cp src/uid/global.scss"
    puts "Usage: devbase cp all-staged"
    exit -1
  end
  src_path = ARGV[1]
  unless File.file?(src_path)
    puts "File do not exists"
    exit -1
  end
  dest_path = "#{$script_dir}/frameworks/#{$config['framework']}"
  puts "Copy file #{src_path} to #{dest_path}"

  content = File.read(src_path).gsub($config['project_name'], '<%= project_name %>')
  File.write("#{dest_path}/#{src_path}", content)
  # FileUtils.cp(src_path, "#{dest_path}/#{src_path}")
  puts "Done"
  exit 0
end

class ErbBinding < OpenStruct
  def get_binding; binding; end
end

def render(path, data={})
  template = File.read(File.join($script_dir, 'templates', path))
  ERB.new(template, 0, '-').result(ErbBinding.new(data).send(:get_binding))
end

$is_first_time = !File.file?('.envrc')

def process_folder(src, dest)
  `mkdir -p #{dest}`
  Dir.foreach(src) { |item|
    next if item == '.' or item == '..'
    src_path = File.join(src, item)
    dest_path = File.join(dest, item)
    if File.directory?(src_path)
      process_folder(src_path, dest_path)
    else
      template = File.read(src_path)
      metadata =
        template.scan(/devbase: .*\n/).each_with_object({}) { |line, h|
          key, value = line.split(':')[-1].split('=').map(&:strip)
          h[key] = value
        } rescue {} # can be image

      next if !$is_first_time && metadata['ensure'] == 'on-bootstrap'

      if File.file?(dest_path) # already exists
        if metadata['ensure'] == 'always' # need to override
          if File.read(dest_path).include?("devbase: ensure=never") # ensure=never found
            next
          end
        else
          next
        end
      end

      puts dest_path
      if dest_path.end_with?("woff") || dest_path.end_with?("gz")
        FileUtils.cp(src_path, dest_path)
      else
        File.write(
          dest_path,
          ERB.new(template, 0, '-').result(ErbBinding.new($config).send(:get_binding))
        )
      end
      if metadata['executable'] == 'yes'
        `chmod +x "#{dest_path}"`
      end
    end
  }
end

src_folder = "#{$script_dir}/frameworks/#{$config['framework']}"
process_folder(src_folder, '.')

if File.file?('.envrc')
  `direnv allow .`
end

unless File.directory?('.git')
  puts "Source Control with git"
  `git init && git add -A && git commit -m "devbase initialized"`
end
