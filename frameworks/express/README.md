<%= project_name %>
<%= project_name.gsub(/./, '=') %>

Install direnv to source the development environment, and allow for this project

```
apt-get install direnv
direnv allow .
```

Source the vim settings, add in your VimRC

```
au VimEnter * call LoadProjectVimRC()
function! LoadProjectVimRC()
  if filereadable(".devbase/project.vim")
    so .devbase/project.vim
  endif
endfunction
```

Start the app with `reboot-dev` or `<leader>r` in Vim

Restart the app again with `reboot-dev` or `<leader>r` in Vim

In Vim, `sl` to view logs.

`build.prod` to build the production image.

`cleanup-all-images` to clean up both dev. & prod. docker images.


Database
========

In Vim, `db` to open the PostgreSQL console.

`co` to open app console.

`yarn knex migrate:make create_user_table` to create a new migration.

`yarn knex migrate:latest` to run migrations.

`yarn knex migrate:rollback` to rollback migrations.

`yarn knex seed:run` to run seed data.

refs: https://devhints.io/knex
refs: https://devhints.io/express
