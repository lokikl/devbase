" devbase: ensure=always
set backupcopy=yes

let proj = $COMPOSE_PROJECT_NAME

nnoremap <leader>r :call TmuxPopup("reboot-dev; any-key")<cr>
nnoremap <leader>o :call system("xdg-open https://${COMPOSE_PROJECT_NAME}-dev.com")<cr>
nnoremap <leader>oi :call TmuxPopup("open-service-in-browser inbucket 9000")<cr>
nnoremap <leader>ou :call TmuxPopup("open-service-in-browser uid 3000")<cr>
nnoremap <leader>or :call system("touch dist/index.js")<cr>
nnoremap <leader>od :call system("touch src/index.dev-server.js")<cr>

nnoremap co :call TmuxSplit("console app")<cr>
nnoremap cb :call TmuxSplit("console hc-builder")<cr>
nnoremap dr :call TmuxPopup("docker-compose exec app /app/.devbase/bin/resetdb; any-key")<cr>

augroup lang_utilities
  autocmd!
  au FileType javascript nnoremap <silent> ss :lua send_sql_under_cursor("execute")<cr>
  au FileType javascript nnoremap <silent> SS :lua send_sql_under_cursor("benchmark")<cr>
  au FileType javascript nnoremap <silent> SW :call InspectSQLTable()<cr>
  au FileType javascript nnoremap <silent> sq :call FormatKnexSQL()<cr>
  " <leader>em to extract selected lines (js functions) to js module
  au FileType javascript vnoremap <leader>em <esc>:call ExtractJSModule()<cr>
  au BufWritePre *.rs lua vim.lsp.buf.formatting_sync(nil, 200)
augroup end

function! RunLastReq(args)
  call system("run-last-req " . expand("%:t") . " " . line('.') . " " . a:args)
endfunction

function! InspectSQLTable()
  let word = expand("<cword>")
  call system("paste-sql-to-next-tmux-pane inspect-table " . word)
endfunction

function! FormatKnexSQL()
  let l:view = winsaveview()
  /`
  let lno2 = line('.')
  ?`
  let lno1 = line('.')
  if lno2 == 0
    let lno2 = line('$')
  endif
  let ind=indent(lno1 + 1)
  execute ":" . lno1 . "," . lno2 . "!xclip -sel clip"
  let formated=system("format-copied-sql " . ind)
  execute "normal uj" . (lno2 - lno1 - 1) . "dd"
  call append(lno1, split(formated, "\n"))
  call winrestview(l:view)
endfunction

function! ExtractJSModule()
  let fullpath = expand('%:h') . '/'
  let newpath = input('Extract selected js functions to a new module: ', fullpath)
  if newpath != fullpath
    if newpath !~ '\.js$'
      let newpath = newpath . ".js"
    endif
    let i = indent(line("'<"))
    execute "'<,'>s/^ \\{" . i . "\\}//"
    execute "'<,'>w " . newpath
    execute "'<,'>d"
    let formated = system("refactor-js.rb extract-module " . expand('%') . " " . newpath)
    call append(line('.') - 1, split(formated, "\n"))
    execute "normal kV="
    execute 'w'
    execute 'tabedit ' . newpath
  endif
  redraw!
endfunction

function! TmuxSplit(subcmd, ...)
  let tmuxArg = get(a:, 1, "")
  let cmd = "source .envrc; echo " . a:subcmd . "; " . a:subcmd
  let cmd = "tmux splitw " . tmuxArg . " \"zsh -c '" . cmd . "'\""
  call system(cmd)
endfunction

function! ConsoleCtl(cmd)
  call system("console-ctl '" . a:cmd . "'")
endfunction

" consumed by dev-server, with the nvim remote server protocol
function! GracefulReload()
  let l:view = winsaveview()
  silent execute 'e'
  call winrestview(l:view)
endfunction
