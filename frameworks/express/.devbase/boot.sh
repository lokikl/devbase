#!/bin/bash
# devbase: ensure=always
# devbase: executable=yes

# SIGTERM-handler
term_handler() {
  if [ $pid -ne 0 ]; then
    kill -SIGTERM "$pid"
    wait "$pid"
  fi
  exit 143; # 128 + 15 -- SIGTERM
}

# setup handlers
trap 'kill ${!}; term_handler' SIGTERM

# run application
if [[ "$APP_ENV" == "development" ]]; then
  echo "=== yarn install ============================================================================================="
  yarn install

  echo "=== yarn run node-sass ======================================================================================="
  yarn run node-sass --output-style=compressed --source-map=true src/assets/scss/app.scss dist/public/app.css
  yarn run node-sass --output-style=compressed --source-map=true src/assets/scss/app.scss dist/public/app.css -w &

  echo "=== yarn run nodemon (dev-server) ============================================================================"
  LOG="dev-server" yarn run nodemon \
    -w src/index.dev-server.js \
    --exec 'yarn run babel src -d dist && yarn node dist/index.dev-server.js' &

  echo "=== wait until dev-server is up =============================================================================="
  until $(curl --output /dev/null --silent --head --fail http://127.0.0.1:3001); do
    printf '.'
    sleep 1
  done

  echo "=== concat uid pugs when changed ============================================================================="
  yarn run nodemon \
    -q \
    -i src/uid/includes.pug \
    -i src/uid/client-includes.pug \
    -e pug \
    -w src/uid \
    --exec "./.devbase/bin/concat-uid-mixins.sh" &

  echo "=== yarn run webpack ========================================================================================="
  yarn run nodemon \
    -w webpack.config.js \
    --exec 'yarn run webpack -w --mode=development --devtool=inline-source-map' &

  echo "=== yarn run nodemon (express) ================================================================================"
  # re-caching uid mixins is slow, so I splitted auto-reload into two flows
  #  1. JS changes: supported in src/index.js with chokidar
  #  2. UID changes: supported by an API in src/routes/devbase.js
  LOG="express" yarn run nodemon \
    -d 0.1 \
    -e js,pug,json \
    -w /webfront/uid \
    -w dist/index.js \
    -w src/views \
    --exec "./.devbase/boot-express.sh || touch dist/index.js" &

else
  if [[ "$1" = "" ]]; then
    node dist/index.js &
  else
    node "dist/index.${1}.js" &
  fi
fi
pid="$!"

# wait forever
while true; do
  tail -f /dev/null & wait ${!}
done

