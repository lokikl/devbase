#!/usr/bin/env bash

# devbase: ensure=always
# devbase: executable=yes

echo "Mixins"
for file in `ls -1 src/uid/mixins/*.pug`; do
  mixinName=`basename "${file}" '.pug'`
  foundCount=`grep -R --exclude=${mixinName}.pug --exclude=includes.pug --exclude=client-includes.pug $mixinName 'src/' | wc -l`
  if [[ $foundCount -lt 1 ]]; then
    echo "  $mixinName $foundCount"
  fi
done

echo ''
echo "NAF modules"
for file in `ls -1 src/frontend/app/*.js`; do
  modName=`basename "${file}" '.js'`
  ccName=`echo $modName | sed -r 's/(^|[-_ ]+)([0-9a-z])/\U\2/g' | awk '{print tolower(substr($0,0,1)) substr($0,2)}'`
  foundCount=`grep -R $ccName 'src/uid/mixins/' | wc -l`
  if [[ $foundCount -lt 1 ]]; then
    echo "  $modName $ccName $foundCount"
  fi
done

