#!/usr/bin/env bash
# devbase: ensure=always
# devbase: executable=yes

errln=$(
  docker logs --tail=100 ${COMPOSE_PROJECT_NAME}_app_1 2>&1 \
  | tac | grep -n 'BABEL_PARSE_ERROR' \
  | head -n1 \
  | cut -d':' -f1
)

if [[ "$errln" = "" ]]; then
  exit 0
fi

goodln=$(
  docker logs --tail=100 ${COMPOSE_PROJECT_NAME}_app_1 2>&1 \
  | tac | grep -n 'devbase/boot-express.sh' \
  | head -n1 \
  | cut -d':' -f1
)

if [[ "$goodln" != "" ]]; then
  if [ $goodln -lt $errln ]; then
    exit 0
  fi
fi

# remove the file with syntax error to make sure can't be overlooked
filename=$(
  docker logs --tail=100 ${COMPOSE_PROJECT_NAME}_app_1 2>&1 \
  | tac | grep 'SyntaxError' \
  | head -n1 \
  | cut -d':' -f2 \
  | sed -e 's/\/app\/src/\/app\/dist/'
)
if [ -f ${filename} ]; then
  rm $filename
fi

# confirmed current codebase has syntax error
curl -s -XPOST "http://127.0.0.1:3000/_/babel-syntax-error"
curl -s "http://127.0.0.1:3001/dev-server?cmd=reload&reason=Detected%20syntax%20error..." > /dev/null
