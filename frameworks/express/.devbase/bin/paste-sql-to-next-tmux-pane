#!/usr/bin/env ruby

# devbase: ensure=always
# devbase: executable=yes

require 'yaml'

cmd = ARGV[0]

ARGV.shift

query = if cmd == 'inspect-table'
  <<-QUERY
  -- expand
  select * from #{ARGV[0]} limit 1
  QUERY
else
  # get query from clipboard, write by vim shortcut in project.vim
  (cmd == 'benchmark' ? ' explain analyze' : '') +
  ARGF.read
    .gsub('\`', '!!')
    .split('`')[1]
    .gsub('\\', '')
end

if query.include?("curl")
  system(query.gsub("\n", " "))
  exit 0
end

query = query.split("-- stop")[0]

# replace variables with sql comments
replaces = query
  .lines
  .select { |line| line.strip.start_with?("--") && line.include?(":") }
  .map(&:strip)
  .map { |line|
    a, b = line.split(":", 2)
    [a.split(' ')[1].strip, b.strip]
  }
  .map { |left, right|
    query.gsub!(":#{left}:", right)
    query.gsub!(":#{left}", right)
  }

# get the sql pane
pane = `tmux list-panes | grep -v active | head -n1`.split(":")[0]

# toggle expand or not by comment
if query.include?("-- expand-mysql")
  query = "#{query} \\G"
elsif query.include?("-- expand")
  query = "#{query} \\gx"
else
  query = "#{query} \\;"
end

query = query
  .gsub('!!', '`')
  .gsub(%Q['], %Q['\\\\''])

cli = %Q[tmux send-keys -t #{pane} C-c '#{query}' C-l Enter]

# paste to the psql console
system(cli)
