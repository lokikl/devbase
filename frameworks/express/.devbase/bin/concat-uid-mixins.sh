#!/usr/bin/env bash
# devbase: ensure=always
# devbase: executable=yes

curl -s "http://127.0.0.1:3001/dev-server?cmd=freeze&reason=Compiling%20views..." > /dev/null

md5Server=`md5sum src/uid/includes.pug | cut -d' ' -f1`
md5Client=`md5sum src/uid/client-includes.pug | cut -d' ' -f1`

sed -e '$s/$/\n/' \
  -e '/^ *\/\//d' \
  -s src/uid/global.pug \
  src/uid/system.pug \
  src/uid/mixins/*.pug \
  > /tmp/includes.pug

sed -e '$s/$/\n/' \
  -e '/^ *\/\//d' \
  -s src/uid/global.pug \
  src/uid/system.pug \
  `grep -lF '//- expose' src/uid/mixins/*.pug` \
  > /tmp/client-includes.pug

echo '+#{mixin}(args)' >> /tmp/client-includes.pug

md5ClientNew=`md5sum /tmp/client-includes.pug | cut -d' ' -f1`
md5ServerNew=`md5sum /tmp/includes.pug | cut -d' ' -f1`

if [[ "$md5Client" != "$md5ClientNew" ]]; then
  mv /tmp/client-includes.pug src/uid/client-includes.pug
fi

if [[ "$md5Server" != "$md5ServerNew" ]]; then
  mv /tmp/includes.pug src/uid/includes.pug
fi

if [[ "$md5Server" != "$md5ServerNew" ]] || [[ "$md5Client" != "$md5ClientNew" ]]; then
  curl -s "http://127.0.0.1:3000/_/uid-invalidate/back" > /dev/null
else
  curl -s "http://127.0.0.1:3001/dev-server?cmd=unfreeze&reason=Compiling%20views..." > /dev/null
fi
