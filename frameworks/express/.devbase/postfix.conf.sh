#!/bin/sh
# devbase: ensure=always
# devbase: executable=yes

# postfix first look up for MTA server name (MX record / nslookup -q=MX xxx.com)
# then look up for the server IP
# for dev., we will disable DNS MX record lookup, so only read /etc/hosts
postconf -e "smtp_dns_support_level=disabled"
