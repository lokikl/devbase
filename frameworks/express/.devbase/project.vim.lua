-- devbase: ensure=always
if console_ctl == nil then
  print("console_ctl is undefined, please checkout nvim-config/nvim.d/my/utilities.lua")
  return
end

local send = require("my.send")

local proj = os.getenv("COMPOSE_PROJECT_NAME")

local view_log = "source .envrc; docker-compose logs -f --no-log-prefix --tail=200 app"
local app_db = "source .envrc; docker-compose exec db psql -U " .. proj .. " -P expanded=auto -P pager=off"
local slurm_db = "cd ../slurmdev; docker-compose exec mysql mysql -uslurm --password=password slurm_acct_db"

_G.send_sql_under_cursor = function(flag)
  local ts = require("nvim-treesitter.ts_utils")
  local node = ts.get_node_at_cursor()
  if node:type() == "template_string" or node:type() == "source" then
    local lines = ts.get_node_text(node)
    local text = table.concat(lines, "\n")
    if text:find("/* cli") then
      send.send_current_line("next")
    else
      local fp = io.popen("paste-sql-to-next-tmux-pane " .. flag, "w")
      fp:write(text) ---@diagnostic disable-line
      fp:close()     ---@diagnostic disable-line
    end
  else
    send.send_current_line("next")
  end
end

-- send commented command
vim.keymap.set("n", "sp", function()
  vim.cmd("normal sfssu")
end)

local replay = function(cmd, clear)
  return function()
    if clear then
      run_cmd("tmux send-keys -t0 -R")
    end
    run_cmd("curl -XPOST -sk https://" .. proj .. "-dev.com/_replay/replay-last/" .. cmd)
  end
end

local replay_current = function()
  return function()
    local fn = vim.fn.expand("%"):gsub(".*src", "src")
    local ln = vim.fn.line(".")
    -- inspect-routes src/routes/dashboard.js 56
    local cmd = "inspect-routes " .. fn .. " " .. ln
    local label = run_cmd(cmd)
    run_cmd("tmux send-keys -t0 -R")
    run_cmd("curl -XPOST -sk 'https://" .. proj .. "-dev.com/_replay/replay-last/" .. label .. "'")
  end
end

which_key_map("r", {
  g = { replay("get", true), "Replay last Get request" },
  p = { replay("post", true), "Replay last Post request" },
  w = { replay_current(), "Replay current file + line" },
  r = { replay("replay", true), "Replay last replayed request" },
  f = { replay("forget", false), "Forget the last replay" },
  c = { "<cmd>call system('tmux send-keys -t0 -R')<cr>", "Clear the left pane" },
  l = { console_ctl(view_log), "View app log" },
  d = { console_ctl(app_db), "Connect to App DB" },
  s = { console_ctl(slurm_db), "Connect to Slurm DB" },
})

deprecate_keymap("n", "<leader>l", "rl")
deprecate_keymap("n", "<leader>d", "rd")
deprecate_keymap("n", "<leader>s", "rs")

local TE = require("telescope-external")
local TT = require("telescope.themes")

local te_external = function(script)
  return function()
    TE.init(script, TT.get_dropdown{})
  end
end

local inspect_files = function(pattern)
  return te_external("inspect-files './" .. pattern .. "'")
end

which_key_map("s", {
  b = { inspect_files(".devbase/*"), "devbase files" },
  h = { inspect_files("src/views/*"), "Search views" },
  a = { inspect_files("src/routes/*"), "Search routes" },
  l = { inspect_files("src/lib/*"), "Search libs" },
  w = { inspect_files("src/workers/*"), "Search background jobs" },
  d = { te_external("inspect-dbfiles"), "Search db files" },
  t = { te_external("inspect-dbtables"), "Search db tables" },
  r = { te_external("inspect-routes all"), "Search routes/jobs" },
  e = { te_external("inspect-routes recent"), "Search recent routes/jobs" },
  v = { te_external("inspect-frontend-events"), "Search frontend events" },
  j = { te_external("inspect-frontend-files"), "Search frontend js" },
})
