// devbase: ensure=always

import {compose} from "crocks";

import * as R from "ramda";
import fs from "fs-extra";

/* eslint-disable import/no-extraneous-dependencies */
import chokidar from "chokidar";

const entrance = process.argv[2];

/* eslint-disable import/no-dynamic-require */
require(`./${entrance}`);

const loaded = compose(
  R.without([`/app/dist/dev-boot.js`]),
  R.without([`/app/dist/${entrance}`]),
  R.filter(R.startsWith("/app/dist/")),
  R.keys
)(require.cache);

/* eslint-disable fp/no-mutating-methods */
const watcher = chokidar.watch(loaded);
watcher.on("ready", () => {
  watcher.on("all", (_, changedPath) => {
    console.log(`[${entrance}] ${changedPath} changed, reloading...`);
    const time = new Date();
    fs.utimesSync("/app/dist/dev-boot.js", time, time);
  });
});
