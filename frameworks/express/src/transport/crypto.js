import crypto from "crypto";
import generator from "generate-password";

const sha512hmac = plain =>
  crypto
    .createHmac("sha512", "<%= project_name %>-platform.cloud-secret-123!")
    .update(plain)
    .digest("hex");

const genPasswd = (length = 12) =>
  generator.generate({
    length,
    numbers: true,
    symbols: false,
    uppercase: true,
    excludeSimilarCharacters: true,
    strict: true
  });

export { sha512hmac, genPasswd };
