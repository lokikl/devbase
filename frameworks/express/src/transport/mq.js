// devbase: ensure=always
import {connect, StringCodec} from "nats";
import {logInfo} from "../devbase_helpers";

/* eslint-disable fp/no-let */
let nc = null;

const sc = StringCodec();

export const connectMQ = async (natsEndpoint, natsPort) => {
  if (nc) return nc;
  const endpoint = natsEndpoint || process.env.NATS_ENDPOINT;
  const port = natsPort || process.env.NATS_PORT;
  nc = await connect({servers: `${endpoint}:${port}`});
  logInfo(`NATS: connected ${nc.info.server_id}`);
  return nc;
};

export const subscribeMQ = (topic, callback) => {
  if (nc === null) {
    connectMQ();
  }
  const sub = nc.subscribe(topic, callback);
  logInfo(`NATS: subscribed ${topic}`);
  return sub;
};

export const publishMQ = async (topic, data = {}) => {
  if (nc === null) {
    await connectMQ();
  }
  nc.publish(topic, sc.encode(JSON.stringify(data)));
  logInfo(`NATS: published ${topic}`);
};

export const askMQ = async (topic, data = {}, timeout = 1000) => {
  if (nc === null) {
    await connectMQ();
  }
  const res = await nc.request(topic, sc.encode(JSON.stringify(data)), {
    timeout,
  });
  const output = JSON.stringify(sc.decode(res.data));
  return output;
};
