// devbase: ensure=always

import express from "express";
import * as R from "ramda";
import fs from "fs-extra";
import yaml from "js-yaml";
import pug from "pug";
import axios from "axios";
import {execP, getUtils} from "./helpers";

const router = express.Router();

const {asyncRoute} = getUtils(R.last(__filename.split("/")));

// cache pug to speed up development
// invalidate cache by hook files
/* eslint-disable fp/no-delete */
const recachePugFile = (template) => {
  console.log(`[recaching ${template}]`);
  delete pug.cache[template];
  pug.compileFile(template, {cache: true});
};

router.get("/uid-invalidate/:side", (req, res) => {
  res.json({ok: 1});
  const {side} = req.params;
  if (side === "both" || side === "back") {
    recachePugFile("/app/src/views/uid.pug");
  }
  if (side === "both" || side === "front") {
    recachePugFile("/webfront/uid/frontend.pug");
  }
  axios.get(
    "http://127.0.0.1:3001/dev-server?cmd=reload&reason=Views%20updated"
  );
});

// regression test APIs for the devbase chrome extension
const regressionTestFile = `test/devbase-regressions.yaml`;

router.get(
  "/test-plans",
  asyncRoute(async (req, res) => {
    if (fs.existsSync(regressionTestFile)) {
      const plansYAML = await fs.readFile(regressionTestFile);
      return res.json(yaml.load(plansYAML, {}));
    }
    res.json({});
  })
);

router.post(
  "/test-plans/save",
  asyncRoute(async (req, res) => {
    await fs.writeFile(
      regressionTestFile,
      yaml.safeDump(JSON.parse(req.rawBody).plans, {})
    );
    res.json({ok: 1});
  })
);

router.post(
  "/resetdb",
  asyncRoute(async (req, res) => {
    await execP("/app/.devbase/bin/resetdb", {timeout: 5000});
    res.json({ok: 1});
  })
);

// forwarding the open-mixin signal from frontend uid() to uid server (like opening a uid-prompt)
router.post(
  "/open-mixin",
  asyncRoute(async (req, res) => {
    const {mixin, params} = JSON.parse(req.body.payload);
    await axios.post("http://uid:3000/open-mixin", {mixin, params});
    res.json({ok: 1});
  })
);

// reported by boot.sh > nodemon > detect-babel-error.sh
router.post(
  "/babel-syntax-error",
  asyncRoute(async (req, res) => {
    req.app.hasBabelSyntaxError = true;
    res.json({ok: 1});
  })
);

export default router;
