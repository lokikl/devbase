import * as R from "ramda";
import { trimHeredoc } from "./helpers";
import nodemailer from "nodemailer";

const getTransport = () =>
  nodemailer.createTransport({
    host: process.env.SMTP_SERVER,
    port: process.env.SMTP_PORT
  });

const sendTestEmail = async (name, address, url) => {
  const message = {
    from: `"${process.env.SMTP_SENDER_NAME}" ${process.env.SMTP_SENDER_ADDRESS}`,
    to: [{ name, address }],
    subject: `Testing email to ${name}`,
    html: trimHeredoc(`
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
          <title>Testing email</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </head>
        <body style="margin: 0; padding: 0;">
          <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;"
            style="border: 1px solid #cccccc;">
            <tr>
              <td align="center" bgcolor="#eeeeee" style="padding: 40px 0 30px 0;">Header</td>
            </tr>
            <tr>
              <td bgcolor="#ffffff" style="padding: 40px 10px 30px 10px;">Content</td>
            </tr>
            <tr>
              <td bgcolor="#eeeeee" style="padding: 40px 10px 30px 10px;">Footer</td>
            </tr>
          </table>
        </body>
      </html>
    `)
  };

  const transport = getTransport();
  return transport.sendMail(message);
};

export { sendTestEmail };


