// devbase: ensure=on-bootstrap

import express from "express";
import * as R from "ramda";
import {sha512hmac} from "../transport/crypto";
import knex from "../transport/database";
import * as H from "./helpers";

const router = express.Router();

const {asyncRoute, renderUID} = H.getUtils(R.last(__filename.split("/")));

router.get(
  "/",
  asyncRoute(async (req, res) => {
    renderUID(req, res, "Homepage", "index", {});
  })
);

router.post(
  "/signin",
  asyncRoute(async (req, res) => {
    const {email, password} = req.body;
    const pass = sha512hmac(password);
    const {rows: users} = await knex.raw(
      `
      -- email: 'loki.ng@pointup.studio'
      -- pass: 'fedd150dc7d20f2746b32d0872e7f14bd479953ef62f3aa839f2d137919f7f29cc4ba02c30f285e594e3a82debe436bf90582e6f7ac0901cbd65e2b4ade220d0'
      select
        *
      from
        user_profile
      where
        email = :email
        and password_hash = :pass
    `,
      {email, pass}
    );
    if (users.length > 0) {
      // success
    } else {
      // failed
      throw new Error("Invalid credential");
    }
    res.redirect("back");
  })
);

export default router;
