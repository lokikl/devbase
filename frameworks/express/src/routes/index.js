import {compose} from "crocks";
import * as R from "ramda";
import express from "express";
import UAParser from "ua-parser-js";
import * as H from "./helpers";

const router = express.Router();

const {asyncRoute, renderUID} = H.getUtils(R.last(__filename.split("/")));

router.use((req, res, next) => {
  res.locals.titleAffix = "<%= project_name %>";
  if (req.app.get("env") === "development") {
    res.on("finish", () => {
      if (R.isNil(res.handlerFile)) return;
      if (req.get("devhost")) return;
      const label = R.join("!!", [res.handlerFile, req.method, req.route.path]);
      const record = {
        label,
        method: req.method,
        host: req.hostname,
        url: `${req.baseUrl}${req.url}`,
        headers: req.headers,
        body: req.body,
        statusCode: res.statusCode,
      };
      req.app.lastReqs = compose(
        R.take(25),
        R.uniqBy(R.prop("label")),
        R.prepend(record)
      )(req.app.lastReqs || []);
    });
  }
  if (req.headers["redirect-to"]) {
    // for res.redirect("back") to work inside navframe
    req.headers.referer = req.headers["redirect-to"];
  }
  next();
});

// guess device from UA
router.use((req, res, next) => {
  if (!req.session.device) {
    const ua = UAParser(req.headers["user-agent"]);
    req.session.device = R.includes(ua.device.type, ["mobile", "tablet"])
      ? "mobile"
      : "desktop";
  }
  next();
});

router.get("/rwd/:d", (req, res) => {
  req.session.device = req.params.d;
  res.redirect("back");
});

const ensureNotAuthed = asyncRoute(async (req, res, next) => {
  if (req.session.user) {
    return res.redirect("/");
  }
  next();
});

router.get(
  "/",
  asyncRoute(async (req, res) => {
    const table = await H.tableExt({
      req,
      options: {
        id: "t",
        sortable: ["name", "state"],
        sort: ["created_at", "asc"],
        filterable: {
          name: "freetext",
          state: "select",
          created_at: "date_range",
        },
      },
      sql: `
        -- expand
        -- accountId: 'cus001'
        select
          id,
          name,
          email,
          state,
          created_at
        from user_profile
      `,
    });
    renderUID(req, res, "Index", "index", {table});
  })
);

router.post(
  "/signin",
  ensureNotAuthed,
  asyncRoute(async (req, res) =>
    // const {username, password} = req.body;
    // req.session.user = user;
    res.redirect("/")
  )
);

router.post(
  "/logout",
  asyncRoute(async (req, res) => {
    req.session.destroy();
    res.redirect("back");
  })
);

export default router;
