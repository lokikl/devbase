import * as R from "ramda";
import express from "express";
import {curry, Async} from "crocks";
import {exec} from "child_process";
import qs from "qs";
import multer from "multer";
import axios from "axios";
import {sqlToTableExt} from "../lib/table-ext";
import knex from "../transport/database";

const getErrStackTrace = (err) => {
  const sp = err.stack ? err.stack.search(/\/app\/src/) : -1;
  if (sp === -1) {
    return "<no stack trace>";
  }
  const cc = err.stack.slice(sp).search(/\n/);
  return err.stack.slice(0, sp + cc).replaceAll("    ", "");
};

const sanitize = R.mapObjIndexed((v, k) =>
  R.includes("password", k) ? "--sanitized--" : v
);

const logSysErr = (req, err) =>
  console.log("logSysErr", {
    req_id: req.id,
    referer: req.get("Referrer") || "",
    user_agent: req.get("user-agent"),
    ip_addr: req.ip,
    at: new Date(),
    method: req.method,
    url: `${req.protocol}://${req.get("host")}${req.originalUrl}`,
    body: JSON.stringify(sanitize(req.body)),
    summary: `${err}`,
    details: getErrStackTrace(err),
  });

export const logEvent = (action) => (req, res, next) => {
  /* eslint-disable fp/no-mutation */
  res.eventLogData = {action, details: []};
  next();
};

const notifyUID = async (req, mixin, params) => {
  if (JSON.stringify(params).length < 50000) {
    await axios.post("http://uid:3000/open-mixin", {mixin, params});
  }
};

export const getUtils = (label) => {
  const renderUID = async (req, res, title, mixin, params, callback) => {
    const final = R.pipe(
      R.defaultTo({}),
      R.merge(params)
    )(res.locals.uidLocals);
    if (req.app.locals.appVersion === "dev" && !req.dontNotifyUID) {
      await notifyUID(req, mixin, final);
    }
    res.render(
      "uid",
      {
        title: R.pipe(
          R.append(res.locals.titleAffix),
          R.reject(R.isNil),
          R.join(" | ")
        )([title]),
        mixin,
        params: final,
        baseUrl: req.baseUrl,
      },
      callback
    );
  };
  return {
    renderUID,
    renderUIDToText: curry(
      (req, res, mixin, params) =>
        new Promise((resolve, reject) => {
          res.locals.forPrint = true;
          renderUID(req, res, "Whatever", mixin, params, (err, output) => {
            if (err) {
              return reject(err);
            }
            return resolve(output);
          });
        })
    ),
    renderText: curry((req, res, mixin, params) =>
      Async((rej, r) => {
        res.locals.forPrint = true;
        renderUID(req, res, "Whatever", mixin, params, (err, output) => {
          if (err) {
            return rej(err);
          }
          return r(output);
        });
      })
    ),
    asyncRoute:
      (route, noRedirect = false) =>
      (req, res, next = console.error) => {
        res.handlerFile = label;
        Promise.resolve(route(req, res, next))
          .then(() => {})
          .catch((err) => {
            console.log(err);
            // .detail is from knex, .message is typical js error object, or it's just a string
            const errMsg =
              err.routine && err.routine === "exec_stmt_raise"
                ? R.last(err.message.split(" - "))
                : err.constraint
                ? err.constraint.replace(/_/g, " ")
                : err.detail || err.message || err;
            if (req.method === "POST") {
              console.log(`asyncRoute Error (POST): ${errMsg}`);
              if (noRedirect) {
                res.json({errMsg});
              } else {
                if (req.flash) {
                  req.flash("error", errMsg);
                }
                res.redirect("back");
              }
            } else {
              console.log(`asyncRoute Error (GET): ${errMsg}`);
              next(err);
            }
            req.err = err; // for event logging in src/routes/index.js
            if (res.eventLogData) {
              res.eventLogData.details = R.append(
                `Error: ${err}`,
                res.eventLogData.details
              );
            }
            logSysErr(req, err);
          });
      },
  };
};

export const keyize = (name) =>
  name
    .toLowerCase()
    .replace(/[^\w]+/g, "-")
    .replace(/-$/, "");

export const capitalize = (name) =>
  name.charAt(0).toUpperCase() + name.slice(1);

export const titleize = R.pipe(
  R.split(/[\s-_]/),
  R.map((str) => capitalize(str)),
  R.join(" ")
);

export const trimHeredoc = (doc) => {
  const lines = doc.split("\n").slice(1, -1);
  const nSpaces = lines[0].match("^ *")[0].length;
  return R.pipe(
    R.map((line) => line.slice(nSpaces)),
    R.join("\n")
  )(lines);
};

const mapIndexed = R.addIndex(R.map);

const processArrayInBody = (req, res, next) => {
  const temp = R.pipe(
    R.keys,
    R.reduce((out, key) => {
      const value = req.body[key];
      return R.is(Array, value) && R.includes("[]", key)
        ? R.pipe(
            mapIndexed((v, i) => [key.replace("[]", `[${i}]`), v]),
            R.fromPairs,
            R.mergeLeft(out)
          )(value)
        : R.assoc(key, value, out);
    }, {})
  )(req.body);
  req.body = qs.parse(qs.stringify(temp), {allowDots: true});
  return next();
};

export const parseBody = [
  multer({dest: "/tmp/"}).array("files"),
  processArrayInBody,
];

export const ensureHosts = (hosts, router) => (req, res, next) => {
  const host = req.get("host");
  if (req.app.locals.appVersion === "dev" && R.endsWith("ngrok.io", host)) {
    return router(req, res, next);
  }
  if (R.includes(host, hosts)) {
    return router(req, res, next);
  }
  return next();
};

export const parseValue = (req, res, next) => {
  req.value = R.trim(req.body.value || "");
  return next();
};

export const ensureValue = (req, res, next) => {
  req.value = R.trim(req.body.value || "");
  if (req.value === "") {
    req.flash("error", "Input is expected but missing");
    res.redirect("back");
  } else {
    next();
  }
};

export const anonymousRouter = (callback) => callback(express.Router());

export const redirectMiddleware = (path) => (req, res) =>
  res.redirect(req.baseUrl + path);

export const tableExt = async ({req, options, sql, sqlParams}) =>
  sqlToTableExt(sql)(req.query)(options)(sqlParams);

/* eslint-disable no-unused-expressions */
export const execP = (cli, options) =>
  new Promise((resolve, reject) => {
    exec(cli, options, (error, stdout) => {
      error ? reject(error.message) : resolve(stdout);
    });
  });

export const delay = (ms) =>
  new Promise((resolve) => {
    setTimeout(resolve, ms);
  });

export const allFlashes = (req) =>
  R.pickBy(R.complement(R.isEmpty), {
    reloadOnNavframeClose: req.flash("reload-on-navframe-close"),
    closeUidPromptByNavframe: req.flash("close-uid-prompt-by-navframe"),
    info: req.flash("info"),
    warn: req.flash("warn"),
    error: req.flash("error"),
  });

export const mergeJSONB = async (table, where, col, data) =>
  knex(table)
    .where(where)
    .update({
      [col]: knex.raw(`${col} || :data`, {data}),
    });
