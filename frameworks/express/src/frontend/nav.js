// devbase: ensure=always

/* eslint-disable jquery/no-class, jquery/no-data, jquery/no-val */
/* eslint-disable jquery/no-attr, jquery/no-prop, jquery/no-css */
/* eslint-disable jquery/no-text, jquery/no-html */

// intersect internal links and form post
// ajax with fetch() and render page gracefully
// avoided reloading css and js
//
// therefore suggest turn this on for backend, or js-heavy app
// but turn off for public frontend

// to activate, please add "nav-base" class into uid.pug

import * as R from "ramda";
import {uidPrompt, uid, quickInfo} from "./uid-prompt";

/* eslint-disable no-promise-executor-return */
const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const raf = () =>
  new Promise((resolve) => window.requestAnimationFrame(resolve));

const pathnameFromUrl = R.pipe(
  R.split("/"),
  R.slice(3, Infinity),
  R.join("/"),
  R.split("?"),
  R.head,
  (str) => `/${str}`
);

// const defaultAnimation = "fade";
const defaultAnimation = "none";

const determineAnimation = (prev, next) => {
  if (prev === next) {
    return defaultAnimation;
  }
  // if (R.startsWith(prev, next)) {
  //   return "forward";
  // }
  // if (R.startsWith(next, prev)) {
  //   return "backward";
  // }
  const pathOrder = $("body").data("path-order");
  if (pathOrder) {
    const prevIdx = R.indexOf(prev.split("/")[1], pathOrder);
    const nextIdx = R.indexOf(next.split("/")[1], pathOrder);
    if (nextIdx < prevIdx) {
      return "slide-left";
    }
    return "slide-right";
  }
  return defaultAnimation;
};

const getAlerts = ($page) => {
  const $alerts = $(".alert", $page);
  if ($alerts.length > 0) {
    return $alerts;
  }
  return $page.filter(".alert");
};

// get url of a frame / root if no frame found
const getCurrentUrl = ($node) => {
  const $navFrame = $node.closest("[data-nav-frame]");
  if ($navFrame.length > 0) {
    return $navFrame.data("nav-frame-url");
  }
  return window.location.pathname + window.location.search;
};

// get the baseUrl of a frame / root if no frame found
const getBaseUrl = ($node) => {
  const $navFrame = $node.closest("[data-nav-frame]");
  if ($navFrame.length > 0) {
    return $navFrame.data("nav-frame-baseurl");
  }
  return $("[data-base-url]").data("base-url");
};

const getCsrfToken = () => $("[data-csrf-token]").data("csrf-token");

const fetchGet =
  (url, headers = {}) =>
  async () =>
    fetch(url, {
      method: "GET",
      redirect: "follow",
      credentials: "same-origin",
      headers,
    });

const fetchPost = (url, data, options = {}) => {
  const formData = new FormData();
  formData.set("_csrf", getCsrfToken());
  R.mapObjIndexed((v, k) => formData.append(k, v))(data);
  const extra = options.currentUrl
    ? {headers: {"redirect-to": options.currentUrl}}
    : {};
  return async () =>
    fetch(url, {
      method: "post",
      body: formData,
      ...extra,
    });
};

/* eslint-disable fp/no-rest-parameters */
/* eslint-disable fp/no-let */
// to avoid recursive calls
const withMutexLock = (fn) => {
  let isLocked = false;
  return async (...args) => {
    if (isLocked) {
      console.log(`fn skipped due to the mutex lock`);
      return true;
    }
    isLocked = true;
    try {
      await fn(...args);
    } finally {
      isLocked = false;
    }
  };
};

const partialReplace = (replacing, $page, html) => {
  const filterTopMosts = R.filter(
    (node) => $(node).parent().closest(replacing).length === 0
  );
  const $current = filterTopMosts($(replacing, $page).toArray());
  const $new = filterTopMosts($(replacing, $(html)).toArray());
  if ($current.length !== $new.length) {
    console.log(`NAV replacing error: count mismatch ${replacing}`);
    return false;
  }
  /* eslint-disable fp/no-loops */
  /* eslint-disable no-restricted-syntax */
  /* eslint-disable guard-for-in */
  for (const [i, current] of $current.entries()) {
    const $newNode = $($new[i]);
    $(current).replaceWith($newNode);
  }
};

const initNav = () => {
  const $body = $("body");

  let currentUrl = false;

  const updateNavFrame = async (config, data, replacing) => {
    const $frame = $(`[data-nav-frame="${config.frame}"]`);
    $frame.closest(".start-up-hidden").removeClass("start-up-hidden");
    if (R.has("mixin", config)) {
      const html = uid(config.mixin, R.dissoc("nav", data), false, {
        baseUrl: config.baseUrl,
      });
      // support replacing
      if (replacing) {
        partialReplace(replacing, $frame, html);
      } else {
        $frame.html(html);
      }
      $(window).trigger("scroll.position-uid-prompt");
    }
    /* eslint-disable no-await-in-loop */
    for (const level in config.flashes || {}) {
      const [msg] = config.flashes[level];
      if (level === "reloadOnNavframeClose") {
        $frame.closest(".up-backdrop").data("reloadOnNavframeClose", msg);
      } else if (level === "closeUidPromptByNavframe") {
        await raf();
        const $up = $(`[data-nav-frame="${msg}"]`).closest(".up-backdrop");
        if ($up.length > 0) {
          await $up.data("dismissAndResolve")();
        }
      } else {
        await raf();
        const $canvas = $frame.closest(".up-canvas");
        if (!$canvas.closest(".up-backdrop").hasClass("dismissed")) {
          quickInfo(msg, level, level !== "error", $canvas);
        } else {
          quickInfo(msg, level, level !== "error");
        }
      }
    }
  };

  const fetchAndRender = withMutexLock(
    async ({
      fetchFn,
      fetchUrl,
      pushState,
      delayMS = 0,
      needLoader = false,
      dir = defaultAnimation,
      replacing = false,
      ignoreLeaveGuard = false,
    }) => {
      if (window.onbeforeunload) {
        if (!ignoreLeaveGuard) {
          const msg = window.onbeforeunload();
          const {action} = await uidPrompt({
            $node: $body,
            html: uid("p-edit-one-value", {
              type: "yesno",
              name: "Confirm",
              desc: msg,
            }),
            direction: "center",
          });
          if (action === "no") {
            return false;
          }
        }
        window.onbeforeunload = null;
      }
      const fn = !fetchFn && fetchUrl ? fetchGet(fetchUrl) : fetchFn;

      const $base = $(".nav-base", $body);
      if (needLoader) {
        $(`<div class="loader">Loading...</div>`).appendTo($base);
      }
      if (delayMS > 0) {
        await delay(delayMS);
      }
      const resp = await fn();

      // handle both fetch & axios
      if (resp.ok || resp.status === 200) {
        const respUrl = resp.url || resp.headers["current-url"];

        // NEW FEATURE: nav frame
        // in server side GET api, we need to return a json with a nav object in order to activate navframe
        // res.json({
        //   nav: {
        //     frame: "edit-receipt",
        //     mixin: "edit-receipt-content",
        //     baseUrl: req.baseUrl,
        //     flashes: H.allFlashes(req),
        //   },
        //   ... mixin params ...
        // });
        const contentType = resp.headers.get("Content-Type");
        if (R.includes("json", contentType)) {
          const json = await resp.json();
          if (json.navRedirect) {
            window.location.href = json.navRedirect;
            return false;
          }
          const config = json.nav;
          await updateNavFrame(config, json, replacing);
          $(`[data-nav-frame="${config.frame}"]`)
            .data("nav-frame-url", respUrl)
            .data("nav-frame-baseurl", config.baseUrl);
          if (needLoader) {
            $(".loader", $base).remove();
          }
          $body.trigger("statechanged");
          return false;
        }

        const html = resp.text ? await resp.text() : resp.data;

        // highlight current tab in nav
        $(`.nav > a`)
          .removeClass("active")
          .each((i, node) => {
            const $btn = $(node);
            if (R.startsWith($btn.prop("href"), respUrl)) {
              $btn.addClass("active");
            }
          });

        // replacing part of page instead of full page (like navy aimed)
        if (replacing) {
          partialReplace(replacing, $base, html);
          if (needLoader) {
            $(".loader", $base).remove();
          }
          return false;
        }

        // dismiss all uid prompts
        $(".up-backdrop").each((i, node) => {
          $(node).data("dismissAndResolve")("cancel-by-statechanged");
        });

        const $page = $(html);
        const $next = $(".nav-base", $page)
          .insertAfter($base)
          .addClass(`nav-load__${dir}`);
        $base.addClass("nav-unload");
        await raf();
        await delay(10);
        $base.addClass(`nav-unload__${dir}`);
        $next.addClass("nav-entering");
        // no animation and no need to delay
        // await delay(400);

        $base.remove();
        $next.removeClass(`nav-load__${dir}`).removeClass("nav-entering");

        const pageTitle = $page.filter("title").text();

        window.document.title = pageTitle;
        if (pushState === true) {
          window.history.pushState({}, pageTitle, respUrl);
        } else if (pushState === false) {
          window.history.replaceState({}, pageTitle, respUrl);
        }
        getAlerts($page).appendTo($body);
        currentUrl = respUrl;
        $body.trigger("statechanged");
      } else {
        if (needLoader) {
          $(".loader", $base).remove();
        }
        const {status, statusText} = resp;
        quickInfo(`${status} ${statusText}`, "error");
      }
    }
  );

  // load nav frame with initial href
  $body.on("statechanged", () => {
    $(`[data-initial-href]`, $body).each(async (i, node) => {
      const $frame = $(node);
      const href = $frame.data("initial-href");
      $frame.removeAttr("data-initial-href");
      await fetchAndRender({
        fetchUrl: href,
        pushState: false,
        delayMS: 0,
        needLoader: false,
      });
    });
  });

  window.navFetchAndRender = fetchAndRender;

  window.onpopstate = async () => {
    const $base = $(".nav-base", $body);
    $base.addClass("nav-upload__forward");
    const url = document.location;
    await fetchAndRender({
      fetchFn: fetchGet(url),
      pushState: false,
      dir: currentUrl
        ? determineAnimation(pathnameFromUrl(currentUrl), url.pathname)
        : defaultAnimation,
    });
  };

  window.navReload = (url) => {
    console.log(url || document.location.href);
    fetchAndRender({
      fetchUrl: url || document.location.href,
      pushState: false,
      needLoader: true,
    });
  };

  const linkTarget = "a:not(.nav-skip):not([target=_blank])";
  const formTarget = "form:not(.nav-skip)";
  const isInternalUrl = R.startsWith(document.location.origin);

  // const getTargetFrame = ($btn) => {
  //   const frame = $btn.data("frame");
  //   if (frame) {
  //     return frame;
  //   }
  //   const $frame = $btn.closest("[data-nav-frame]");
  //   if ($frame.length > 0) {
  //     return $frame.data("nav-frame");
  //   }
  //   return false;
  // };

  $body.on("click.nav-click", linkTarget, async (e) => {
    const $btn = $(e.currentTarget);
    if ($btn.closest(".nav-skip").length > 0) return true;
    const url = $btn[0].href;
    if (R.isNil(url) || url === "") return true;
    if (!isInternalUrl(url)) return true;
    if ($btn.attr("href")[0] === "#") return true;
    e.preventDefault();

    if (url === window.location.href) {
      return true;
    }
    // const frame = getTargetFrame($btn);
    await fetchAndRender({
      // fetchFn: fetchGet(url, frame ? {"Target-Frame": frame} : {y),
      fetchFn: fetchGet(url),
      pushState: true,
      delayMS: $btn.data("nav-delay"),
      dir: determineAnimation(window.location.pathname, pathnameFromUrl(url)),
      ignoreLeaveGuard: $btn.hasClass("ignore-leave-guard"),
      replacing: $btn.data("nav-replacing"),
    });
  });

  // use navframe in unobtrusive complete CSR
  $body.on("click.update-nav-frame", "[data-update-nav-frame]", async (e) => {
    const $btn = $(e.currentTarget);
    const {frame, mixin, params} = $btn.data("update-nav-frame");
    const baseUrl = getBaseUrl($btn);
    const config = {frame, mixin, baseUrl};
    await updateNavFrame(config, params);
  });

  $body.on("submit.nav-form-submit", formTarget, async (e) => {
    const $form = $(e.currentTarget);
    if ($form.closest(".nav-skip").length > 0) return true;
    e.preventDefault();
    const method = ($form.attr("method") || "").toLowerCase();
    let fetchFn = false;
    if (method === "get") {
      const url = new URL($form.prop("action"));
      R.forEach((input) => {
        url.searchParams.set(input.name, input.value);
      })($form.serializeArray());
      fetchFn = () => fetch(url.toString());
    } else if (method === "post") {
      const formData = new FormData($form[0]);
      formData.set("_csrf", getCsrfToken());
      fetchFn = () =>
        fetch($form.attr("action"), {
          method: "post",
          body: formData,
          headers: {
            "redirect-to": getCurrentUrl($form),
          },
        });
    } else {
      return false;
    }
    if ($form.hasClass("slow-form")) {
      $form.addClass("nav-submitting");
    }
    await fetchAndRender({
      fetchFn,
      pushState: false,
      ignoreLeaveGuard: $form.hasClass("ignore-leave-guard"),
      replacing: $form.data("nav-replacing"),
    });
    $form.removeClass("nav-submitting");
  });

  const mapIndexed = R.addIndex(R.map);
  const addIdx = mapIndexed((item, idx) => [idx, item]);

  $body.on("click.upload-multiple", `[data-upload-multiple]`, async (e) => {
    const $btn = $(e.currentTarget);
    const {
      accept,
      to: url,
      sizeLimitMB,
      progressPrompt,
      autoResolve,
    } = $btn.data("upload-multiple");

    $(".upload-multiple-input", $body).remove();
    const $input = $(`<input type="file" multiple accept="${accept}">`)
      .addClass("upload-multiple-input")
      .css("transform", "translate(0, -9999px)")
      .appendTo($body);

    $input.on("change", async () => {
      const {files} = $input[0];

      const exceededSizeFile = R.find(
        (file) => file.size > sizeLimitMB * 1024 * 1024,
        files
      );
      if (exceededSizeFile) {
        $input.remove();
        alert(
          `File (${exceededSizeFile.name}) is larger than ${sizeLimitMB}MB`
        );
        return false;
      }

      await uidPrompt({
        $node: $("body"),
        html: uid(progressPrompt, {}),
        direction: "center",
        onReady: async ($prompt, dismissAndResolve) => {
          $(`[data-resolve="cancel"]`, $prompt).addClass("hidden");
          const filesCount = files.length;
          $(".files-total", $prompt).text(filesCount);
          const $progress = $(".progress", $prompt);
          const $filesCurrent = $(".files-current", $prompt);
          const $file = $(".file", $prompt);
          const $status = $(".status", $prompt);
          /* eslint-disable no-restricted-syntax */
          /* eslint-disable no-await-in-loop */
          /* eslint-disable fp/no-loops */
          for (const [idx, file] of addIdx(files)) {
            $progress.text(
              `${Math.round((idx * 2 * 100) / (filesCount * 2))}%`
            );
            $filesCurrent.text(idx + 1);
            $file.text(file.name);
            const formData = new FormData();
            formData.set("_csrf", getCsrfToken());
            formData.append("files", file);
            const res = await axios.request({
              method: "post",
              url,
              data: formData,
              onUploadProgress: (p) => {
                const progress = Math.round((p.loaded * 100) / p.total);
                if (progress === 100) {
                  $progress.text(
                    `${Math.round(((idx * 2 + 1) * 100) / (filesCount * 2))}%`
                  );
                  $status.text(`analysing`);
                } else {
                  $status.text(`uploading ${progress}%`);
                }
              },
            });
            if (res.data.err) {
              $status
                .parent()
                .text(`Error: ${res.data.err.message || res.data.err}`);
              $(`[data-resolve="cancel"]`, $prompt).removeClass("hidden");
              return false;
            }
          }
          $progress.text("100%");
          $file.text("-");
          $status.text("-");
          $input.remove();
          $(`[data-resolve="cancel"]`, $prompt).removeClass("hidden");
          if (autoResolve) {
            dismissAndResolve(autoResolve);
          }
        },
      });
      await fetchAndRender({
        fetchFn: fetchGet(getCurrentUrl($btn)),
        pushState: false,
      });
    });
    $input.trigger("click");
  });

  $body.on("click.upload-file", `[data-upload-file]`, async (e) => {
    const $btn = $(e.currentTarget);
    const {accept, to: url, sizeLimitMB} = $btn.data("upload-file");

    $(".upload-file-input", $body).remove();
    const $input = $(`<input type="file" accept="${accept}">`)
      .addClass("upload-file-input")
      .css("transform", "translate(0, -9999px)")
      .appendTo($body);

    $input.on("change", async () => {
      const {
        files: [file],
      } = $input[0];
      if (file.size > sizeLimitMB * 1024 * 1024) {
        alert(`File too large (${sizeLimitMB}MB)`);
        return false;
      }
      const formData = new FormData();
      formData.set("_csrf", getCsrfToken());
      formData.append("files", file);
      const fetchFn = () =>
        axios.request({
          method: "post",
          url,
          data: formData,
          onUploadProgress: (p) => {
            const progress = Math.round((p.loaded * 100) / p.total);
            if (progress === 100) {
              $btn.text(`Processing ...`);
            } else {
              $btn.text(`${progress}%`);
            }
          },
        });
      await fetchAndRender({
        fetchFn,
        pushState: false,
      });
      // const rJson = await r.json();
      // // console.log("rJson" + ": " + JSON.stringify(rJson));
      // // @TODO loki, 12/05/2020: popup dialog to show changes and confirm with users
      // window.location.reload();
      $input.remove();
    });
    $input.trigger("click");
  });
};

export {initNav, fetchGet, fetchPost, getCurrentUrl, getBaseUrl};
