// devbase: ensure=always

/* eslint-disable jquery/no-class, jquery/no-data, jquery/no-val */
/* eslint-disable jquery/no-attr, jquery/no-prop, jquery/no-css */
/* eslint-disable jquery/no-text, jquery/no-html */

import * as R from "ramda";
import * as Nav from "./nav";
import {uidPrompt, uid} from "./uid-prompt";

const initTableExt = () => {
  const $body = $("body");

  const encodeOptions = R.pipe(
    R.dissoc("id"),
    R.dissoc("sortable"),
    R.dissoc("filterable"),
    R.dissoc("extraColumns"),
    R.dissoc("extraFlags"),
    R.dissoc("exportUrl"),
    R.dissoc("highlight"),
    R.dissoc("noSettings"),
    (opt) => encodeURIComponent(JSON.stringify(opt)),
    (str) => btoa(str)
  );

  $body.on("statechanged", () => {
    const $exportBtn = $(".table-ext a.export-xlsx-btn", $body);
    if ($exportBtn.length > 0) {
      const $wrap = $exportBtn.closest(".table-ext");
      const options = $wrap.data("options");
      const encoded = encodeOptions(options);
      const href = $exportBtn.prop("href");
      const newHref = `${href}?${options.id}=${encoded}`;
      $exportBtn.prop("href", newHref).removeClass("hidden");
    }
    // check for conflicted tableext id
    const ids = $(".table-ext")
      .map((_i, node) => $(node).data("id"))
      .toArray();
    if (ids.length !== R.uniq(ids).length) {
      $(".table-ext").before(
        "<div style='color:red'>Conflicting table-ext IDs detected</div>"
      );
    }
  });

  $body.on("click.import-xlsx", `[data-import-xlsx]`, async (e) => {
    const $btn = $(e.currentTarget);
    const href = $btn.data("import-xlsx");

    $(".upload-xlsx", $body).remove();
    const $input = $('<input type="file" accept=".xlsx">')
      .addClass("upload-xlsx")
      .css("transform", "translate(0, -9999px)")
      .appendTo($body);

    $input.on("change", async () => {
      const {
        files: [file],
      } = $input[0];
      if (file.size > 1 * 1024 * 1024) {
        alert("File too large (1MB)");
        return false;
      }
      /* eslint-disable no-undef */
      const formData = new FormData();
      formData.append("xlsx", file);
      await fetch(href, {
        method: "POST",
        body: formData,
      });
      // @TODO loki, 12/05/2020: popup dialog to show changes and confirm with users
      window.location.reload();
      $input.remove();
    });
    $input.trigger("click");
  });

  const updateTable = async ($table, options, flags = false) => {
    const encoded = encodeOptions(options);
    const url = Nav.getCurrentUrl($table);
    const [path, qs] = url.split("?");
    // replace the tableext query
    const newQS = R.pipe(
      R.split("&"),
      R.without([""]),
      R.map(R.split("=")),
      R.reject(([k]) => R.includes(k, [options.id, "flags"])),
      R.append([options.id, encoded]),
      flags ? R.append(["flags", R.join(",", flags)]) : R.identity,
      R.map(R.join("=")),
      R.join("&")
    )(qs || "");
    await window.navFetchAndRender({
      fetchUrl: `${path}?${newQS}`,
      pushState: false,
      delayMS: 0,
      needLoader: false,
    });
  };

  $body.on("click.table-ext-switch-page", "[data-te-page]", (e) => {
    const $btn = $(e.currentTarget);
    const $wrap = $btn.closest(".table-ext");
    const options = $wrap.data("options");
    if ($btn.is("[disabled]")) return true;
    options.page = $btn.data("te-page");
    updateTable($wrap, options);
  });

  $body.on("click.table-ext-sort", "[data-te-sort]", (e) => {
    const $btn = $(e.currentTarget);
    const $wrap = $btn.closest(".table-ext");
    const options = $wrap.data("options");
    const key = $btn.data("te-sort");
    if (options.sort[0] === key) {
      options.sort[1] = options.sort[1] === "asc" ? "desc" : "asc";
    } else {
      options.sort = [key, "asc"];
    }
    updateTable($wrap, R.dissoc("page", options));
  });

  // deprecated by submenu implemented right underneath
  $body.on("change.table-ext-apply-filter", "[data-te-filter]", (e) => {
    const $input = $(e.currentTarget);
    const $wrap = $input.closest(".table-ext");
    const options = $wrap.data("options");
    const key = $input.data("te-filter");
    options.filters = R.defaultTo({}, options.filters);
    const val = R.trim($input.val());
    if (val === "") {
      options.filters = R.dissoc(key, options.filters);
    } else {
      options.filters = R.assoc(key, val, options.filters);
    }
    updateTable($wrap, R.dissoc("page", options));
  });

  $body.on(
    "click.table-ext-select-filter",
    "[data-te-select-filter]",
    async (e) => {
      const $node = $(e.currentTarget);
      const $wrap = $node.closest(".table-ext");
      const {options, stats} = $wrap.data();
      const {name, selected} = $node.data("te-select-filter");
      const filterValues = stats.fopts[name];
      const {$form, action} = await uidPrompt({
        $node,
        html: uid("table-ext-select-filter", {
          selected,
          options: filterValues,
        }),
        direction: "outside,inside",
        dismissOnOutsideClick: true,
      });
      if (action === true) {
        return;
      }
      options.filters = R.defaultTo({}, options.filters);
      const newVal =
        action === "-cancel-"
          ? []
          : action === "-confirm-"
          ? $("input:checked", $form)
              .map((i, node) => $(node).val())
              .toArray()
          : [action];
      if (R.isEmpty(newVal)) {
        options.filters = R.dissoc(name, options.filters);
      } else {
        options.filters = R.assoc(name, newVal.join(";"), options.filters);
      }
      updateTable($wrap, R.dissoc("page", options));
    }
  );

  // adv. options
  $body.on("click.open-options", "[data-te-options]", async (e) => {
    const $node = $(e.currentTarget);
    const $wrap = $node.closest(".table-ext");
    const data = $wrap.data();
    await uidPrompt({
      $node,
      html: uid("table-ext-options", data),
      direction: "left",
      dismissOnOutsideClick: true,
    });
  });

  // adv. options - dyna columns
  $body.on("submit.apply-dyna-columns", "[data-te-dyna-columns]", (e) => {
    e.preventDefault();
    e.stopPropagation();
    const $form = $(e.currentTarget);
    const formData = $form.serializeArray();
    const columns = R.pipe(
      R.filter(R.propEq("name", "columns")),
      R.pluck("value")
    )(formData);
    const itemsPerPage = R.pipe(
      R.find(R.propEq("name", "itemsPerPage")),
      R.prop("value")
    )(formData);
    const flags = R.pipe(
      R.filter(R.propEq("name", "flags")),
      R.pluck("value")
    )(formData);

    const teId = $form.data("te-dyna-columns");
    const $wrap = $(`.table-ext[data-id="${teId}"]`);
    const options = $wrap.data("options");
    options.columns = columns;
    options.itemsPerPage = itemsPerPage * 1;
    updateTable($wrap, R.dissoc("page", options), flags);
  });

  $body.on("mousedown.table-ext-move-row", "[data-te-move-row]", (e) => {
    if (e.which !== 1) return true;
    const $node = $(e.currentTarget);
    const $nodeCell = $node.closest(".cell");
    const colIdx = $nodeCell.data("col");
    const $table = $node.closest(".grid-table");
    const colQry = `.cell[data-col="${colIdx}"]:not(.header)`;
    $nodeCell.css("background-color", "orange");
    $(colQry, $table).css("cursor", "pointer");
    const onMove = (me) => {
      $(".highlight-top-border", $table).removeClass("highlight-top-border");
      const $newTarget = $(me.target).closest(colQry);
      if ($newTarget.length > 0 && $.contains($table[0], $newTarget[0])) {
        const localY = me.pageY - $newTarget.offset().top;
        if (localY < $newTarget.outerHeight() / 2) {
          $newTarget.addClass("highlight-top-border");
        } else {
          $newTarget.nextAll(colQry).first().addClass("highlight-top-border");
        }
      }
    };
    $body.on("mousemove.table-ext-move-row", onMove);
    $(document).on("mouseup.table-ext-move-row", () => {
      $body.off("mousemove.table-ext-move-row");
      $(document).off("mouseup.table-ext-move-row");
      $nodeCell.css("background-color", "unset");
      $(colQry, $table).css("cursor", "unset");
      const $targetCell = $(".highlight-top-border", $table);
      if ($targetCell.length > 0) {
        $targetCell.removeClass("highlight-top-border");
        const srcVal = $nodeCell.data("row");
        const dstVal = $targetCell.data("row");
        if (dstVal !== srcVal && dstVal !== srcVal + 1) {
          console.log(`confirm moving #${srcVal} to above #${dstVal}`);
          const srcRank = $(`[data-te-move-row]`, $nodeCell).data(
            "te-move-row"
          );
          const dstRank = $(`[data-te-move-row]`, $targetCell).data(
            "te-move-row"
          );
          $node.trigger("te-move-row", {srcVal, dstVal, srcRank, dstRank});
        }
      }
    });
    return false;
  });
};

export {initTableExt};
