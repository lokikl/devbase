// devbase: ensure=always

/* eslint-disable jquery/no-class, jquery/no-val */
/* eslint-disable jquery/no-attr, jquery/no-prop, jquery/no-css */
/* eslint-disable jquery/no-text, jquery/no-html, jquery/no-ready, jquery/no-submit */

const initTextScale = () => {
  const scaleText = () => {
    $("[data-scale-text]").each((i, node) => {
      const $node = $(node);
      const minWidth = $node.data("scale-text") * 1;

      $node.css({
        transform: `none`,
        width: `auto`,
        marginLeft: `0`,
        height: `auto`,
        marginTop: `0`,
      });

      const width = $node.width();

      if (width < minWidth) {
        const ratio = (width * 1.0) / minWidth;
        const xOffset = width / 2 / ratio - width / 2;
        $node.css({
          transform: `scale(${ratio})`,
          width: minWidth,
          marginLeft: -xOffset,
        });

        const nodeHeight = $node.height();
        const newNodeHeight = nodeHeight * ratio;
        const outputHeight = newNodeHeight * ratio;
        const yOffset = (newNodeHeight - outputHeight) / 2;
        $node.css({
          marginTop: -yOffset,
          height: newNodeHeight,
        });
      }
    });
  };

  $(window).resize(scaleText);
  scaleText();
};

export {initTextScale};
