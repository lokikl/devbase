import {compose} from "crocks";
// devbase: ensure=always

/* eslint-disable jquery/no-class, jquery/no-data, jquery/no-val */
/* eslint-disable jquery/no-attr, jquery/no-prop, jquery/no-css */
/* eslint-disable jquery/no-text, jquery/no-html */

import * as R from "ramda";
import {debounce} from "throttle-debounce";

/* eslint-disable no-promise-executor-return */
const raf = () =>
  new Promise((resolve) => window.requestAnimationFrame(resolve));

const initVF = () => {
  const $body = $("body");
  /* eslint-disable fp/no-let */
  let errMessages = {};

  const renderError = (
    $wrapper,
    title,
    desc,
    klass = "inline",
    $input = null
  ) => {
    const customErr = ($input || $wrapper).data("vf-errmsg") || {};
    const msg = customErr[title] || errMessages[title] || desc;
    if ($wrapper.next(".vf-hint").length === 0) {
      $(`<div class="vf-hint ${klass}"></div>`)
        .text(`* ${msg}`)
        .insertAfter($wrapper);
    }
    $wrapper.addClass("vf-error").addClass("vf-need-validate");
  };

  const vfCheckers = {
    required: ($input) => {
      if ($input.is(`[type="radio"]`) || $input.is(`[type="checkbox"]`)) {
        const name = $input.prop("name");
        const selected = $(
          `[name="${name}"]:checked`,
          $input.closest("form")
        ).length;
        if (selected === 0) {
          renderError(
            $input.parent().parent().parent(),
            "requiredField",
            "Required",
            "blocking",
            $input
          );
          return false;
        }
      } else if (R.isNil($input.val()) || $input.val().length < 1) {
        renderError($input, "requiredField", "Required");
        return false;
      }
      return true;
    },
    passwordMinLength: ($input, minLen) => {
      const len = $input.val().length;
      if (len > 0 && len < (minLen || 10)) {
        renderError(
          $input,
          "invalidPassword",
          `Expect at least ${minLen || 10} characters.`
        );
        return false;
      }
      return true;
    },
    pattern: ($input, pattern) => {
      const v = $input.val();
      if (v.length > 0 && R.isNil(v.match(pattern))) {
        renderError($input, "invalidFormat", "Format is not valid.");
        return false;
      }
      return true;
    },
    custompattern: ($input, fnStr, formData) => {
      /* eslint-disable no-eval */
      const customPattern = eval(fnStr);
      const pattern = customPattern(formData);
      console.log("calculated pattern", pattern);

      const v = $input.val();
      if (v.length > 0 && R.isNil(v.match(pattern))) {
        renderError($input, "invalidFormat", "Format is not valid.");
        return false;
      }
      return true;
    },
    mobile: ($input) => {
      const v = $input.val();
      if (v.length > 0 && R.isNil(v.match(/^\+?[\d- ]+$/))) {
        renderError($input, "invalidMobile", "Invalid Mobile Number.");
        return false;
      }
      return true;
    },
    email: ($input) => {
      const address = $input.val();
      const emailRegex = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]+$/;
      if (!emailRegex.test(address)) {
        renderError($input, "invalidEmailAddress", "Invalid Email Address.");
        return false;
      }
      return true;
    },
    name: ($input) => {
      const regexp = /^[^()/><\][}{!@#$%^*_+\-:"\\\x22,;|]+$/;
      if (!regexp.test($input.val())) {
        renderError($input, "invalidName", "Invalid Format.");
        return false;
      }
      return true;
    },
    password: ($input) => {
      const regexp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,15}$/;
      if (!regexp.test($input.val())) {
        renderError($input, "invalidPassword", "Invalid Password Format.");
        return false;
      }
      return true;
    },
  };

  const clearErrors = ($form) => {
    $(".vf-error", $form).removeClass("vf-error");
    $(".vf-hint", $form).remove();
  };

  const serializeForm = ($form) => {
    const s = compose(R.fromPairs, R.map(R.props(["name", "value"])));
    return s($form.serializeArray());
  };

  const validateForm = ($form) => {
    if ($form.hasClass("vf-disabled")) return true;

    if ($(".vf-messages").length > 0) {
      errMessages = $(".vf-messages").data("messages");
    }
    let allValid = true;
    clearErrors($form);
    let errInputs = [];

    const formData = serializeForm($form);

    R.forEachObjIndexed((fn, key) => {
      $(`[data-vf-${key}]:not(:disabled)`, $form).each((_i, input) => {
        if (
          !R.includes(input, errInputs) &&
          fn($(input), $(input).data(`vf-${key}`), formData) === false
        ) {
          errInputs = R.concat([input], errInputs);
          allValid = false;
        }
      });
    })(vfCheckers);
    raf().then(() => {
      $(".vf-hint", $form).addClass("displayed");
    });
    if (allValid) {
      $form.removeClass("vf-invalid");
    } else {
      $form.addClass("vf-invalid");
    }
    return allValid;
  };

  $body.on("change.vf-revalidate", ".vf-need-validate", (e) => {
    const $form = $(e.currentTarget).closest("form");
    validateForm($form);
  });

  $body.on(
    "keyup.vf-revalidate",
    ".vf-need-validate",
    debounce(200, true, (e) => {
      const $form = $(e.currentTarget).closest("form");
      validateForm($form);
    })
  );

  $body.on("submit.vf-validation, vf-validate", "form", (e) => {
    const $form = $(e.currentTarget);
    if (validateForm($form)) {
      return true;
    }
    e.preventDefault();
    e.stopImmediatePropagation();
    e.stopPropagation();
    return false;
  });

  $body.on("click.vf-hint-clicked", ".vf-hint", (e) => {
    $(e.currentTarget).removeClass("displayed");
  });
};

export {initVF};
