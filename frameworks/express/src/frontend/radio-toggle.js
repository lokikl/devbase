// devbase: ensure=always

/* eslint-disable jquery/no-class, jquery/no-data, jquery/no-val */
/* eslint-disable jquery/no-attr, jquery/no-prop, jquery/no-css */
/* eslint-disable jquery/no-text, jquery/no-html */

// import * as R from "ramda";

const initRadioToggle = () => {
  const $body = $("body");

  const getOutsideForm = ($form) => {
    const $wrapper = $form.parent();
    return $(".outside-form", $wrapper).length > 0
      ? $(".outside-form", $wrapper)
      : $('<div class="outside-form hidden">').appendTo($wrapper);
  };

  const hideHook = ($hook) => {
    if ($hook.data("$hook")) return;
    const $outsideForm = getOutsideForm($hook.closest("form"));
    $("<div>")
      .attr("data-hook-radio-toggle", $hook.data("hook-radio-toggle"))
      .attr("data-visible-on-value", $hook.data("visible-on-value"))
      .data("$hook", $hook)
      .insertAfter($hook);
    $hook.appendTo($outsideForm);
  };

  $body.on("statechanged", () => {
    $(`[data-hook-radio-toggle]`, $body).each((i, node) => {
      const $hook = $(node);
      if ($hook.closest(".outside-form").length > 0) return;
      const name = $hook.data("hook-radio-toggle");
      const hookValue = $hook.data("visible-on-value");
      const $form = $hook.closest("form");
      const $checked = $(`[data-radio-toggle][name="${name}"]:checked`, $form);
      if ($checked.length === 0 || $checked.prop("value") !== hookValue) {
        hideHook($hook);
      }
    });
  });

  $body.on("change.radio-toggle", "[data-radio-toggle]", (e) => {
    const $node = $(e.currentTarget);
    const name = $node.prop("name");
    const $form = $node.closest("form");
    const value = $node.prop("value");
    $(`[data-hook-radio-toggle="${name}"]`, $form).each((i, node) => {
      const $hook = $(node);
      const hookValue = $hook.data("visible-on-value");
      if (hookValue === value) {
        if ($hook.data("$hook")) {
          $hook.data("$hook").insertAfter($hook);
          $hook.remove();
        }
      } else {
        hideHook($hook);
      }
    });
  });
};

export {initRadioToggle};
