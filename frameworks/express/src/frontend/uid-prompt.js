// devbase: ensure=always

/* eslint-disable jquery/no-class, jquery/no-data, jquery/no-val */
/* eslint-disable jquery/no-attr, jquery/no-prop, jquery/no-css */
/* eslint-disable jquery/no-text, jquery/no-html */

import * as R from "ramda";
import murmur from "murmurhash-js";
import clientIncludes from "../uid/client-includes.pug";
import * as frontendHelpers from "../frontend-helpers";

const hashSeed = String(Math.random());
const getHash = (key) => murmur.murmur3(key, hashSeed);

imagesLoaded.makeJQueryPlugin($);

const SLUG_PATTERN = /^[a-z]+[a-zA-Z0-9-_]*$/;

const delay = (ms) =>
  new Promise((resolve) => {
    setTimeout(resolve, ms);
  });

const raf = () =>
  new Promise((resolve) => {
    window.requestAnimationFrame(resolve);
  });

const untilImagesLoaded = ($node) =>
  new Promise((resolve) => {
    $node.imagesLoaded().always(resolve);
  });

/* eslint-disable fp/no-rest-parameters */
/* eslint-disable no-restricted-syntax */
/* eslint-disable fp/no-let */
/* eslint-disable fp/no-loops */
const interpolate = (literals, expressions) => {
  let string = ``;
  for (const [i, val] of expressions.entries()) {
    string += literals[i] + val;
  }
  string += literals[literals.length - 1];
  return string;
};

const trimPaddingSpaces = (string) => {
  const lines = string.split("\n").slice(1, -1);
  const nSpaces = lines[0].match("^ *")[0].length;
  return R.pipe(
    R.map((line) => line.slice(nSpaces)),
    R.join("\n")
  )(lines);
};

const trim = (literals, ...expressions) => {
  const string = interpolate(literals, expressions);
  return trimPaddingSpaces(string);
};

const pugCompile = (literals, ...expressions) => {
  let string = interpolate(literals, expressions);
  string = trimPaddingSpaces(string);
  return pug.compile(string);
};

const quickInfo = async (msg, level, autoDismiss = true, $target = null) => {
  $(".alert").remove();
  const html = `
    <div class="alert alert-dismissible alert-${level} fade show">
      <span>${msg}</span>
      <button class="close" type="button" data-dismiss="alert">
        <span>&times</span>
      </button>
    </div>
  `;
  const $alert = $(html).prependTo($target || $("body"));
  await raf();
  $alert.addClass("alert-displayed");
  if (autoDismiss) {
    await delay(2000);
    $alert.addClass("alert-removed");
    await delay(500);
    $alert.remove();
  }
};

// new positioning, included ",", e.g. "outside,start"
// vertical: outside / inside / top / start / center / end / bottom
// horizontal: outside / inside / left / start / center / end / right
// outside = top / bottom / left / right     (based on position)
// inside = start / end    (based on position)
const positionPrompt = ($form, nodeBox, direction = "outside,inside") => {
  const {width, height} = $form[0].getBoundingClientRect();
  const $window = $(window);
  const scrHeight = $window.height();
  const scrWidth = $window.width();
  if (direction === "center") {
    const top = scrHeight / 2 - height / 2;
    const left = scrWidth / 2 - width / 2;
    $form.css({
      position: "fixed",
      top: Math.max(2, top),
      left: Math.max(2, left),
    });
  } else {
    let top = nodeBox.top - 26;
    let left = 0;
    if (R.indexOf(",", direction) > -1) {
      const [inV, inH] = direction.split(",");
      const isLowerPart = nodeBox.top + nodeBox.height / 2 > scrHeight / 2;
      const v =
        {
          outside: isLowerPart ? "top" : "bottom",
          inside: isLowerPart ? "end" : "start",
        }[inV] || inV;
      const isRightPart = nodeBox.left + nodeBox.width / 2 > scrWidth / 2;
      const h =
        {
          outside: isRightPart ? "left" : "right",
          inside: isRightPart ? "end" : "start",
        }[inH] || inH;
      top = {
        top: nodeBox.top - height - 2,
        start: nodeBox.top,
        center: nodeBox.top + nodeBox.height / 2 - height / 2,
        end: nodeBox.bottom - height,
        bottom: nodeBox.top + nodeBox.height,
      }[v];
      left = {
        left: nodeBox.left - width - 2,
        start: nodeBox.left,
        center: nodeBox.left + nodeBox.width / 2 - width / 2,
        end: nodeBox.right - width,
        right: nodeBox.left + nodeBox.width,
      }[h];
    } else if (direction === "left") {
      // deprecated: historical cases
      left = nodeBox.left - width - 2;
    } else if (direction === "right") {
      left = nodeBox.left + nodeBox.width + 2;
    } else if (direction === "bottom") {
      left = nodeBox.left;
      top = nodeBox.top + nodeBox.height;
    } else if (direction === "select") {
      left = nodeBox.left - 1;
      top = nodeBox.top - 1;
      $form.css({width: nodeBox.width + 2});
    } else if (direction === "bottom-left") {
      left = nodeBox.left - width + nodeBox.width;
      top = nodeBox.top + nodeBox.height;
    }
    // handle off-screen
    const offset = $form.offsetParent()[0].getBoundingClientRect();
    top -= offset.top;
    left -= offset.left;
    if (top + height > scrHeight) {
      top = scrHeight - height - 2;
    }
    if (left + width > scrWidth) {
      left = scrWidth - width - 2;
    }
    $form.css({top: Math.max(2, top), left: Math.max(2, left)});
  }
};

const uidPrompt = ({
  $wrapper,
  $node,
  html,
  onReady,
  direction = "right",
  alwaysFloat,
  dismissOnOutsideClick = false,
  autoDismissInSecs = false,
  backdropNoFade = false,
  transparentBackdrop = false,
}) =>
  new Promise((resolve) => {
    const $body = $wrapper || $("body");
    $body.addClass("uid-prompt-openned");
    // const $body = $node.closest(".app-initialized,body");
    const nodeBox = $node[0].getBoundingClientRect();
    const canvasClasses = [
      "up-canvas",
      alwaysFloat ? "always-float" : "",
      `toward-${direction}`,
    ].join(" ");
    const $backdrop = $(`
      <div class="up-backdrop">
        <div class="${canvasClasses}" data-dir="${direction}">
          ${html}
        </div>
      </div>
    `).appendTo($body);

    if (backdropNoFade || transparentBackdrop) {
      $backdrop.addClass("no-animation");
    }
    if (transparentBackdrop) {
      $backdrop.addClass("transparent");
    }
    const $form = $backdrop.children(".up-canvas");

    const dismissAndResolve = (action) => {
      const reloadOnClose = $backdrop.data("reloadOnNavframeClose");
      $(window).off("scroll.position-uid-prompt");
      $backdrop.off("mousedown.dismiss-uid-prompt-on-outside-click");
      $form.parent().addClass("dismissed");
      if ($("> .up-backdrop:not(.dismissed)", $body).length === 0) {
        $body.removeClass("uid-prompt-openned");
      }
      resolve({$form, action});
      delay(200).then(() => {
        if (reloadOnClose) {
          if (reloadOnClose === true) {
            window.navReload();
          } else {
            window.navReload(reloadOnClose);
          }
        }
        $form.parent().remove();
      });
    };

    $backdrop.data("dismissAndResolve", dismissAndResolve);

    if (dismissOnOutsideClick) {
      $backdrop.on("mouseup.dismiss-uid-prompt-on-outside-click", (e) => {
        if ($("body").hasClass("dragging")) return;
        if ($(e.target).is($backdrop)) {
          dismissAndResolve(dismissOnOutsideClick);
        }
      });
    }
    $form.on("click.uid-prompt-resolve", "[data-resolve]", (e) => {
      const $btn = $(e.currentTarget);
      if ($btn.closest(".up-canvas").is($form)) {
        dismissAndResolve($btn.data("resolve"));
      }
    });
    $form.on("submit.uid-prompt-resolve", "form[data-submit-resolve]", (e) => {
      const $that = $(e.currentTarget);
      if ($that.closest(".up-canvas").is($form)) {
        e.preventDefault();
        e.stopPropagation();
        $that.trigger("vf-validate");
        if (!$that.hasClass("vf-invalid")) {
          dismissAndResolve($(e.currentTarget).data("submit-resolve"));
        }
      }
    });

    untilImagesLoaded($backdrop).then(() => {
      positionPrompt($form, nodeBox, direction);
      $(window).on("scroll.position-uid-prompt", () => {
        untilImagesLoaded($backdrop).then(() => {
          if ($node.closest("body").length > 0) {
            const newBox = $node[0].getBoundingClientRect();
            positionPrompt($form, newBox, direction);
          }
        });
      });
      window.requestAnimationFrame(async () => {
        $form.parent().addClass(`displayed`);
        $form.removeClass(`toward-${direction}`);
        $("[autofocus]:visible", $form).focus();
        $("[data-press-enter-to-resolve]", $form).each((i, node) => {
          $(node).on("keyup", (e) => {
            if (e.key === "Enter") {
              const action = $(node).data("press-enter-to-resolve");
              dismissAndResolve(action);
            }
          });
        });
        if (onReady) {
          onReady($form, dismissAndResolve);
        }
        $("body").trigger("statechanged");
        $form.on("mousedown.uid-prompt-move", "h1, h2, h3, h4, h5", (e) => {
          if (e.which !== 1) return true;
          const $header = $(e.currentTarget);
          if (!$header.is($("h1, h2, h3, h4, h5", $form).first())) return true;
          const {top, left} = $form.position();
          $form.addClass("moving");
          const onMove = (me) => {
            const dx = me.clientX - e.clientX;
            const dy = me.clientY - e.clientY;
            $(window).off("scroll.position-uid-prompt");
            $form.css({
              top: top + dy,
              left: left + dx,
            });
          };
          $body.on("mousemove.uid-prompt-move", onMove);
          $(document).on("mouseup.uid-prompt-move", () => {
            $body.off("mousemove.uid-prompt-move");
            $(document).off("mouseup.uid-prompt-move");
            $form.removeClass("moving");
          });
          return false;
        });
        if (autoDismissInSecs) {
          setTimeout(() => {
            dismissAndResolve("cancel");
          }, autoDismissInSecs * 1000);
        }
      });
    });
  });

const padSpacesToLines = (padStr, lines, sliceFirstLine = false) =>
  R.pipe(
    R.split("\n"),
    R.map((l) => `${padStr}${l}`),
    R.join("\n"),
    (output) => (sliceFirstLine ? output.slice(padStr.length) : output)
  )(lines);

const uid = (mixin, args, noDev, globals = {}) => {
  const assetsContext = $("[data-assets-context]").data("assets-context");
  const appVersion = $("body").data("app-version");
  if (appVersion === "dev" && noDev !== true) {
    console.log("render mixin", mixin);
    console.log("with args", args);
    // send mixin signal to uid server
    const formData = new FormData();
    formData.set("payload", JSON.stringify({mixin, params: args}));
    fetch("/_/open-mixin", {method: "post", body: formData});
  }
  const baseUrl = $("[data-base-url]").data("base-url");
  const isMobile = frontendHelpers.getIsMobile();
  return clientIncludes(
    R.merge(frontendHelpers, {
      asset: (filename) => `${assetsContext}/${filename}`,
      mixin,
      args,
      appVersion,
      baseUrl,
      isMobile,
      ...globals,
    })
  );
};

const handleMultipleInput = ($prompt, label, defaultOption) => {
  const addItem = () => {
    const html = uid(`one-${label}`, defaultOption);
    $(html)
      .appendTo($(`.${label}-wrap`, $prompt))
      .find("input")
      .first()
      .focus();
  };
  $prompt.on(`click.add-${label}`, `[data-add-${label}]`, addItem);
  $prompt.on(
    `keydown.add-${label}-on-enter`,
    `[data-add-${label}-on-enter]`,
    (e) => {
      if (e.which === 13) {
        addItem();
        return false;
      }
    }
  );
  $prompt.on(`click.remove-${label}`, `[data-remove-${label}]`, (e) => {
    $(e.currentTarget).closest(`.one-${label}-grid`).remove();
    return false;
  });
};

const initUIDExposedMixins = R.pipe(
  ($body) => $("i.uid-mixin", $body).toArray(),
  R.map((node) => {
    const {name, content} = $(node).data();
    return `-\n  pug_mixins["${name}"] = ${padSpacesToLines(
      "    ",
      content,
      true
    )}`;
  }),
  R.append("+#{mixin}(args)"),
  (str) => {
    const assetsContext = $("[data-assets-context]").data("assets-context");
    return R.prepend(
      `- var asset = (filename) => "${assetsContext}/" + filename;`,
      str
    );
  },
  R.join("\n"),
  (mixins) => pug.compile(mixins),
  (fn) =>
    $("body").data("app-version") === "dev"
      ? (mixin, args, helpers = {}) => {
          console.log(`render mixin: ${mixin}`);
          console.log(`args: ${JSON.stringify(args)}`);
          console.warn(
            "initUIDExposedMixins is deprecated, please use uid() instead"
          );
          return fn(R.mergeRight(helpers, {mixin, args, R}));
        }
      : (mixin, args, helpers) => fn(R.mergeRight(helpers, {mixin, args, R}))
);

// replace a dom node with uid result, apply caching to reduce dom manipulation and avoid flickering
const uidReplace = ($node, mixin, params) => {
  const html = uid(mixin, params, true);
  const hash = getHash(html);
  if (hash === $node.data("_uidHash")) return;
  const $newNode = $(html).data("_uidHash", hash);
  $node.replaceWith($newNode);
};

export {
  delay,
  raf,
  untilImagesLoaded,
  SLUG_PATTERN,
  pugCompile,
  trim,
  uidPrompt,
  quickInfo,
  uid,
  uidReplace,
  handleMultipleInput,
  // initUIDExposedMixins is deprecated, please use uid() instead
  initUIDExposedMixins,
};
