// devbase: ensure=always

import express from "express";
import socket from "socket.io";

import http from "http";

/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-unused-vars */
import * as R from "ramda";
import fs from "fs-extra";
import crypto from "crypto";
import md5File from "md5-file";
import "colors";
import chokidar from "chokidar";
import {CLIEngine} from "eslint";
import {exec} from "child_process";
import {logInfo} from "./devbase_helpers";

const babel = require("@babel/core");

const projectName = process.env.DB_NAME;

// modified from https://github.com/theoephraim/lint-fix-nodemon

// watches file changes
// run eslint (with fixes enabled)
// if no errors, run babel
const eslintCLI = new CLIEngine({
  fix: true,
  cache: true,
  ignorePath: ".gitignore",
  extensions: ["js"],
});
const formatter = eslintCLI.getFormatter();
/* eslint-disable fp/no-let */
/* eslint-disable fp/no-mutation */
let ignoreNextChange = false;

const runBabel = (filename) => {
  const opts = new babel.OptionManager().init({filename});
  if (R.isNil(opts)) return;
  opts.babelrc = false;
  opts.sourceMaps = "inline";
  opts.ast = false;
  const distFile = filename.replace("src", "dist");
  if (!fs.existsSync(distFile)) {
    fs.closeSync(fs.openSync(distFile, "w"));
  }
  babel.transformFile(filename, opts, (err, result) => {
    if (!result && !err) {
      console.error(`No result from Babel for file: ${filename}`);
      return;
    }
    if (err || !result) {
      console.error(`Babel compilation error: ${err}`);
      return;
    }
    const md5sum = crypto.createHash("md5").update(result.code).digest("hex");
    const dist = filename.replace(/^src/, "dist");
    const filemd5 = md5File.sync(dist);
    if (filemd5 === md5sum) return;

    fs.writeFileSync(dist, result.code);
    console.log(">>> babel done <<<".brightGreen.bold.underline);
  });
};

const runESLint = (path) => {
  const report = eslintCLI.executeOnFiles([path]);
  return report;
};

const lintAndRun = (path) => {
  if (ignoreNextChange) {
    ignoreNextChange = false;
    return;
  }
  const st = Number(new Date());
  console.log(`Linting ${path}`);
  const report = runESLint(path);
  CLIEngine.outputFixes(report);
  const results = formatter(report.results);
  const msecs = Number(new Date()) - st;
  if (`${results}` !== "") {
    console.log(results);
  } else {
    console.log(`>>> Lint Okay - ${msecs / 1000}s <<<`.brightGreen);
  }

  const fixed = R.any(R.has("output"), report.results);
  if (fixed) {
    ignoreNextChange = true;
    const cli = `nvr --servername /tmp/nvim/${projectName} -c 'call GracefulReload()'`;
    exec(cli, {});
  }

  const hasError = R.any((res) => res.errorCount > 0)(report.results);
  if (hasError) return;

  if (R.startsWith("src", path)) {
    runBabel(path);
  }
};

// watch files for changes, run linter and restart server
/* eslint-disable fp/no-mutating-methods */
chokidar
  .watch(["src/**/*.js", "test/**/*.js"], {
    ignored: [
      "node_modules",
      "src/index.dev-server.js",
      /(^|[/\\])\../, // ignore anything starting with .
    ],
  })
  .on("change", (path, _stats) => {
    // console.log(path);
    lintAndRun(path);
  });

const app = express();

app.get("/dev-server", (req, res) => {
  app.io.emit("msg", req.query);
  res.json({ok: 1});
});

// catch 404 and error
app.use((_req, res) => {
  res.send("404 not found");
});
app.use((err, _req, res) => {
  console.log(err);
  res.send("500 server error");
});

/**
 * Event listener for HTTP server "error" event.
 */
const onError = (error) => {
  if (error.syscall !== "listen") {
    throw error;
  }
  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(`Socket requires elevated privileges`);
      break;
    case "EADDRINUSE":
      console.error(`Port is already in use`);
      break;
    default:
      throw error;
  }
  process.exit(1);
};

app.set("port", 3001);
const server = http.createServer(app);
app.io = socket(server, {
  path: "/dev-server",
  serveClient: true,
});
app.io.on("connection", () => {
  logInfo("devbase chrome extension connected");
});
server.listen(3001);
server.on("error", onError);
server.on("listening", async () => {
  const addr = server.address();
  logInfo(`devbase dev server listened ${JSON.stringify(addr)}`);
});
