import * as R from "ramda";
import {initNavyAppFramework} from "./frontend/navy-app-framework";
import {initLayoutDragger} from "./frontend/splitpane";
import {initVF} from "./frontend/vf";
import {initNav, fetchPost, getBaseUrl} from "./frontend/nav";
import {initTableExt} from "./frontend/table-ext";
import {initSubmitOn} from "./frontend/submit-on";
import {initRadioToggle} from "./frontend/radio-toggle";
import {
  formatDateTime,
  formatDateTimeZone,
  getIsMobile,
} from "./frontend-helpers";

/* eslint-disable func-names */
/* eslint-disable jquery/no-class, jquery/no-val */
/* eslint-disable jquery/no-attr, jquery/no-prop, jquery/no-css, jquery/no-parents */
/* eslint-disable jquery/no-text, jquery/no-html, jquery/no-ready, jquery/no-submit */

import {uidPrompt, uid, quickInfo} from "./frontend/uid-prompt";

// ##devbase-anchor naf-import

const appInitializers = {
  // ##devbase-anchor naf-load
};

const $body = $("body");

if ($body.data("app-version") === "dev") {
  window.onerror = (msg) => {
    $(`<div>Uncaught Error: ${msg}</div>`)
      .css({
        "text-align": "center",
        "background-color": "red",
        color: "white",
        "font-size": "2rem",
      })
      .prependTo($body);
  };
}

const delay = (ms) =>
  new Promise((resolve) => {
    setTimeout(resolve, ms);
  });

$(() => {
  initNavyAppFramework(appInitializers);
  initLayoutDragger();
  initVF();
  // suggest turn this on for backend, or js-heavy app
  // but turn off for public frontend
  // see nav.js for details
  initNav();
  initTableExt();
  initSubmitOn();
  initRadioToggle();

  $body.on("statechanged", () => {
    $(".alert-dismissible", $body).each(async (i, node) => {
      const $alert = $(node);
      $alert.addClass("alert-displayed");
      await delay(3000);
      if ($alert.hasClass("alert-primary")) {
        // auto-dismiss only for primary alerts
        $alert.addClass("alert-removed");
        await delay(500);
        $alert.remove();
      }
    });

    $("[data-format-datetime]", $body).each((i, node) => {
      const $node = $(node);
      const d = new Date($node.data("format-datetime"));
      $node.text(formatDateTime(d));
      $node.removeAttr("data-format-datetime");
    });

    $("[data-format-datetimezone]", $body).each((i, node) => {
      const $node = $(node);
      const d = new Date($node.data("format-datetimezone"));
      $node.text(formatDateTimeZone(d));
      $node.removeAttr("data-format-datetimezone");
    });

    $(".init-litepicker", $body).each((i, node) => {
      const $picker = $(node).removeClass("init-litepicker");
      const config = $picker.data("config") || {};
      const lockDaysFilter = config.dates
        ? (date1) => !R.includes(date1.format("YYYY.MM.DD"), config.dates)
        : config.range
        ? (date1) => {
            const dstr = date1.format("YYYY.MM.DD");
            return dstr < config.range[0] || dstr > config.range[1];
          }
        : null;
      const picker = new Litepicker(
        R.merge(
          {
            format: "YYYY.MM.DD",
            element: $picker[0],
            inlineMode: config.inline !== false,
            firstDay: 0,
            numberOfMonths: 1,
            numberOfColumns: 1,
            lockDaysFilter,
            setup: (p) => {
              p.on("before:show", () => {
                if (config.dates && !R.isEmpty(config.dates)) {
                  const today = new Date();
                  const closest = R.reduce(
                    R.minBy((date) => Math.abs(new Date(date) - today)),
                    config.dates[0],
                    config.dates
                  );
                  p.gotoDate(closest);
                } else if (config.range) {
                  p.gotoDate(config.range[0]);
                }
              });
              p.on("selected", () => {
                $picker.change();
                if (config.resolveOnChanged) {
                  $picker.closest(".up-backdrop").data("dismissAndResolve")(
                    "submitted"
                  );
                }
                if (config.destroyOnChanged) {
                  picker.destroy();
                }
              });
              p.on("clear:selection", () => {
                $picker.change();
                if (config.destroyOnChanged) {
                  picker.destroy();
                }
              });
            },
          },
          config
        )
      );
      $picker.data("picker", picker);

      const bindToQuery = $picker.data("bind-query");
      if (bindToQuery) {
        $picker.on("change.bind-picker-to-query", () => {
          const val = $picker.val();
          window.navFetchAndRender({
            fetchUrl: `?${bindToQuery}=${val}`,
            pushState: false,
          });
        });
      }
      // re-position uid prompt
      $(window).trigger("scroll.position-uid-prompt");
    });

    /* eslint-disable fp/no-this */
    /* eslint-disable fp/no-let */
    $("[data-auto-trigger]:not(.auto-triggered)").each(function () {
      const data = $(this).data();
      let interval = 1;
      if (data.autoTriggerDelay) {
        interval = data.autoTriggerDelay * 1000;
      }
      if (data.autoTrigger === "select") {
        data.autoTrigger = "change";
      } else if (data.autoTrigger === "focusout") {
        data.autoTrigger = "focusout";
      } else if (data.autoTrigger === "click-and-dismiss") {
        data.autoTrigger = "click-and-dismiss";
      } else {
        data.autoTrigger = "click";
      }
      const $obj = $(this);
      setTimeout(() => {
        if (data.autoTriggerVisibleOnly && !$obj.is(":visible")) {
          return false;
        }
        if (data.autoTrigger === "click-and-dismiss") {
          $obj[0].click();
          $(".general-modal").modal("hide");
        } else if (data.autoTrigger === "click") {
          $obj[0].click();
        } else {
          $obj.trigger(data.autoTrigger);
        }
        $obj.addClass("auto-triggered");
        return true;
      }, interval);
    });

    $("input[autohighlight]", $body).each((i, node) => {
      $(node).select().removeAttr("autohighlight");
    });
  });

  $body.on("click.toggle-password", "[data-toggle-password]", (e) => {
    const $node = $(e.currentTarget);
    const $pass = $($node.data("toggle-password"));
    const currentType = $pass.prop("type");
    if (currentType === "password") {
      $node.text("Hide password");
      $pass.prop("type", "text");
    } else {
      $node.text("Show password");
      $pass.prop("type", "password");
    }
  });

  $body.on("click.dismiss-alert", "[data-dismiss='alert']", (e) => {
    const $node = $(e.currentTarget);
    $node.parent().remove();
  });

  $body.on("click.delete-dom", "[data-delete-dom]", () => {
    const $this = $(this);
    $($this.data("delete-dom")).remove();
  });

  $body.on("click.confirm", "[data-confirm]", async (e) => {
    const $node = $(e.currentTarget);
    if (!$node.hasClass("confirmed")) {
      e.stopImmediatePropagation();
      e.stopPropagation();
      e.preventDefault();
      $node.addClass("hidden-mobile");
      const question = $node.data("confirm");
      const {action} = await uidPrompt({
        $node,
        html: uid("p-confirm", {question}),
        direction: "center",
      });
      $node.removeClass("hidden-mobile");
      if (action === "confirm") {
        $node.addClass("confirmed");
        $node[0].click();
      }
    }
    $node.removeClass("confirmed");
    return true;
  });

  // submenu
  const openSubmenu = async (node) => {
    const $node = $(node);
    const {
      wrapper,
      mixin,
      dir,
      extraData,
      alwaysFloat,
      data = {},
      dismissOnOutsideClick = "--cancel",
    } = $node.data("submenu");
    const promptConfig = {};
    const $navFrame = $node.closest("[data-nav-frame]");
    if (wrapper === "naf") {
      promptConfig.$wrapper = $node.closest("[data-naf-code]");
    } else if ($navFrame.length > 0) {
      promptConfig.$wrapper = $navFrame;
    }
    const promptData = extraData
      ? R.mergeRight(data, await (await fetch(extraData)).json())
      : data;
    await uidPrompt({
      $node,
      html: uid(mixin, promptData, false, {baseUrl: getBaseUrl($node)}),
      direction: dir || "outside,inside",
      alwaysFloat,
      // transparentBackdrop: true,
      dismissOnOutsideClick,
      ...promptConfig,
    });
  };
  $body.on("submit.submenu", "form[data-submenu]", async (e) =>
    openSubmenu(e.currentTarget)
  );
  $body.on("click.submenu", "[data-submenu]:not(form)", async (e) =>
    openSubmenu(e.currentTarget)
  );

  // navframe data api
  const openNavframeSubmenu = async (node) => {
    const $node = $(node);
    const $wrapper =
      $node.closest("[data-naf-code]").length > 0
        ? $node.closest("[data-naf-code]")
        : false;
    const {
      name: frameName,
      url,
      dir = "center",
      msg = "Loading data ...",
      width = 600,
      style = false,
      outside = false,
    } = $node.data("navframe");
    const isMobile = getIsMobile();
    const promptData = {
      name: frameName,
      url,
      mixin: "generic-msg",
      mixinParams: {msg, wip: true},
      style:
        style || (isMobile ? "" : `width: 100vw; max-width: ${width / 16}rem`),
    };
    await uidPrompt({
      $wrapper,
      $node,
      html: uid("p-navframe", promptData, true, {baseUrl: getBaseUrl($node)}),
      direction: dir,
      dismissOnOutsideClick: outside,
    });
  };
  $body.on("submit.navframe-submenu", "form[data-navframe]", async (e) =>
    openNavframeSubmenu(e.currentTarget)
  );
  $body.on("click.navframe-submenu", "[data-navframe]:not(form)", async (e) =>
    openNavframeSubmenu(e.currentTarget)
  );

  $(".auto-trigger[data-submenu]", $body).each((i, node) => {
    $(node).click().remove();
  });

  // generic reorder
  $body.on("te-move-row", "[data-te-move-row]", async (e, result) => {
    const $node = $(e.currentTarget);
    const $wrap = $node.closest("[data-te-move-row-url]");
    if ($wrap.length > 0) {
      const url = $wrap.data("te-move-row-url");
      await window.navFetchAndRender({
        fetchFn: fetchPost(url, result),
        pushState: false,
        delayMS: 0,
        needLoader: true,
      });
    }
  });

  // click to copy
  $body.on("click.click-to-copy", "[data-click-to-copy]", async (e) => {
    const $node = $(e.currentTarget);
    const target = $node.data("click-to-copy");
    const execCopy = async (input) => {
      input.select();
      input.setSelectionRange(0, 99999); // For mobile devices
      document.execCommand("copy");
      quickInfo("Text copied.", "info");
    };
    if (R.startsWith("text:", target)) {
      const $target = $("<input>")
        .css("max-height", "0px")
        .appendTo("body")
        .val(target.split("text:")[1]);
      execCopy($target[0]);
      $target.remove();
    } else if ($(target).length > 0) {
      execCopy(target);
    }
  });

  $body.on("change.batch-ops", "[data-batch-ops]", async (e) => {
    const $node = $(e.currentTarget);
    const query = $node.data("batch-ops");
    const $ops = $(query, $body);
    const $checked = $(`[data-batch-ops="${query}"]:checked`, $body);
    if ($checked.length > 0) {
      const ids = $checked
        .map((i, node) => $(node).val())
        .toArray()
        .join(",");
      const {fillInput, fillSubmenuData} = $node.data();
      if (fillInput) {
        $(fillInput, $ops).each((i, node) => {
          $(node).val(ids);
        });
      }
      if (fillSubmenuData) {
        $(fillSubmenuData, $ops).each((i, node) => {
          $(node).data("submenu").data.ids = ids;
        });
      }
      $ops.removeClass("hidden");
    } else {
      $ops.addClass("hidden");
    }
  });

  $body.on("click.batch-ops-all", "[data-batch-ops-all]", async (e) => {
    const $node = $(e.currentTarget);
    const query = $node.data("batch-ops-all");
    const $boxes = $(`[data-batch-ops="${query}"]`, $body);
    $boxes.prop("checked", "checked").change();
  });

  $body.on("click.batch-ops-none", "[data-batch-ops-none]", async (e) => {
    const $node = $(e.currentTarget);
    const query = $node.data("batch-ops-none");
    const $boxes = $(`[data-batch-ops="${query}"]`, $body);
    $boxes.prop("checked", false).change();
  });

  $body.on("click.check-all-within", "[data-check-all-within]", async (e) => {
    const $node = $(e.currentTarget);
    const query = $node.data("check-all-within");
    const $wrapper = $(query, $body);
    $('input[type="checkbox"]', $wrapper).prop("checked", true);
  });

  $body.on("click.check-none-within", "[data-check-none-within]", async (e) => {
    const $node = $(e.currentTarget);
    const query = $node.data("check-none-within");
    const $wrapper = $(query, $body);
    $('input[type="checkbox"]', $wrapper).prop("checked", false);
  });

  $body.trigger("statechanged");
});
