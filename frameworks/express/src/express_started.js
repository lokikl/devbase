// devbase: ensure=always

// import i18next from "i18next";
import pug from "pug";
import fs from "fs-extra";
import {exec} from "child_process";
import {logInfo, logError} from "./devbase_helpers";

const execP = (cli, options) =>
  new Promise((resolve, reject) => {
    exec(cli, options, (error, stdout) => {
      if (error) {
        console.log("error.message", error.message);
      }
      return error ? reject(error.message) : resolve(stdout);
    });
  });

export default async (/* server */) => {
  logInfo("pre-cache views (warm up)");
  pug.compileFile("/app/src/views/uid.pug", {cache: true});

  if (process.env.APP_VERSION === "dev") {
    const dbSeeded = fs.existsSync(".dbchecksum");
    if (dbSeeded === false) {
      logError("Database not seeded, please resetdb");
    } else {
      const actual = await execP(
        `find db -type f -exec md5sum {} \\; | md5sum`
      );
      const seeded = fs.readFileSync(".dbchecksum").toString();
      if (actual !== seeded) {
        logError("Database outdated, please resetdb");
      }
    }
  }
};
