// used in uid
import * as R from "ramda";

export {R};

export const keyize = (name) =>
  name
    .toLowerCase()
    .replace(/[^\w]+/g, "-")
    .replace(/-$/, "");

export const capitalize = (name) =>
  name.charAt(0).toUpperCase() + name.slice(1);

export const titleize = R.pipe(
  R.split(/[\s-_]/),
  R.map((str) => capitalize(str)),
  R.join(" ")
);

export const formatTimeHMS = (d) =>
  R.join(":", [
    d.getHours(),
    String(d.getMinutes()).padStart(2, "0"),
    String(d.getSeconds()).padStart(2, "0"),
  ]);

// 11:59
export const formatTime = (d) =>
  `${d.getHours()}:${String(d.getMinutes()).padStart(2, "0")}`;

// 2021.03.16
export const formatDate = (d) => {
  const dd = String(d.getDate()).padStart(2, "0");
  const mm = String(d.getMonth() + 1).padStart(2, "0"); // January is 0!
  return `${d.getFullYear()}.${mm}.${dd}`;
};

export const addDays = (days, date) => {
  const d = new Date(date);
  d.setDate(d.getDate() + days);
  return d;
};

export const addMonths = (months, date) => {
  const d = new Date(date);
  d.setMonth(d.getMonth() + months);
  return d;
};

// 2021.03
export const formatMonth = (d) => formatDate(d).replace(/.\d\d$/, "");

// 2021.03.16 11:59
export const formatDateTime = (d) => `${formatDate(d)} ${formatTime(d)}`;

// 2021.03.16 11:59 HKT
export const formatDateTimeZone = (d) => {
  const tz = new Date()
    .toLocaleTimeString("en-us", {timeZoneName: "short"})
    .split(" ")[2];
  return `${formatDateTime(d)} ${tz}`;
};

export const formatPrice = (number) =>
  number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

export const getIsMobile = () => $(".detect-rwd").data("current") === "mobile";
