// devbase: ensure=always
import * as R from "ramda";
import chalk from "chalk";

const getEnv = (key, defaultValue = "") => {
  const value = process.env[key];
  return R.isNil(value) ? defaultValue : value;
};

const go = (promise) =>
  promise.then((data) => [null, data]).catch((err) => [err]);

const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const isDev = process.env.APP_VERSION === "dev";

const logInfoDev = (msg) =>
  console.log(`INFO  ${new Date().toLocaleString()} ${chalk.dim(msg)}`);

const logErrorDev = (msg) =>
  console.error(`ERROR ${new Date().toLocaleString()} ${chalk.red(msg)}`);

const logInfoProd = (msg) =>
  console.log(`INFO  ${new Date().toLocaleString()} ${msg}`);

const logErrorProd = (msg) =>
  console.error(`ERROR ${new Date().toLocaleString()} ${msg}`);

const logInfo = isDev ? logInfoDev : logInfoProd;
const logError = isDev ? logErrorDev : logErrorProd;

export {getEnv, go, delay, logInfo, logError};
