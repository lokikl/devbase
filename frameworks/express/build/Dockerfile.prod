# ==========================================
# stage 1: prepare base image
# ==========================================
FROM node:15.14-stretch AS baseimg

# msjh is for receipt generation (phantomjs and chinese rendering)
# ping is required by memcached and psql client for reconnecting
RUN \
  apt-get update && \
  apt-get install -y zip ca-certificates curl iputils-ping telnet iproute2 && \
  rm -rf /var/lib/apt/lists/* && \
  npm install -g --force yarn && \
  yarn set version berry


# ==========================================
# stage 2: node modules & assets
# ==========================================
FROM baseimg AS stage2

WORKDIR /app

RUN \
  apt-get update && \
  apt-get install -y python build-essential && \
  rm -rf /var/lib/apt/lists/*

# Install all dependencies includes dev
COPY package.json yarn.lock .yarnrc.yml ./
COPY .yarn .yarn
RUN yarn install --immutable

# Build server side and client side js and css
ENV NODE_ENV production
COPY . .
RUN \
  # compile server side js in dist
  yarn build && \
  # compile css in dist/public
  yarn run node-sass --output-style=compressed src/assets/scss/app.scss dist/public/app.css && \
  # compile js in dist/public
  yarn run webpack --mode=production && \
  npm prune --production

# ==========================================
# stage 3: composite the prod image
# ==========================================
FROM baseimg

WORKDIR /app

ENV NODE_ENV production

# Build application
COPY --from=stage2 /app/node_modules /app/node_modules
COPY --from=stage2 /app/dist /app/dist
COPY package.json .yarnrc.yml yarn.lock knexfile.js ./
COPY .yarn/releases .yarn/releases
COPY .devbase .devbase
COPY db db
COPY ./src/assets/static ./dist/public/static
COPY ./src/assets/static/vendor.js ./src/assets/static/vendor.css ./dist/public/
COPY ./src/views ./src/views
COPY ./src/uid ./src/uid
COPY ./src/uid/global.css ./dist/public/global.css

ARG APP_VERSION
ENV APP_VERSION $APP_VERSION

# random assets context for bypassing the cloudflare cache
ARG ASSETS_CONTEXT
ENV ASSETS_CONTEXT $ASSETS_CONTEXT

# Expose default port
EXPOSE 3000

ENTRYPOINT ["/app/.devbase/boot.sh"]
