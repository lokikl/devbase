// devbase: ensure=on-bootstrap
// naming conventions: https://launchbylunch.com/posts/2014/Feb/16/sql-naming-conventions/

const {knexHelpers} = require("../helpers");

exports.up = async (knex) => {
  const {restrictOptions, autoId, autoUpdatedAt} = knexHelpers(knex);

  // account table
  await knex.schema.createTable("account", (table) => {
    table.string("id").primary();
    table.string("name").notNull().unique();
    table.string("type", 20).notNull();
    table.string("state");
    table.jsonb("config").defaultTo({});
    table.jsonb("payment").defaultTo({});

    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  });
  await autoId("account");
  await autoUpdatedAt("account");
  await restrictOptions("account", "type", [
    "platform",
    "reseller",
    "customer",
  ]);
  await restrictOptions("account", "state", ["Active", "Inactive"]);

  // user_profile table
  await knex.schema.createTable("user_profile", (table) => {
    table.string("id").primary();
    table.string("name").notNull();
    table.string("email").unique().notNull();
    table.string("password_hash").notNull();
    table.string("secret_key");
    table.string("state");
    table.jsonb("config").defaultTo({});

    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  });
  await autoId("user_profile");
  await autoUpdatedAt("user_profile");
  await restrictOptions("user_profile", "state", [
    "Verifying Email",
    "Active",
    "Inactive",
  ]);

  // auto generate secret key for ws authenitcation
  await knex.raw(`
    create or replace function gen_user_profile_secret_key() returns trigger
    language 'plpgsql'
    as $$
      begin
        new.secret_key = new_token(32);
        return new;
      end;
    $$;
    drop trigger if exists trigger_user_profile_secret_key on user_profile;
    create trigger trigger_user_profile_secret_key
      before insert on user_profile
      for each row execute procedure gen_user_profile_secret_key()
  `);

  // account_member table
  await knex.schema.createTable("account_member", (table) => {
    table.string("user_profile_id").references("user_profile.id").notNull();
    table.string("account_id").references("account.id").notNull();
    table.primary(["user_profile_id", "account_id"]);

    table.string("state");
    table.jsonb("permissions").defaultTo([]);
    table.jsonb("config").defaultTo({});
    table.timestamp("created_at").defaultTo(knex.fn.now());
  });
  await restrictOptions("account_member", "state", ["Active", "Inviting"]);

  // check if at least one user has full management
  await knex.raw(`
    create or replace function ensure_at_least_one_owner() returns trigger
    language 'plpgsql'
    as $$
      declare owner_count int;
      begin
        select count(1)
        from account_member
        where account_id = coalesce(old.account_id, new.account_id)
        and state = 'Active'
        and permissions \\? 'Owner'
        into owner_count;

        if owner_count = 0 then
          raise exception 'account must has at least one owner';
        end if;

        return new;
      end;
    $$;
    drop trigger if exists trigger_user_profile_secret_key on account_member;
    create trigger trigger_user_profile_secret_key
      after insert or update or delete on account_member
      for each row execute procedure ensure_at_least_one_owner()
  `);

  return true;
};

exports.down = async (knex) => {
  await knex.schema.dropTableIfExists("account_member");
  await knex.schema.dropTableIfExists("user_profile");
  await knex.schema.dropTableIfExists("account");
  return true;
};
