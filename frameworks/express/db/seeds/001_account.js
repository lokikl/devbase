// devbase: ensure=on-bootstrap

const R = require("ramda");

const {readRecordsFromYAML, addIncrementalTime} = require("../helpers");

const crypto = require("../../dist/transport/crypto");

exports.seed = async (knex) => {
  const addPassword = R.assoc(
    "password_hash",
    crypto.sha512hmac("Password321$")
  );

  const Records = await readRecordsFromYAML("initial-account.yaml");

  await knex("account").insert(
    addIncrementalTime("created_at")(Records.account)
  );

  const users = await R.pipe(
    addIncrementalTime("created_at"),
    R.map(addPassword)
  )(Records.user_profile);

  await knex("user_profile").insert(users);

  await knex("account_member").insert(
    addIncrementalTime("created_at")(Records.account_member)
  );

  return true;
};
