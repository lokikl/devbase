// devbase: ensure=on-bootstrap

const R = require("ramda");

const {readRecordsFromYAML, addIncrementalTime} = require("../helpers");

const seqRes = R.reduce((last, p) => last.then(R.always(p)), Promise.resolve());

exports.seed = async (knex) => {
  await knex.transaction(async (trx) => {
    const tbls = [
      // add your seed tbl
    ];

    // create the account 2 days before for developing billing
    const lastAt = new Date(new Date() - 2 * 24 * 60 * 60 * 1000);

    for (const tbl of tbls) {
      let {[tbl]: records} = await readRecordsFromYAML(`initial-table-${tbl}.yaml`);

      const timeFields = R.filter(R.endsWith("_at"), R.keys(records[0]));

      for (const tf of timeFields) {
        records = addIncrementalTime(tf, lastAt)(records);
      }

      await trx(tbl).insert(records);
    }
  });

  return true;
};
