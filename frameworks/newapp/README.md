### Dev. Env. setup

vim + tmux is the most optimized way to develop on this project.

We recommend leveraging [Loki's setup](https://github.com/lokikl/nvim-config).
Otherwise, please apply the following in your own vim setup to source all the required key mappings.

    au VimEnter * call LoadProjectVimRC()
    function! LoadProjectVimRC()
      if filereadable(".devbase/project.vim.lua")
        luafile .devbase/project.vim.lua
      endif
    endfunction
    au! BufWritePost project.vim.lua luafile %

### Booting up

First, install direnv to initialize envvar and temp folders.

    sudo apt install direnv
    direnv allow .

Create the compose environment with `reboot-dev` in your console or by `<leader>r` in vim.

Press `rl` in vim to open the log viewer.

Migrate the DB and generate seed data with `resetdb` in console or by `dr` in vim.

### Development

Most of the edit will be reflected automatically in browser. If you want to explicitly restart the app, following is the shortcut:

1. Refresh browser: `<space>oo`
1. Restart app server: `<space>or`
1. Restart workers: `<space>ow`
1. Restart dev-server: `<space>od`

To inspect the environment, you may use the following shortcuts:

- `<leader>o` open the website in your default browser
- `rl` for logs
- `rd` for the postgres database
- `co` for the app container console

### Architecture

This project follows a strict MVC architecture, leveraging various stacks and languages in each layer.

#### Model

PostgreSQL is used as the model layer and we utilize triggers and functions to achieve the following:

- Data lookup, aggregation, preprocessing, pagination
- Validation
- On change hooks
- Backup and restore

Long SQL queries are placed inside the controller to query and preprocess all data an API needs. Computationally intensive or memory-heavy tasks are delegated to PostgreSQL as it is considered more performant than NodeJS. Multiple queries are often merged to reduce network overhead.

Common workflows include:

- `dr` to quickly reset the entire database after any changes.
- `rd` to open the PostgreSQL console.
- `ss` in any SQL code block to execute inside the PostgreSQL console.
- `st` to search for table definitions.
- `sd` to search for migration or seed files.
- `gen-db-seed-yaml [table name]` (shell) to generate a seed file from current table.
- `recreate-db abc.sql` (shell) to recreate the database with a pg_dump file.

#### View

Views are pure functions and must not read or write additional data that is not explicitly provided. This means that rendering output can be redone or cached without side effects, which helps with auto-reloading in deeply nested situations. This also ensures complete information when adding non-web-based representation.

All views are defined as mixins written in the Pug language. They will be compiled and sent to the client's browser only when needed. The first time rendering is on the server-side, but subsequent renderings are done in client browsers.

Common workflows include:

- Use `sh` to search for views, ordered by the time they are rendered.
- Use `sc` to search for styles in SCSS, also ordered by the time rendered.
- Use `sj` to search for frontend JS.
- Use `alt-i` to toggle between Pug and SCSS.
- Once you edit a Pug/SCSS file, the browser auto-renders it.

Frontend JS and CSS are auto-compiled into app.js and app.css, and auto-reloaded in browser.

##### Third-party Codes (distributed as ES Modules)

1. Install with `yarn add --dev`.
1. Import into `frontend.js` or `frontend/*.js`, and let Webpack do the linking.

##### Third-party Codes (browser JS)

1. Install with `yarn add --dev` or download to the `vendor` folder.
1. Insert into `concat-vendor-js-css.sh`.
1. Run the script to pack them into vendor.js and vendor.css.

#### Controller

Controllers glue models and views together to provide logic. NodeJS is used because of its popularity. Logic should be constructed by function composing to have the maximum reusability.

Common workflows include:

- Use `se` to search for routes, ordered by the time consumed.
- Use `rl` to toggle the log pane.
- Use `rw` to replay the API closest to the cursor.
- Use `rr` to replay last replayed API.
- Use `rf` to forget the last replay. (otherwise, system will replay it on file change)
- Use `rc` to clear the log pane.

### Technologies

Following are several useful converged technologies built on top of this MVC framework which accelerate development:

- `fcsr`: Support both client and server side rendering with cached views.
- `navframe`: Support a virtual embedded browser which extends standard HTTP workflows.
- `tableext`: Support SQL to table with pagination, sorting, filtering, columns and page size control.
- `ssicon`: Auto derive minified SVG bundle by actual usage from multiple icon packs.

### Build production image

Run `build.prod` in console to build the production image. `cleanup-all-images` is for cleaning up them.
