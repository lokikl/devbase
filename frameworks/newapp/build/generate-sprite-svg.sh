#!/usr/bin/env bash

# devbase: ensure=always
# devbase: executable=yes

SRC="src/assets/static/sprite.svg"
TMP="/tmp/t.svg"
DST="$1"

used=`grep -R 'ssicon' src/mixins | cut -d'"' -f2 | grep -v mixin | sort | uniq`

head -n1 $SRC > $TMP

for i in $used; do
  grep "id=\"$i\"" $SRC >> $TMP
done

tail -n1 $SRC >> $TMP

dstmd5="-not-found-"
if [[ -f $DST ]]; then
  dstmd5=`md5sum $DST | awk '{print $1}'`
fi

tmpmd5=`md5sum $TMP | awk '{print $1}'`

if [[ "$dstmd5" != "$tmpmd5" ]]; then
  echo "* updating $DST"
  mv $TMP $DST
fi
