# step 1: build the executable
# ==========================================
FROM <%= project_name %>/dev as step1

# Build server side and client side js and css
ENV NODE_ENV production

RUN set -x &&\
  mkdir -p /root/.nexe &&\
  curl https://f.lokikl.com/nexe.linux.18.7.0.tar.gz | tar -xz -C /root/.nexe

WORKDIR /app

# Install all dependencies includes dev
COPY package.json yarn.lock .yarnrc.yml ./
COPY .yarn .yarn
RUN yarn install --immutable

COPY . .
RUN set -x &&\
  # server side js
  yarn run babel src -d dist --minified --no-comments &&\
  # app.js
  yarn run webpack --mode=production &&\
  # app.css
  find src/style -type f |\
    grep -v index.scss | sed '1s|^|src/style/index.scss\n|' |\
    xargs cat | yarn run node-sass --output-style compressed > dist/public/app.css &&\
  mkdir -p dist/public/static &&\
  mv ./src/assets/static/vendor.js ./src/assets/static/vendor.css ./dist/public &&\
  mv ./src/assets/static/fonts ./dist/public/static/fonts &&\
  mv ./src/assets/static/img ./dist/public/static/img &&\
  ./build/generate-sprite-svg.sh ./dist/public/static/sprite.min.svg &&\
  update-alternatives --install /usr/bin/python python /usr/bin/python3 1 &&\
  npm uninstall <%= project_name %> &&\
  npm prune --omit dev &&\
  mv node_modules/bcrypt/lib/binding/napi-v3/bcrypt_lib.node bcrypt_lib.node &&\
  npx nexe dist/index.nexe.js \
    --verbose \
    -r "node_modules/**/*" \
    -r "config/**/*" \
    -r "src/views/**/*" \
    -r "dist/**/*" \
    -r "src/mixins/**/*" \
    -r "db/**/*" \
    --build \
    --make="-j8" \
    --fakeArgv \
    -o <%= project_name %>-bin


# step 2: composite the output image
# ==========================================
FROM buildpack-deps:bullseye

# ping is required by memcached and psql client for reconnecting
# ldap-utils for ldapsearch for health check
RUN \
  apt-get update &&\
  apt-get install -y zip ca-certificates curl iputils-ping telnet iproute2 gosu &&\
  rm -rf /usr/share/lmod/lmod/modulefiles/Core &&\
  rm -rf /var/lib/apt/lists/*

ENV NODE_ENV production

WORKDIR /app

COPY --from=step1 /app/bcrypt_lib.node /app/node_modules/bcrypt/lib/binding/napi-v3/bcrypt_lib.node
COPY --from=step1 /app/<%= project_name %>-bin /app/<%= project_name %>-bin
COPY .devbase/boot.sh .devbase/

ARG APP_VERSION
ENV APP_VERSION $APP_VERSION

# random assets context to bypass CDN cache
ARG ASSETS_CONTEXT
ENV ASSETS_CONTEXT $ASSETS_CONTEXT

# Expose default port
EXPOSE 3000

ENTRYPOINT ["/app/.devbase/boot.sh"]
