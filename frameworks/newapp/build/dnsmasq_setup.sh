#!/bin/sh

# haproxyIp=$(nslookup lb | awk '/^Address: / { print $2 ; exit }')
nginxIp=$(nslookup web | awk '/^Address: / { print $2 ; exit }')

dnsmasq \
  --address=/.<%= project_name %>-dev.com/$nginxIp \
  --address=/^<%= project_name %>-dev.com/$nginxIp \
  --bogus-priv --domain-needed --no-resolv -k
