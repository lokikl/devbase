// devbase: ensure=on-bootstrap

const R = require("ramda");
const SC = require("../../dist/lib/sys-config");

const {readRecordsFromYAML, addIncrementalTime} = require("../helpers");

const seqRes = R.reduce((last, p) => last.then(R.always(p)), Promise.resolve());

exports.seed = async (knex) => {
  await knex.transaction(async (trx) => {
    const tbls = [
      "account",
      "user_profile",
      "membership",
    ];

    // create the account 2 days before for developing billing
    const lastAt = new Date(new Date() - 2 * 24 * 60 * 60 * 1000);

    for (const tbl of tbls) {
      const data = await readRecordsFromYAML(`initial-table-${tbl}.yaml`);
      let records = data[tbl];

      const timeFields = R.filter(R.endsWith("_at"), R.keys(records[0]));

      if (!data.skipTimeModification) {
        for (const tf of timeFields) {
          records = addIncrementalTime(tf, lastAt)(records);
        }
      }

      await trx(tbl).insert(records);
    }
  });

  return true;
};
