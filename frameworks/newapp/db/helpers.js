// devbase: ensure=always

const yaml = require("js-yaml");
const fs = require("fs-extra");
const R = require("ramda");

const knexHelpers = (knex) => {
  const patternCheck = (table, column, pattern) =>
    knex.schema.raw(`
      ALTER TABLE ${table}
        ADD CONSTRAINT ${column}_pattern
        CHECK (${column} ~ '${pattern}')
    `);

  const restrictOptions = (tableName, colName, options) =>
    patternCheck(tableName, colName, `^(${options.join("|")})$`);

  // will ensure unique
  const autoId = (table, length = 9) =>
    knex.raw(`
      create trigger trigger_${table}_id
        before insert on ${table}
        for each row execute procedure gen_id${length}()
    `);

  const autoDependentId = (table) =>
    knex.raw(`
      create trigger trigger_${table}_id
        before insert on ${table}
        for each row execute procedure dependent_serial_id()
    `);

  const minLength = (table, col, length) =>
    knex.raw(`
      alter table ${table}
      add constraint ${col}_min_length_${length} check (
        length(${col}) >= ${length}
      )
    `);

  const autoUpdatedAt = (table) =>
    knex.raw(`
      create trigger trigger_${table}_updated_at
        before update on ${table}
        for each row execute procedure set_updated_at()
    `);

  // lazy ranking
  const autoInsertRank = (table) =>
    knex.raw(`
      create trigger trigger_${table}_auto_insert_rank
      before insert on ${table}
      for each row
      execute procedure auto_insert_rank()
    `);

  const createMoveRankFunction = (table) =>
    knex.raw(`
      create
      or replace function move_rank_${table}(from_rank int, to_rank int) returns void
      language plpgsql
      as $$
        declare
          default_base constant int := 10000;
          interval constant int := 100;
          prev_rank int;
          rebalance_new_base int;
        begin
          -- find the rank before to_rank
          select
            rank into prev_rank
            from ${table}
            where rank < to_rank and rank != from_rank
            order by rank desc;

          if not found then
            -- move to top
            update ${table}
              set rank = to_rank - interval
              where rank = from_rank;
            return;

          elsif to_rank - prev_rank > 1 then
            -- move to the middle of the interval
            update ${table}
              set rank = (prev_rank + to_rank) / 2
              where rank = from_rank;
            return;
          end if;

          -- exhausted, rebalance
          -- determine the new base
          select
            case
              when count(1) * interval + default_base < min(rank) then default_base
              else max(rank) + interval
            end
            into rebalance_new_base
            from ${table};

          -- calculate new ranks of all rows, store into temp table
          create temp table temp_ranks
            on commit drop
            as select
              rank,
              rank() over(order by rank) * interval + rebalance_new_base as new_rank
              from ${table};

          -- apply the calculated new ranks
          update ${table}
            set rank = temp_ranks.new_rank
            from temp_ranks
            where ${table}.rank = temp_ranks.rank;

          -- recursively call move_rank
          perform move_rank_${table}(
            (select new_rank::int from temp_ranks where rank = from_rank),
            (select new_rank::int from temp_ranks where rank = to_rank)
          );
        end
      $$
    `);

  const enableLazyRanking = async (table) => {
    await autoInsertRank(table);
    await createMoveRankFunction(table);
  };

  const createEGridUpdater = async (tbl, jsonbField, topFields) => {
    const topFieldsCondition = R.isEmpty(topFields)
      ? "false"
      : R.pipe(
          R.map((field) => `'${field}'`),
          R.join(", "),
          (str) => `change->>'key' in (${str})`
        )(topFields);
    await knex.raw(`
      create
      or replace function update_${tbl}(changes jsonb) returns void
      language plpgsql
      as $$
        declare
          r record;
          change jsonb;
          tfquery text;
        begin
          for r in (select jsonb_array_elements(changes) change) loop
            change := r.change;

            if ${topFieldsCondition} then
              tfquery := case jsonb_typeof(change->'newVal')
                when 'number' then '$2::numeric'
                when 'boolean' then '$2::bool'
                else '$2#>>''{}'''
              end;

              execute '
                update ${tbl}
                  set ' || quote_ident(change->>'key') || ' = ' || tfquery || '
                  where id = $1
              ' using (change->>'pk'), (change->'newVal');

            else
              update ${tbl}
                set ${jsonbField} = jsonb_set(${jsonbField}, array[change->>'key'], change->'newVal')
                where id = change->>'pk';
            end if;
          end loop;
        end
      $$
    `);
    await knex.raw(`
      create
      or replace function reorder_${tbl}(changes jsonb) returns void
      language plpgsql
      as $$
        begin
          update ${tbl}
            set rank = inp.rank
          from (
            select
              value->>0 as id,
              (value->>1)::int as rank
            from jsonb_array_elements(changes) ranks
          ) as inp
          where ${tbl}.id = inp.id;
        end
      $$
    `);
  };

  const constraintJsonbType = (table, col, type) =>
    knex.raw(`
      alter table ${table} add constraint ${col}_must_be_${type}
      check (jsonb_typeof(${col}) = '${type}')
    `);

  return {
    patternCheck,
    restrictOptions,
    autoId,
    autoDependentId,
    minLength,
    autoUpdatedAt,
    autoInsertRank,
    createMoveRankFunction,
    enableLazyRanking,
    createEGridUpdater,
    constraintJsonbType,
  };
};

const arrValToJson = R.map(
  R.mapObjIndexed(
    R.cond([
      [R.is(Array), JSON.stringify],
      [R.T, R.identity],
    ])
  )
);

const readRecordsFromYAML = R.pipe(
  (filename) => `db/seeds/${filename}`,
  fs.readFile,
  R.andThen(yaml.load),
  R.andThen(R.ifElse(R.is(Array), arrValToJson, R.mapObjIndexed(arrValToJson)))
);

const mapIndexed = R.addIndex(R.map);

const addSecs = (secs, date) => new Date(date.getTime() + secs);

const addIncrementalTime =
  (col, lastAt = new Date()) =>
  (records) => {
    const startAt = addSecs(records.length * -1000, lastAt);
    return mapIndexed((r, idx) =>
      R.assoc(col, addSecs(idx * 1000, startAt), r)
    )(records);
  };

exports.knexHelpers = knexHelpers;
exports.readRecordsFromYAML = readRecordsFromYAML;
exports.addIncrementalTime = addIncrementalTime;
