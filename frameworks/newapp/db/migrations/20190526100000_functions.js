// devbase: ensure=always
const {knexHelpers} = require("../helpers");

exports.up = async (knex) => {
  const {autoUpdatedAt} = knexHelpers(knex);

  await knex.raw(`
    create extension if not exists "tablefunc";
    create extension if not exists "pgcrypto";
  `);

  // removed similar letters: i, l, 1, I, 0, O, D
  // so, in total 25 + 25 + 10 - 7 = base 55
  /* `
    select length(new_token(1000))
  ` */
  await knex.raw(`
    create or replace function new_token(length int)
    returns text
    language sql as $$
      SELECT string_agg(
        substr(
          'abcdefghjkmnopqrstuvwxyzABCEFGHJKLMNPQRSTUVWXYZ23456789',
          floor(random() * 54)::int + 1,
          1
        ),
        ''
      )
      FROM generate_series(1, length)
    $$
  `);

  // trigger which generate new_token(9) as id and has uniqueness check
  for (const length of [9, 12, 15, 16, 18, 20, 24, 28, 32, 64]) {
    await knex.raw(`
      create or replace function gen_id${length}() returns trigger
      language 'plpgsql' as $$
        declare
          qry constant text := 'select id from ' || quote_ident(tg_table_name) || ' where id=';
          key text;
          found text;
        begin
          if new.id is null then
            loop
              key := new_token(${length});
              execute qry || quote_literal(key) into found;
              if found is null then
                exit;
              end if;
            end loop;
            new.id = key;
          end if;
          return new;
        end;
      $$
    `);
  }

  await knex.raw(`
    create or replace function set_updated_at()
    returns trigger
    language plpgsql
    as $$ begin
      new.updated_at := now();
      return new;
    end $$
  `);

  /* `
    select titleize('abc_def')
  ` */
  await knex.raw(`
    create or replace function titleize(x text)
    returns text
    language sql immutable
    as $$
      select
        initcap(regexp_replace(x, '[-_]', ' ', 'g'))
    $$
  `);

  /* `
    select ftime(now())
  ` */
  await knex.raw(`
    create or replace function ftime(x timestamp with time zone)
    returns text
    language sql immutable
    as $$
      select
        to_char(x at time zone 'hkt', 'YYYY.MM.DD HH24:MI HKT')
    $$
  `);

  /* `
    select ftime_hhmm(now())
  ` */
  await knex.raw(`
    create or replace function ftime_hhmm(x timestamp with time zone)
    returns text
    language sql immutable
    as $$
      select
        to_char(x at time zone 'hkt', 'HH24:MI')
    $$
  `);

  /* `
    select ftime_ddmmyyyy(now())
  ` */
  await knex.raw(`
    create or replace function ftime_ddmmyyyy(x timestamp with time zone)
    returns text
    language sql immutable
    as $$
      select
        to_char(x at time zone 'hkt', 'DD/MM/YYYY')
    $$
  `);

  /* `
    select ellipses(3, '12345')
  ` */
  await knex.raw(`
    create or replace function ellipses(len int, text text)
    returns text
    language sql immutable
    as $$
      select
        case
          when length(text) > len then concat(substring(text, 1, len), '...')
          else text
        end
    $$
  `);

  /* `
    select
      index_by_prop_eq(questions, 'code', 'gender'),
      jsonb_array_length(questions) as before_count,
      jsonb_array_length(
        questions - index_by_prop_eq(questions, 'code', 'gender')
      ) as after_count
    from survey
    where id = 'prxbiflgls2m'
  ` */
  await knex.raw(`
    create or replace function index_by_prop_eq(items jsonb, prop_code text, prop_value text)
    returns int
    language sql
    as $$
      with _with_ind as (
        select
          row_number() over() - 1 as ind,
          q
        from
          jsonb_array_elements(items) as q
      )
      select
        ind :: int
      from
        _with_ind
      where
        q ->> prop_code = prop_value
    $$
  `);

  /* `
    -- expand
    select
      jsonb_uniq('[1, 1, 2, 3, 3]'::jsonb),
      jsonb_array_length(jsonb_uniq('[1, 1, 2, 3, 3]'::jsonb))
    from survey
    where id = 'prxbiflgls2m'
  ` */
  await knex.raw(`
    create or replace function jsonb_uniq(items jsonb)
    returns jsonb
    language sql
    as $$
      select
        jsonb_agg(distinct q)
      from
        jsonb_array_elements(items) as q
    $$
  `);

  /* `
    select jsonb_slice('[0, 1, 2, 3, 4, 5, 6]'::jsonb, 3, 2)
  ` */
  await knex.raw(`
    create or replace function jsonb_slice(items jsonb, pos int, count int)
    returns jsonb
    language sql
    as $$
      with t as (
        select jsonb_array_elements(items) as ae
      ),
      q as (
        select * from t limit count offset pos
      )
      select
        coalesce(jsonb_agg(q.ae), '[]')
      from q
    $$
  `);

  /* `
    select jsonb_insert('[0, 1, 2, 3, 4, 5, 6]'::jsonb, 2, '9'::jsonb)
  ` */
  /* `
    select jsonb_insert('[0, 1, 2, 3, 4, 5, 6]'::jsonb, 0, '9'::jsonb)
  ` */
  /* `
    select jsonb_insert('[0, 1, 2, 3, 4, 5, 6]'::jsonb, 7, '9'::jsonb)
  ` */
  await knex.raw(`
    create or replace function jsonb_insert(items jsonb, pos int, item jsonb)
    returns jsonb
    language sql
    as $$
      select
        jsonb_slice(items, 0, pos) || item || jsonb_slice(items, pos, jsonb_array_length(items) - pos)
    $$
  `);

  /* `
    -- move [2] to before [4]
    select jsonb_move('[0, 1, 2, 3, 4, 5, 6]'::jsonb, 2, 4)
  ` */
  /* `
    -- move [4] to before [2]
    select jsonb_move('[0, 1, 2, 3, 4, 5, 6]'::jsonb, 4, 2)
  ` */
  await knex.raw(`
    create or replace function jsonb_move(items jsonb, src_pos int, dst_pos int)
    returns jsonb
    language sql
    as $$
      select
        jsonb_insert(
          items,
          dst_pos,
          items -> src_pos
        ) - case
          when src_pos > dst_pos then src_pos + 1
          else src_pos
        end
    $$
  `);

  // trigger inserting new row (lazy ranking)
  await knex.raw(`
    create or replace function auto_insert_rank()
    returns trigger
    language plpgsql
    as $$
      declare next_rank int;
      begin
        execute
          'select coalesce(max(rank), 10000) + 100 from ' || TG_TABLE_NAME
          into next_rank;
        new.rank = next_rank;
        return new;
      end
    $$
  `);

  /* `
    select '{1,2,3}'::int[] - '{3,4}'::int[]
  ` */
  await knex.raw(`
    create or replace function diff_elements(arr1 anyarray, arr2 anyarray)
    returns anyarray
    language sql immutable
    as $$
      select array(
        select unnest(arr1) except select unnest(arr2)
      )
    $$;

    drop operator if exists - (anyarray, anyarray);
    create operator - (
      procedure = diff_elements,
      rightarg = anyarray,
      leftarg = anyarray
    );
  `);

  /* `
    select jsonb_to_text_array('["321", "123"]')
  ` */
  await knex.raw(`
    create or replace function jsonb_to_text_array(jarr jsonb)
    returns text[]
    language sql immutable
    as $$
      select array_agg(a)
      from jsonb_array_elements_text(jarr) a
    $$
  `);

  /* `
    select int2hm('120 hours')
  ` */
  await knex.raw(`
    create or replace function int2hm(i interval) returns text as $$
      select to_char(i, 'hh24:mi')
    $$ language sql
  `);

  /* `
    select secs2hm(1000000)
  ` */
  await knex.raw(`
    create or replace function secs2hm(secs int) returns text as $$
      select int2hm((secs || ' second')::interval)
    $$ language sql
  `);

  await knex.raw(`
    create or replace function dependent_serial_id()
    returns trigger as $$
      declare
        prefix text;
        key text;
        temp integer;
      begin
        if new.id is null then
          prefix := upper(left(new.type, 2)) || to_char(now(), 'yyyymmdd');
          execute '
            select max(id) from ' || TG_TABLE_NAME::regclass::text || '
            where id like ''' || prefix || '%''
          ' into key;
          if key is null then
            new.id := prefix || '001';
          else
            temp := substr(key, 11, 100) ::int + 1;
            new.id := prefix || lpad(temp::text, 3, '0');
          end if;
        end if;
        return new;
      end;
    $$ language 'plpgsql'
  `);

  return true;
};

exports.down = async (knex) => {
  return true;
};
