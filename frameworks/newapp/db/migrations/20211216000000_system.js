const {knexHelpers} = require("../helpers");

exports.up = async (knex) => {
  const {restrictOptions, autoId, autoDependentId, autoUpdatedAt} = knexHelpers(knex);

  // system config table
  await knex.schema.createTable("sys_config", (table) => {
    table.string("id").primary();
    table.string("type").notNull();
    table.string("by").notNull();
    table.timestamp("at").defaultTo(knex.fn.now());
    table.text("content");
  });
  await autoDependentId("sys_config");

  // job_execution table
  await knex.schema.createTable("job_execution", (table) => {
    table.increments("id");
    table.string("job_name").notNull();
    table.string("by").notNull();
    table.timestamp("started_at").defaultTo(knex.fn.now());
    table.integer("duration_ms");
    table.bool("throttled").defaultTo(false);
    table.bool("failed").defaultTo(false);
    table.text("errmsg");
  });

  /* `
    call new_job_execution('dummyTestCase', 'loki', 3000, null, null)
  ` */
  // 3 cases
  // last_ran + threshold < now()            -> run now, return (id, 0)
  // last_ran < now() < last_ran + threshold -> run later, return (id, wait in ceil(secs))
  // now() < last_ran                        -> reject, return (null, 0)
  await knex.raw(`
    create or replace procedure new_job_execution(
      _job_name text, _by text, _threshold int, out job_id int, out delay_ms int
    ) language plpgsql as $$
      declare
        new_job_id int := null;
      begin
        lock table job_execution in access exclusive mode;
        with t as (
          select
            floor(extract(epoch from now()) * 1000) as now,
            coalesce(
              floor(extract(epoch from max(started_at)) * 1000),
              0
            ) as last_ran
          from job_execution
          where job_name = _job_name
        ),
        t2 as (
          select *, greatest(last_ran + _threshold - now, 0) as wait from t
        ),
        t3 as (
          insert into job_execution (job_name, by, started_at, throttled)
          select
            _job_name, _by, to_timestamp((now + wait) / 1000),
            wait > 0
          from t2 where wait <= _threshold
          returning id
        )
        select id, wait
        from t2
        left join t3 on true
        into job_id, delay_ms;
      end
    $$
  `);

  await knex.schema.createTable("error_log", (table) => {
    table.string("id").primary();
    table.text("referer");
    table.text("user_agent");
    table.text("ip_addr");
    table.timestamp("at");
    table.text("method");
    table.text("url");
    table.text("body");
    table.text("summary");
    table.text("details");
  });
  await autoId("error_log")

  return true;
};

exports.down = async (knex) => {
  await knex.schema.dropTableIfExists("error_log");
  await knex.schema.dropTableIfExists("job_execution");
  await knex.schema.dropTableIfExists("sys_config");
  return true;
};
