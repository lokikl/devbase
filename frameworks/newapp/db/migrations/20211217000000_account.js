const {knexHelpers} = require("../helpers");

exports.up = async (knex) => {
  const {restrictOptions, autoId, autoUpdatedAt} = knexHelpers(knex);

  // account table
  await knex.schema.createTable("account", (table) => {
    table.string("id").primary();

    table.string("parent").references("account.id");
    table.string("owner_email");
    table.enum("type", ["admin", "tenant"]).notNull();
    table.enum("status", ["active", "inactive"]).notNull().defaultTo("active");
    table.jsonb("config").defaultTo({});
    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  });

  await autoUpdatedAt("account");

  // user_profile table
  await knex.schema.createTable("user_profile", (table) => {
    table.string("id").primary();
    table.string("email").unique().notNull();
    table.enum("status", ["active", "inactive"]).notNull().defaultTo("active");

    table.string("default_account").references("account.id");
    table.string("password_hash").notNull();
    table.string("api_key");
    table.string("api_key_secret");
    table.jsonb("config").defaultTo({}); // lastLoginAt, privateKey, publicKey, uid, home

    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  });
  await autoUpdatedAt("user_profile");

  // membership table
  await knex.schema.createTable("membership", (table) => {
    table.string("user_profile_id").references("user_profile.id").notNull();
    table.string("account_id").references("account.id").notNull();
    table.primary(["user_profile_id", "account_id"]);

    table.jsonb("config").defaultTo({});
    table.timestamp("created_at").defaultTo(knex.fn.now());
  });

  await knex.schema.createTable("access_log", (table) => {
    table.string("id").primary();
    table.string("user_login");
    table.string("user_agent");
    table.string("client_ip");
    table.enum("status", ["success", "failed"]).notNull().defaultTo("success");
    table.text("message");
    table.timestamp("created_at").defaultTo(knex.fn.now());
  });
  await autoId("access_log");

  return true;
};

exports.down = async (knex) => {
  await knex.schema.dropTableIfExists("access_log");
  await knex.schema.dropTableIfExists("membership");
  await knex.schema.dropTableIfExists("user_profile");
  await knex.schema.dropTableIfExists("account");
  return true;
};
