#!/usr/bin/env bash
# devbase: ensure=always
# devbase: executable=yes

# SIGTERM-handler
term_handler() {
  if [ $pid -ne 0 ]; then
    kill -SIGTERM "$pid"
    wait "$pid"
  fi
  exit 143; # 128 + 15 -- SIGTERM
}
# setup handlers
trap 'kill ${!}; term_handler' SIGTERM

if [[ "$APP_VERSION" == "dev" ]]; then
  gosu node /app/.devbase/boot-dev.sh

else
  ./<%= project_name %>-bin $1 &
fi

pid="$!"

# wait forever
while true; do
  tail -f /dev/null & wait ${!}
done

