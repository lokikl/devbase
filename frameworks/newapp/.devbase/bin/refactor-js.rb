#!/usr/bin/env ruby
# devbase: ensure=always
# devbase: executable=yes

cmd = ARGV[0]

def remove_unused_imports(path)
  import_lines = %x[sed "#{path}" -e '/^import/!d'].lines.map(&:strip)
  import_lines.each { |line|
    clean = line.split(' from')[0].split('import ')[1].sub('{', '').sub('}', '')
    parts = clean.split(',').map(&:strip).map { |part|
      part[/as/] ? part.split('as ')[1] : part
    }
    used = parts.any? { |part|
      system(%Q[grep '#{part}' "#{path}" | grep -qv 'import.*from'])
    }
    unless used
      %x[sed -i "/#{clean}/d" "#{path}"]
    end
  }
end

if cmd == "extract-module"
  oldpath = ARGV[1]
  basepath = oldpath.split('/')[0..-2].join('/') + '/'
  newpath = ARGV[2]

  %x[
    sed "#{oldpath}" -e '/^import/!d' | sed '$G' | cat - "#{newpath}" > "#{newpath}.new";
    mv "#{newpath}.new" "#{newpath}";
    sed -i "#{newpath}" -e 's/^const/export const/g'
  ]

  exports = %x[
    sed "#{newpath}" -e '/^export/!d' | awk '{print $3}'
  ].lines.map(&:strip).join(', ')

  remove_unused_imports(newpath)

  relpath = newpath.split(basepath)[1].split('.js')[0]
  puts %Q[import {#{exports}} from "./#{relpath}";]
end

if cmd == "remove-unused-imports"
  remove_unused_imports(ARGV[1])
end
