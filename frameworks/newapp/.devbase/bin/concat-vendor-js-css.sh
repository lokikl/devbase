#!/usr/bin/env bash
# devbase: ensure=on-bootstrap
# devbase: executable=yes

# webpack-merge-and-include-globally is not working with webpack 5
# so this script is to manually prepare the vendor js and css

echo "" > src/assets/static/vendor.js

echo concat vendor.js
for f in \
  node_modules/jquery/dist/jquery.js \
  vendor/hide-show.js \
  node_modules/axios/dist/axios.js \
  node_modules/imagesloaded/imagesloaded.pkgd.js \
  node_modules/litepicker/dist/litepicker.js \
  node_modules/socket.io-client/dist/socket.io.js \
  node_modules/notification-polyfill/notification.js \
  node_modules/d3/dist/d3.js \
  node_modules/qrcode/build/qrcode.js \
  ; do
    cat "${f}" >> src/assets/static/vendor.js
    echo >> src/assets/static/vendor.js
done
echo minify vendor.js
docker compose exec app \
  npx terser src/assets/static/vendor.js --compress --mangle -o src/assets/static/vendor.js

echo concat vendor.css
cat \
  node_modules/normalize.css/normalize.css \
  > src/assets/static/vendor.css
echo minify vendor.css
docker compose exec app \
  yarn run node-sass --output-style=compressed src/assets/static/vendor.css src/assets/static/vendor.css

echo update symbolic links
rm -f dist/public/vendor.js
rm -f dist/public/vendor.css
cp src/assets/static/vendor.js dist/public/vendor.js
cp src/assets/static/vendor.css dist/public/vendor.css
