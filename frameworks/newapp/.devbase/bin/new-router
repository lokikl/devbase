#!/usr/bin/env ruby
# devbase: ensure=always
# devbase: executable=yes

require 'fileutils'

name = ARGV[0] # e.g. backendAuth

class String
  def underscore
    self.gsub(/::/, '/').
    gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
    gsub(/([a-z\d])([A-Z])/,'\1_\2').
    tr("-", "_").
    downcase
  end
end

def add_router_file(name)
  filename = name.underscore
  filepath = "src/routes/#{filename}.js"
  puts "Creating #{filepath} ..."

  File.write(filepath, <<-CODE
  import express from "express";
  import * as R from "ramda";
  import knex from "../transport/database";
  import * as H from "./helpers";

  const router = express.Router();

  const {renderUID, asyncRoute} = H.getUtils(R.last(__filename.split("/")));

  router.get(
    "/",
    asyncRoute(async (req, res) => {
      renderUID(req, res, "Sign in", "signin", {});
    })
  );

  export default router;
  CODE
    .gsub(/^  /, '')
  )
  puts "Done"
  puts "Add lines below to install:"
  puts
  puts %Q[import #{name}Router from "./#{filename}";]
  puts %Q[router.use("/", #{name}Router);]
end

if name
  add_router_file(name)
else
  puts "Usage:"
  puts
  puts "new-router [router_name]"
  puts
  puts "e.g. new-router backendAuth"
end
