#!/usr/bin/env bash

# devbase: ensure=always
# devbase: executable=yes

echo "Mixins"
for file in `ls -1 src/mixins/*.pug`; do
  mixinName=`basename "${file}" '.pug'`
  foundCount=`grep -I -R --exclude=${mixinName}.pug --exclude=includes.pug --exclude=client-includes.pug $mixinName 'src/' | wc -l`
  if [[ $foundCount -lt 1 ]]; then
    echo "  $mixinName $foundCount"
  fi
done
