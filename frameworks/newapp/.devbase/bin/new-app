#!/usr/bin/env ruby
# devbase: ensure=always
# devbase: executable=yes

require 'active_support/inflector'

name = ARGV[0] # e.g. create_user

def update_frontend_js(name)
  file = 'src/frontend.js'
  puts "Updating #{file} ..."
  lines = File.read(file).lines

  ##devbase-anchor naf-import
  lno = lines.index { |l| l.include?('##devbase-anchor naf-import') }
  lines.insert(lno, %Q[import { init as init#{name.camelize} } from './frontend/app/#{name.underscore}';\n])

  ##devbase-anchor naf
  lno = lines.index { |l| l.include?('##devbase-anchor naf-load') } + 1
  lines.insert(lno, %Q[  #{name}: init#{name.camelize},\n])

  File.write(file, lines.join(''))
end

def add_app_js_file(name)
  controller_file_path = "src/frontend/app/#{name.underscore}.js"
  puts "Creating #{controller_file_path} ..."
  File.write(controller_file_path, <<-CODE
  /* jshint esversion: 6 */

  import {
    appReturn,
    backStageRequest,
    nafOnClick,
    nafOpen
  } from "../navy-app-framework";

  const init = async $body => {
    const openNAFApp = nafOpen($body);
    const onClick = nafOnClick($body);
  };

  export { init };
  CODE
    .gsub(/^  /, '')
  )
end

def create_view(name)
  body_view_path = "src/views/app/#{name.underscore}.pug"
  puts "Creating #{body_view_path} ..."
  File.write(body_view_path, <<-CODE
  extends ../naf_app_modal

  block content
    div.fluid-container.navy-enabled
      div(
        data-init-app=appCode
        data-naf-id=nafId
      )
      div.#{name.gsub('_', '-')}-wrapper Hello, JS will load.
  CODE
    .gsub(/^  /, '')
  )
end

def update_express_index(name)
  file = 'src/index.js'
  puts "Updating #{file} ..."
  lines = File.read(file).lines

  ##devbase-anchor express-routes-import
  lno = lines.index { |l| l.include?('##devbase-anchor express-routes-import') }
  lines.insert(lno, %Q[import #{name.camelize(:lower)}Router from "./routes/app/#{name.underscore}";\n])

  ##devbase-anchor express-routes-load
  lno = lines.index { |l| l.include?('##devbase-anchor express-routes-load') }
  lines.insert(lno, %Q[app.use("/", #{name.camelize(:lower)}Router);\n])

  File.write(file, lines.join(''))
end

def add_express_route_file(name)
  js_path = "src/routes/app/#{name.underscore}.js"
  puts "Creating #{js_path} ..."
  File.write(js_path, <<-CODE
  import express from "express";
  import * as R from "ramda";

  const router = express.Router();

  router.post("/app/#{name.underscore}", (req, res, next) => {
    res.render("app/#{name.underscore}", R.mergeRight({
      appCode: "#{name.underscore}",
      appTitle: "#{name.titleize}",
      appSize: 9,
      draggable: false
    }, req.body || {}));
  });

  export default router;
  CODE
    .gsub(/^  /, '')
  )
end

def update_scss_index(name)
  css_path = 'src/assets/scss/app.scss'
  puts "Updating #{css_path} ..."
  lines = File.read(css_path).lines

  ##devbase-anchor naf-scss-import
  lno = lines.index { |l| l.include?('##devbase-anchor naf-scss-import') }
  lines.insert(lno, %Q[@import "./app/#{name.underscore}";\n])

  File.write(css_path, lines.join(''))
end

def add_app_scss(name)
  css_path = "src/assets/scss/app/#{name.underscore}.scss"
  puts "Creating #{css_path} ..."
  File.write(css_path, <<-CODE
  .#{name.underscore.gsub('_', '-')}-wrapper {
  }
  CODE
    .gsub(/^  /, '')
  )
end

update_frontend_js(name)
add_app_js_file(name)
create_view(name)
update_express_index(name)
add_express_route_file(name)
update_scss_index(name)
add_app_scss(name)
