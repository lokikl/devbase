#!/bin/bash
# devbase: ensure=always
# devbase: executable=yes

echo "=== yarn install ============================================================================================="
yarn install

echo "=== yarn run nodemon (dev-server) ============================================================================"
yarn run babel src -d dist
LOG="dev-server" yarn run nodemon \
  -w .prettierrc.yaml \
  -w dist/index.dev-server.js \
  --exec 'yarn node dist/index.dev-server.js' &

echo "=== wait until dev-server is up =============================================================================="
until $(curl --output /dev/null --silent --head --fail http://127.0.0.1:3001); do
  printf '.'
  sleep 1
done

echo "=== generate sprite.min.svg on mixins changed ================================================================"
yarn run nodemon \
  -q \
  -e pug \
  -w src/mixins \
  --exec "./build/generate-sprite-svg.sh src/assets/static/sprite.min.svg" &

echo "=== yarn run webpack ========================================================================================="
yarn run nodemon \
  -w webpack.config.js \
  --exec 'yarn run webpack -w --mode=development --devtool=inline-source-map' &

echo "=== yarn run nodemon (express) ================================================================================"
# re-caching pug is slow, so I splitted auto-reload into two flows
#  1. JS changes: supported in src/index.js with chokidar
#  2. mixins changes: supported by an API in src/routes/devbase.js
LOG="express" yarn run nodemon \
  -d 0.1 \
  -e js,pug,json \
  -w dist/index.js \
  -w src/views \
  --exec 'printf "\n\n\n"; node dist/index.js || touch dist/index.js' &

# echo "=== ws server =================================================================================="
# LOG="ws" yarn run nodemon \
#   -w dist/dev-boot.js \
#   -w dist/index.ws.js \
#   dist/dev-boot.js index.ws.js &

echo "=== yarn run nodemon (worker1) ================================================================================"
LOG="w1" TOUCH_FILES="dist/index.workers.js,.skip-starting-bg-jobs" yarn run nodemon \
  -w dist/dev-boot.js \
  -w dist/index.workers.js \
  dist/dev-boot.js index.workers.js &

echo "=== yarn run nodemon (worker2) ================================================================================"
LOG="w2" yarn run nodemon \
  -w dist/dev-boot.js \
  -w dist/index.workers.js \
  dist/dev-boot.js index.workers.js &
