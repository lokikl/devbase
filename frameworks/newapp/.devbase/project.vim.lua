-- devbase: ensure=always

vim.cmd([[
  set backupcopy=yes
  " consumed by dev-server, with the nvim remote server protocol
  function! GracefulReload()
    let l:view = winsaveview()
    silent execute 'e'
    call winrestview(l:view)
  endfunction

  function! InspectSQLTable()
    let word = expand("<cword>")
    call system("paste-sql-to-next-tmux-pane inspect-table " . word)
  endfunction

  function! FormatKnexSQL()
    let l:view = winsaveview()
    /`
    let lno2 = line('.')
    ?`
    let lno1 = line('.')
    if lno2 == 0
      let lno2 = line('$')
    endif
    let ind=indent(lno1 + 1)
    execute ":" . lno1 . "," . lno2 . "!xclip -sel clip"
    let formated=system("format-copied-sql " . ind)
    execute "normal uj" . (lno2 - lno1 - 1) . "dd"
    call append(lno1, split(formated, "\n"))
    call winrestview(l:view)
  endfunction

  function! ExtractJSModule()
    let fullpath = expand('%:h') . '/'
    let newpath = input('Extract selected js functions to a new module: ', fullpath)
    if newpath != fullpath
      if newpath !~ '\.js$'
        let newpath = newpath . ".js"
      endif
      let i = indent(line("'<"))
      execute "'<,'>s/^ \\{" . i . "\\}//"
      execute "'<,'>w " . newpath
      execute "'<,'>d"
      let formated = system("refactor-js.rb extract-module " . expand('%') . " " . newpath)
      call append(line('.') - 1, split(formated, "\n"))
      execute "normal kV="
      execute 'w'
      execute 'tabedit ' . newpath
    endif
    redraw!
  endfunction

  augroup lang_utilities
    autocmd!
    au FileType javascript nnoremap <silent> ss :lua send_sql_under_cursor("execute")<cr>
    au FileType javascript nnoremap <silent> SS :lua send_sql_under_cursor("benchmark")<cr>
    au FileType javascript nnoremap <silent> SW :call InspectSQLTable()<cr>
    au FileType javascript nnoremap <silent> sq :call FormatKnexSQL()<cr>
    " <leader>em to extract selected lines (js functions) to js module
    au FileType javascript vnoremap <leader>em <esc>:call ExtractJSModule()<cr>
    au BufWritePre *.rs lua vim.lsp.buf.formatting_sync(nil, 200)
  augroup end
]])

if console_ctl == nil then
  print("console_ctl is undefined, please checkout nvim-config/nvim.d/my/utilities.lua")
  return
end

local send = require("my.send")

local proj = os.getenv("COMPOSE_PROJECT_NAME")

local view_log = "source .envrc; docker compose logs -f --no-log-prefix --tail=200 app"
local app_db = "source .envrc; docker compose exec db psql -U " .. proj .. " -P expanded=auto -P pager=off"
local test_view = "source .envrc; docker compose exec app yarn test --watch --silent=false"

_G.send_sql_under_cursor = function(flag)
  local ts = require("nvim-treesitter.ts_utils")
  local node = ts.get_node_at_cursor()
  if node ~= nil and (node:type() == "template_string" or node:type() == "source") then
    local lines = ts.get_node_text(node)
    local text = table.concat(lines, "\n")
    if text:find("/%* cli") then
      send.send_current_line("next")
    else
      local fp = io.popen("paste-sql-to-next-tmux-pane " .. flag, "w")
      fp:write(text) ---@diagnostic disable-line
      fp:close()     ---@diagnostic disable-line
    end
  else
    send.send_current_line("next")
  end
end

-- send commented command
vim.keymap.set("n", "sp", function()
  vim.cmd("normal sfssu")
end)

local replay = function(cmd, clear)
  return function()
    if clear then
      run_cmd("tmux send-keys -t0 -R")
    end
    run_cmd("curl -XPOST -sk https://" .. proj .. "-dev.com/_replay/replay-last/" .. cmd)
  end
end

local replay_current = function()
  return function()
    local fn = vim.fn.expand("%"):gsub(".*src", "src")
    local ln = vim.fn.line(".")
    -- inspect-routes src/routes/dashboard.js 56
    local cmd = "inspect-routes " .. fn .. " " .. ln
    local id = run_cmd(cmd)
    run_cmd("tmux send-keys -t0 -R")
    run_cmd("curl -XPOST -sk 'https://" .. proj .. "-dev.com/_replay/replay-last/" .. id .. "'")
  end
end

which_key_map("r", {
  w = { replay_current(), "Replay current file + line" },
  r = { replay("replay", true), "Replay last replayed request" },
  f = { replay("forget", false), "Forget the last replay" },
  e = { replay("refresh", false), "Refresh in browser" },
  z = { console_pane_flag_toggle, "Toggle console position (left/right)" },
  c = { "<cmd>call system('tmux send-keys -t0 -R')<cr>", "Clear the left pane" },
  l = { console_ctl(view_log), "View app log" },
  d = { console_ctl(app_db), "Connect to App DB" },
  t = { console_ctl(test_view), "Open test view" },
})

local tpopup = function(cmd)
  return function()
    send.run_popup(cmd .. "; any-key")
  end
end

local tsplit = function(cmd, arg)
  return function()
    exec_in_split(cmd, arg)
  end
end

local system = function(cmd)
  return function()
    run_cmd(cmd)
  end
end

which_key_map("c", {
  o = { tsplit("console app"), "console for app" },
})

which_key_map("d", {
  r = { tpopup("resetdb"), "reset db" },
})

which_key_map(" ", {
  r = { tpopup("reboot-dev"), "reboot-dev" },
  o = { system("xdg-open https://" .. proj .. "-dev.com"), "open app in browser" },
})

which_key_map(" o", {
  o = { system("curl -k https://" .. proj .. "-dev.com/_/refresh-browser"), "refresh browser" },
  u = { system("xdg-open https://" .. proj .. "-dev.com/_/pick-ssicon"), "pick ssicon" },
  r = { system("touch dist/index.js"), "restart express" },
  s = { system("touch dist/index.ws.js"), "restart ws" },
  w = { system("touch dist/index.workers.js"), "restart workers" },
  l = { system("touch dist/index.ldap.js"), "restart ldap" },
  d = { system("touch .prettierrc.yaml"), "restart dev-server" },
})

deprecate_keymap("n", "<leader>l", "rl")
deprecate_keymap("n", "<leader>d", "rd")
deprecate_keymap("n", "<leader>s", "rs")

local TE = require("telescope-external")

local inspect_files = function(pattern, depth)
  return TE.open("inspect-files './" .. pattern .. "' " .. depth)
end

which_key_map("s", {
  b = { inspect_files(".devbase/*", 2), "devbase files" },
  h = { TE.open("inspect-mixins pug"), "Search mixin views" },
  c = { TE.open("inspect-mixins scss"), "Search mixin styles" },
  a = { inspect_files("src/routes/*", 3), "Search routes" },
  l = { inspect_files("src/lib/*", 3), "Search libs" },
  w = { inspect_files("src/workers/*", 3), "Search background jobs" },
  d = { TE.open("inspect-dbfiles"), "Search db files" },
  t = { TE.open("inspect-dbtables"), "Search db tables" },
  r = { TE.open("inspect-routes all"), "Search routes/jobs" },
  e = { TE.open("inspect-routes recent"), "Search recent routes/jobs" },
  v = { TE.open("inspect-frontend-events"), "Search frontend events" },
  j = { TE.open("inspect-frontend-files"), "Search frontend js" },
  o = { TE.open("inspect-todos"), "TODOs" },
})

-- show an extmark if scss of that mixin exists
local mixinToggleGrp = vim.api.nvim_create_augroup("mixinToggling", { clear = true })
local setupToggle = function(pattern, folder, type, row)
  vim.api.nvim_create_autocmd("BufRead", {
    group = mixinToggleGrp,
    pattern = pattern,
    callback = function()
      -- ref is to reference it once to allow f-strings to use outer variable
      local dir = vim.fn.expand("%:h")
      local basename = vim.fn.expand("%:t:r")
      local path = table.concat({
        dir,
        "..",
        folder,
        basename .. "." .. type
      }, "/")
      if vim.fn.filereadable(path) > 0 then
        local ns_id = vim.api.nvim_create_namespace("devbase")
        vim.api.nvim_buf_set_extmark(0, ns_id, row, 0, {
          id = 1,
          virt_text = {{" " .. type .. " (alt+i) ", "IncSearch"}},
          virt_text_pos = "eol",
        })
      end
      vim.keymap.set("n", "<m-i>", function()
        vim.cmd("edit " .. path)
      end, { buffer = true, noremap = true, silent = true })
    end
  })
end
setupToggle("*.pug", "style", "scss", 1)
setupToggle("*.scss", "mixins", "pug", 0)
