import knex from "./database";

test("has 4 commands", async () => {
  const {
    rows: [{ans}],
  } = await knex.raw(`select 1 + 2 as ans`);
  expect(ans).toBe(3);
});
