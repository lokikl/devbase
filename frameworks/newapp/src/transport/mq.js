// devbase: ensure=always
import {connect, StringCodec} from "nats";
import {logInfo} from "../devbase_helpers";

/* eslint-disable fp/no-let */
let nc = null;

const sc = StringCodec();

export const connectMQ = async (natsEndpoint, natsPort) => {
  if (nc) return nc;
  const endpoint = natsEndpoint || process.env.NATS_ENDPOINT;
  const port = natsPort || process.env.NATS_PORT;

  nc = await connect({
    servers: `${endpoint}:${port}`,
    pingInterval: 60000, // ping every 60s
    reconnect: true,
    maxReconnectAttempts: -1,
  });

  console.info(`connected ${nc.getServer()}`);

  logInfo(`NATS: connected ${nc.info.server_id}`);
  return nc;
};

export const subscribeMQ = async (topic, params) => {
  if (nc === null) {
    await connectMQ();
  }
  const sub = nc.subscribe(topic, params);
  logInfo(`NATS: subscribed ${topic}`);
  return sub;
};

export const publishMQ = async (topic, data = {}) => {
  if (nc === null) {
    await connectMQ();
  }
  nc.publish(topic, sc.encode(JSON.stringify(data)));
  logInfo(`NATS: published ${topic}`);
};

export const askMQ = async (topic, data = {}, timeout = 10000) => {
  if (nc === null) {
    await connectMQ();
  }
  const res = await nc.request(topic, sc.encode(JSON.stringify(data)), {
    timeout,
  });
  return JSON.parse(sc.decode(res.data));
};
