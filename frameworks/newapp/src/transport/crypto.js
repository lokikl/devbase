import crypto from "crypto";

const algorithm = "aes-256-ctr";

const encrypt = (secretKey, text) => {
  const iv = crypto.randomBytes(16);
  const cipher = crypto.createCipheriv(algorithm, secretKey, iv);
  const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
  return [iv.toString("hex"), encrypted.toString("hex")].join(".");
};

const decrypt = (secretKey, hash) => {
  const [iv, content] = hash.split(".");
  const decipher = crypto.createDecipheriv(
    algorithm,
    secretKey,
    Buffer.from(iv, "hex")
  );
  const decrpyted = Buffer.concat([
    decipher.update(Buffer.from(content, "hex")),
    decipher.final(),
  ]);
  return decrpyted.toString();
};

export {encrypt, decrypt};
