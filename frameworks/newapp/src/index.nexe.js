const Knex = require("../node_modules/knex/knex");
const KnexConfig = require("../knexfile");
/* eslint-disable global-require */
const knex = Knex(KnexConfig.production);

const cmd = process.argv[2] || "app";

console.log(`<%= project_name %> version: ${process.env.APP_VERSION}`);
console.log(`cmd: ${cmd}, time: ${new Date()}`);

if (cmd === "resetdb") {
  (async () => {
    console.log("Rolling back all migrations ...");
    await knex.migrate.rollback("all");

    console.log("Migrating to latest ...");
    await knex.migrate.latest();

    console.log("Seeding data ...");
    await knex.seed.run();

    console.log("Done");
    process.exit(1);
  })();
} else if (cmd === "migrate") {
  (async () => {
    console.log("Migrating to latest ...");
    await knex.migrate.latest();

    console.log("Done");
    process.exit(1);
  })();
} else if (cmd === "workers") {
  console.log("Starting worker ...");
  require("./index.workers");
} else if (cmd === "ws") {
  console.log("Starting ws ...");
  require("./index.ws");
} else if (cmd === "app") {
  console.log("Starting app server ...");
  require("./index");
} else {
  console.log(`Unknown cmd ${cmd}`);
}
