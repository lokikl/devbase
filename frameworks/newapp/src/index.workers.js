import "source-map-support/register";
import fs from "fs-extra";
import * as R from "ramda";
import cron from "node-cron";
import axios from "axios";
import {StringCodec} from "nats";
import chalk from "chalk";
import {getEnv} from "./devbase_helpers";
import {formatTimeHMS} from "./frontend-helpers";
import {subscribeMQ, publishMQ} from "./transport/mq";
import knex from "./transport/database";
import dummyTestCase from "./workers/dummyTestCase";

const sc = StringCodec();

if (!getEnv("NATS_ENDPOINT")) {
  throw new Error("NATS_ENDPOINT is undefined.");
}

const cases = {
  dummyTestCase,
};

const colorLevel = (level) =>
  level === "info"
    ? chalk.dim(level)
    : level === "error"
    ? chalk.red(level)
    : level;

const workerId = process.env.LOG;

const workerLabel = {
  w1: chalk.cyan,
  w2: chalk.magenta,
  w3: chalk.yellow,
  w4: chalk.blue,
  w5: chalk.green,
  w6: chalk.red,
}[workerId](workerId);

const logger = (key, by) => {
  const label = `${by}@${key}`;
  return process.env.APP_VERSION === "dev"
    ? (msg, level = "info") => {
        console.log(workerLabel, colorLevel(level), chalk.dim(label), msg);
      }
    : (msg) => {
        console.log(label, msg);
      };
};

const getNewJobId = async (options) => {
  const {
    rows: [{job_id: jobId, delay_ms: delayMS}],
  } = await knex.raw(
    `call new_job_execution(:key, :by, :throttle, null, null)`,
    options
  );
  return [jobId, delayMS];
};

const runCase = async (key, by, data, isBlocking, pendingJobId = null) => {
  const log = logger(key, by);
  const startAt = new Date();
  const {throttle: throttle_ = 0, run} = cases[key];
  const throttle = isBlocking ? 0 : throttle_;

  /* eslint-disable fp/no-let */
  let jobId = null;

  if (R.isNil(pendingJobId)) {
    log(`got msg at ${formatTimeHMS(startAt)}`);
    const [nextJobId, delayMS] = await getNewJobId({key, throttle, by});
    if (R.isNil(nextJobId)) {
      log(`de-duplicated due to throttling`);
      return false;
    }
    if (delayMS > 0) {
      log(`debounce job ${nextJobId}, execute in ${delayMS}ms`);
      setTimeout(() => runCase(key, by, data, isBlocking, nextJobId), delayMS);
      return false;
    }
    jobId = nextJobId;
  } else {
    log(`resumed debounced msg, ${pendingJobId}`);
    jobId = pendingJobId;
  }

  const updateStatus = (updates = {}) =>
    knex("job_execution")
      .where("id", jobId)
      .update({
        duration_ms: new Date() - startAt,
        ...updates,
      });

  let returns = null;
  const replayRec = {
    method: "JOB",
    label: key,
    headers: {
      by,
    },
    body: data,
  };
  try {
    log(`permitted to run, jobId = ${jobId}`);
    returns = await run(data, log);
    log(`finished, took ${new Date() - startAt}ms`);
    if (process.env.APP_VERSION === "dev") {
      axios.post("http://localhost:3001/report", {
        ...replayRec,
        status: "ok",
      });
    }
  } catch (err) {
    log(`error!, ${err}`, "error");

    await updateStatus({
      failed: true,
      errmsg: err.message,
    });
    if (process.env.APP_VERSION === "dev") {
      axios.post("http://localhost:3001/report", {
        ...replayRec,
        status: "failed",
      });
    }
    return false;
  }
  await updateStatus();
  return returns;
};

(async () => {
  /* eslint-disable fp/no-loops */
  /* eslint-disable no-restricted-syntax */
  /* eslint-disable no-underscore-dangle */
  const sub = await subscribeMQ("workers", {queue: "chatweb"});
  for await (const m of sub) {
    const data = JSON.parse(sc.decode(m.data));
    const isBlocking = !R.isNil(m._msg.reply);
    const result = await runCase(data.case, data.by, data.data, isBlocking);
    const res = {ok: 1, ...(result || {})};
    if (isBlocking) {
      m.respond(sc.encode(JSON.stringify(res)));
    }
  }
})();

if (workerId === "w1") {
  const skippingBgJobs = fs.existsSync(".skip-starting-bg-jobs");
  if (skippingBgJobs) {
    fs.removeSync(".skip-starting-bg-jobs");
  }
  /* eslint-disable guard-for-in */
  for (const key in cases) {
    if (cases[key].schedule) {
      cron.schedule(cases[key].schedule, () => {
        publishMQ("workers", {
          case: key,
          by: "cron",
          data: {},
        });
      });
    }
    if (!skippingBgJobs && cases[key].startOnBoot) {
      publishMQ("workers", {
        case: key,
        by: "system",
        data: {},
      });
    }
  }
  if (process.env.APP_VERSION === "dev") {
    axios.post("http://127.0.0.1:3001/_replay/replay-last/replay").catch(() => {
      console.log("failed to auto replay, maybe dev-server is not started");
    });
  }
}
