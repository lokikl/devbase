// devbase: ensure=always
import * as R from "ramda";
import chalk from "chalk";
import axios from "axios";
import querystring from "querystring";

export const getEnv = (key, defaultValue = "") => {
  const value = process.env[key];
  return R.isNil(value) ? defaultValue : value;
};

export const go = (promise) =>
  promise.then((data) => [null, data]).catch((err) => [err]);

export const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const isDev = process.env.APP_VERSION === "dev";

const logInfoDev = (msg) =>
  console.log(`INFO  ${new Date().toLocaleString()} ${chalk.dim(msg)}`);

const logErrorDev = (msg) =>
  console.error(`ERROR ${new Date().toLocaleString()} ${chalk.red(msg)}`);

const logInfoProd = (msg) =>
  console.log(`INFO  ${new Date().toLocaleString()} ${msg}`);

const logErrorProd = (msg) =>
  console.error(`ERROR ${new Date().toLocaleString()} ${msg}`);

export const logInfo = isDev ? logInfoDev : logInfoProd;
export const logError = isDev ? logErrorDev : logErrorProd;

export const devWS = async (query) => {
  const qs = querystring.stringify(query);
  return axios.get(`http://localhost:3001/dev-server?${qs}`);
};
