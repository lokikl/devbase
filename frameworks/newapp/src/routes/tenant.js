import express from "express";
import * as R from "ramda";
import * as H from "./helpers";
/* import knex from "../transport/database"; */

/* import * as SC from "../lib/sys-config"; */

const router = express.Router();

const {renderUID, asyncRoute} = H.getUtils(R.last(__filename.split("/")));

router.get(
  "/",
  asyncRoute(async (req, res) => {
    res.redirect("/dashboard");
  })
);

router.get(
  "/dashboard",
  asyncRoute(async (req, res) => {
    renderUID(req, res, "Home", "tenant-dashboard", {});
  })
);

export default router;
