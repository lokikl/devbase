import express from "express";
import * as R from "ramda";
import * as H from "./helpers";
import knex from "../transport/database";

import * as SC from "../lib/sys-config";

const router = express.Router();

const {renderUID, asyncRoute} = H.getUtils(R.last(__filename.split("/")));

// edit sys config
router.get("/", H.redirectMiddleware("/general-settings"));

router.get(
  "/:type",
  asyncRoute(async (req, res) => {
    const {type} = req.params;
    const content = await SC.get(type, true);

    renderUID(req, res, "Config", "admin-sys-config", {
      items: SC.items,
      type,
      mode: SC.getMode(type),
      content,
    });
  })
);

router.get(
  "/:type/versions",
  asyncRoute(async (req, res) => {
    const {type} = req.params;
    const table = await H.tableExt({
      req,
      options: {
        id: "t",
        sortable: ["version", "by", "at"],
        sort: ["version", "desc"],
      },
      sql: `
        -- expand
        -- type: 'user-profile'
        select
          id as version,
          by,
          at,
          1 as actions
        from sys_config
        where type = :type
      `,
      sqlParams: {type},
    });
    res.nav("sysconf-versions", "p-sysconf-versions", {table, type});
  })
);

router.get(
  "/:type/view/:version",
  asyncRoute(async (req, res) => {
    const {type} = req.params;
    const {
      rows: [data],
    } = await knex.raw(
      `
        -- expand
        -- type: 'general-settings'
        -- id: 'GE20230207003'
        with t as (
          select id, content from sys_config
          where type = :type and id < :id
          order by id desc
          limit 1
        ),
        tn as (
          (
            select id as next_id from sys_config
            where type = :type and id > :id
            order by id asc
            limit 1
          )
          union all
          select null
          limit 1
        )
        select
          t.content as left,
          sc.content as right,
          t.id as left_label,
          sc.id || ' ' || '*' as right_label,
          tn.next_id
        from sys_config sc, t, tn
        where sc.type = :type and sc.id = :id
      `,
      {type, id: req.params.version}
    );

    res.nav("sysconf-versions", "p-sysconf-compare", {
      type,
      data: {
        left: data.left,
        right: data.right,
        leftLabel: data.left_label,
        rightLabel: data.right_label,
        nextId: data.next_id,
      },
    });
  })
);

router.get(
  "/:type/compare/:version",
  asyncRoute(async (req, res) => {
    const {type} = req.params;
    const {
      rows: [{old, current}],
    } = await knex.raw(
      `
        -- expand
        -- type: 'user-profile'
        -- id: 'US20220816002'
        select
          content as old,
          (
            select content from sys_config
            where type = :type
            order by id desc
            limit 1
          ) as current
        from sys_config
        where type = :type and id = :id
      `,
      {type, id: req.params.version}
    );

    res.nav("sysconf-versions", "p-sysconf-compare", {
      type,
      data: {
        left: old,
        right: current,
        leftLabel: `${req.params.version} *`,
        rightLabel: "Current",
      },
    });
  })
);

// on save user-profile / cluster-setup / slurm
router.post(
  "/:type",
  H.navStayOnError("-no-frame-"),
  asyncRoute(async (req, res) => {
    const {code} = req.body;
    const current = await SC.get(req.params.type, true);
    if (code === "unchanged" || current === code) {
      req.flash("info", "No change detected");
    } else {
      await SC.set(req.params.type, req.user.id, code);
      req.flash("info", "Config updated");
    }
    res.nav("-no-frame-");
  })
);

export default router;
