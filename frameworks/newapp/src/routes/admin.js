import express from "express";
import * as R from "ramda";
import * as H from "./helpers";

import knex from "../transport/database";

import sysConfigRouter from "./admin-sys-config";

const router = express.Router();

const {renderUID, asyncRoute} = H.getUtils(R.last(__filename.split("/")));

router.get(
  "/",
  asyncRoute(async (req, res) => {
    res.redirect("/admin/dashboard");
  })
);

router.get(
  "/dashboard",
  asyncRoute(async (req, res) => {
    renderUID(req, res, "Home", "admin-dashboard", {});
  })
);

router.get(
  "/access-log",
  asyncRoute(async (req, res) => {
    const table = await H.tableExt({
      req,
      options: {
        id: "al",
        sortable: ["created_at"],
        sort: ["created_at", "desc"],
        filterable: {
          message: "freetext",
          status: "select",
          user_login: "select",
          client_ip: "freetext",
          user_agent: "freetext",
          created_at: "date_range",
        },
      },
      sql: `
        -- expand
        select
          id as _id,
          user_login,
          status,
          message,
          client_ip,
          user_agent,
          created_at
        from access_log
      `,
      sqlParams: {},
    });
    renderUID(req, res, "Access Log", "admin-access-log", {table});
  })
);

const getErrorLogs = async (req, options = {}) =>
  H.tableExt({
    req,
    options: {
      id: "al",
      sortable: ["at"],
      sort: ["at", "desc"],
      filterable: {
        req_id: "freetext",
        referer: "freetext",
        user_agent: "select",
        ip_addr: "freetext",
        at: "date_range",
        method: "select",
        url: "freetext",
        body: "freetext",
        summary: "freetext",
        detail: "freetext",
      },
      extraColumns: [
        "method",
        "url",
        "referer",
        "user_agent",
        "ip_addr",
        "body",
        "details",
      ],
      ...options,
    },
    sql: `
      -- expand
      select
        at,
        id as _id,
        referer,
        user_agent,
        ip_addr,
        url,
        body,
        summary,
        details,
        method,
        1 as action
      from error_log
    `,
    sqlParams: {},
  });

router.get(
  "/error-log",
  asyncRoute(async (req, res) => {
    const table = await getErrorLogs(req);
    renderUID(req, res, "Error Log", "admin-error-log", {table});
  })
);

router.get(
  "/error-log/:id",
  asyncRoute(async (req, res) => {
    const detail = await knex("error_log").where({id: req.params.id}).first();

    res.nav("error-log-detail", "p-error-log-detail", {
      detail,
    });
  })
);

router.post(
  "/error-log/delete-filtered",
  asyncRoute(async (req, res) => {
    const table = await getErrorLogs(req, {itemsPerPage: 99999});
    const ids = R.pluck("_id", table.rows);
    await knex("error_log").whereIn("id", ids).del();
    req.flash("info", `${ids.length} error logs removed`);
    res.redirect("/admin/error-log");
  })
);

router.use("/sys-config", sysConfigRouter);

export default router;
