// devbase: ensure=always

import express from "express";
import * as R from "ramda";
import fs from "fs-extra";
import {map, compose} from "crocks";
import {getUtils} from "./helpers";
import * as fcsr from "../lib/fcsr";
import {devWS} from "../devbase_helpers";

const router = express.Router();

const {renderUID, asyncRoute} = getUtils(R.last(__filename.split("/")));

const getInsideQuotes = map(compose(R.nth(1), R.split('"')));

const {packs} =
  process.env.APP_VERSION === "dev"
    ? JSON.parse(fs.readFileSync("src/assets/static/sprite.json", "utf8"))
    : {packs: {}};

router.get(
  "/pick-ssicon",
  asyncRoute(async (req, res) => {
    const svg = await fs.readFile("src/assets/static/sprite.min.svg", "utf8");
    const used = compose(
      R.groupBy((n) => R.split("_", n)[0]),
      getInsideQuotes,
      map(compose(R.last, R.split("id="), R.head, R.split(">"))),
      R.dropLast(1),
      R.drop(1),
      R.split("\n")
    )(svg);

    renderUID(req, res, "Pick ssicon", "pick-ssicon", {
      packs,
      used,
      showAll: req.query.all,
      pack: req.query.pack,
    });
  })
);

router.post(
  "/update-i18n/:lang",
  asyncRoute(async (req, res) => {
    const filepath = `config/locales/translation.${req.params.lang}.json`;
    const tokens = JSON.parse(fs.readFileSync(filepath));
    /* eslint-disable fp/no-loops */
    /* eslint-disable no-restricted-syntax */
    /* eslint-disable guard-for-in */
    for (const key in req.body) {
      if (key !== "_csrf") {
        tokens[key] = req.body[key];
        if (tokens[key] === key || tokens[key] === "") {
          /* eslint-disable fp/no-delete */
          delete tokens[key];
        }
      }
    }

    fs.writeFileSync(filepath, JSON.stringify(tokens, null, 2));

    res.json({
      nav: {
        frame: "i18n-manager",
        flashes: {
          info: ["Saved, reloading ..."],
        },
      },
    });
  })
);

router.get(
  "/on-mixin-changed",
  asyncRoute(async (req, res) => {
    await fcsr.prepareIndex();
    const names = await fcsr.indexOneFile(req.query.p);

    if (R.isNil(names)) {
      return res.json({ok: 1});
    }

    res.json({
      ok: 1,
      index: fcsr.getIndex(),
      mixins: fcsr.getMixins(names),
    });

    // for server side rendering
    req.app.ssr = await fcsr.compileSSR();
  })
);

router.get(
  "/on-mixin-removed",
  asyncRoute(async (req, res) => {
    await fcsr.prepareIndex();
    await fcsr.onMixinFileRemoved(req.query.p);
    // for server side rendering
    req.app.ssr = await fcsr.compileSSR();
    res.json({ok: 1});
  })
);

router.get(
  "/refresh-browser",
  asyncRoute(async (req, res) => {
    devWS({cmd: "refresh"});
    res.json({ok: 1});
  })
);

export default router;
