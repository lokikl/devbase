/* eslint-disable import/no-import-module-exports */
import * as R from "ramda";
import express from "express";
import UAParser from "ua-parser-js";
import {compose} from "crocks";
import axios from "axios";
/* import URL from "url"; */
import {scryptSync, timingSafeEqual} from "crypto";

import {t, defaultLanguage} from "../frontend-helpers";
import * as H from "./helpers";
import devbaseRouter from "./devbase";
import tenantRouter from "./tenant";
import adminRouter from "./admin";
import knex from "../transport/database";
import * as fcsr from "../lib/fcsr";
/* import * as SC from "../lib/sys-config"; */

const router = express.Router();

const {asyncRoute, renderUID} = H.getUtils(R.last(__filename.split("/")));

if (process.env.APP_VERSION === "dev") {
  router.use("/_", devbaseRouter);
}

router.use((req, res, next) => {
  res.locals.titleAffix = "<%= project_name %>";

  req.session.currentLang ||= defaultLanguage;
  res.locals.currentLang = req.session.currentLang;
  req.app.locals.i18next.changeLanguage(req.session.currentLang);

  res.locals.currentMode = req.session.currentMode || "dark";

  if (req.app.get("env") === "development") {
    res.on("finish", async () => {
      if (R.isNil(res.handlerFile)) return;
      if (req.get("devhost")) return;
      if (R.isNil(req.route)) return;

      axios.post("http://localhost:3001/report", {
        method: req.method, // GET / POST
        label: R.join("!!", [res.handlerFile, req.method, req.route.path]),
        host: req.hostname,
        url: `${req.baseUrl}${req.url}`,
        headers: req.headers,
        body: req.body,
        status: res.statusCode,
      });
    });
  }
  if (req.headers["redirect-to"]) {
    // for res.redirect("back") to work inside navframe
    req.headers.referer = req.headers["redirect-to"];
  }

  // navframe responder
  res.nav = (frame, mixin, params = {}) => {
    if (process.env.APP_VERSION === "dev") {
      axios
        .post("http://localhost:3001/report-mixin", {
          name: mixin,
        })
        .catch(R.identity);
    }

    res.json({
      nav: {
        frame,
        mixin,
        baseUrl: req.baseUrl,
        currentUrl: `${req.baseUrl}${req.url}`,
        flashes: H.allFlashes(req),
      },
      ...params,
    });
  };
  next();
});

router.get(
  "/uid/index",
  asyncRoute(async (req, res) => {
    await fcsr.prepareIndex();
    res.json({index: fcsr.getIndex()});
  })
);

router.get(
  "/uid/get-mixins/:mixins",
  asyncRoute(async (req, res) => {
    await fcsr.prepareIndex();
    const mixins = fcsr.getMixins(req.params.mixins.split(","));
    res.json({mixins});
  })
);

// guess device from UA
router.use((req, _res, next) => {
  if (!req.session.device) {
    const ua = UAParser(req.headers["user-agent"]);

    req.session.device = R.includes(ua.device.type, ["mobile", "tablet"])
      ? "mobile"
      : "desktop";
  }
  next();
});

router.get("/rwd/:d", (req, res) => {
  req.session.device = req.params.d;
  res.redirect("back");
});

router.post(
  "/switch-lang/:lng",
  asyncRoute(async (req, res) => {
    const {lng} = req.params;
    if (req.session.userId) {
      await H.mergeJSONB("user_profile", {id: req.session.userId}, "config", {
        language: lng,
      });
    }
    req.session.currentLang = lng;
    res.redirect("back");
  })
);

router.post(
  "/switch-mode",
  asyncRoute(async (req, res) => {
    const mode = res.locals.currentMode === "dark" ? "light" : "dark";
    if (req.session.userId) {
      await H.mergeJSONB("user_profile", {id: req.session.userId}, "config", {
        mode,
      });
    }
    req.session.currentMode = mode;
    res.redirect("back");
  })
);

const ensureNotAuthed = asyncRoute(async (req, res, next) => {
  if (req.session.userId) {
    return res.redirect("/");
  }
  next();
});

const ensureAuthed = asyncRoute(async (req, res, next) => {
  if (!req.session.userId) {
    return res.redirect("/login");
  }
  const {
    rows: [user],
  } = await knex.raw(
    `
      -- expand
      -- id: 'admin'
      select
        u.id,
        email,
        default_account,
        u.config - 'publicKey' - 'privateKey' as config,
        jsonb_agg(
          jsonb_build_object(
            'id', a.id,
            'type', a.type,
            'gid', a.config->>'gid'
          )
        ) as accounts
      from user_profile u
      left join membership m on m.user_profile_id = u.id
      left join account a on m.account_id = a.id
      where u.id = :id
      and u.status = 'active'
      and a.status = 'active'
      group by u.id
    `,
    {id: req.session.userId}
  );
  if (!user) {
    req.session.destroy();
    return res.redirect("/login");
  }

  user.hasTenantAccess = !(
    user.accounts.length === 1 && user.accounts[0].id === "root"
  );
  user.hasAdminAccess = R.any(R.propEq("id", "root"), user.accounts);
  req.user = user;
  res.locals.uidLocals ||= {};

  res.locals.uidLocals.session = compose(
    R.assoc("authSource", req.session.saml ? "saml" : "ldap"),
    R.assoc("path", res.locals.pathname),
    R.assoc("hasTenantAccess", req.user.hasTenantAccess),
    R.assoc("hasAdminAccess", req.user.hasAdminAccess)
  )(user);
  next();
});

const ensureTenant = asyncRoute(async (req, res, next) => {
  if (!req.user.hasTenantAccess) {
    return res.redirect("/admin");
  }
  next();
});

const ensureAdmin = asyncRoute(async (req, res, next) => {
  if (!req.user.hasAdminAccess) {
    return res.redirect("/");
  }
  next();
});

const onSignedIn = async (user, req, res) => {
  await H.mergeJSONB("user_profile", {id: user.id}, "config", {
    lastLoginAt: new Date(),
  });
  req.session.userId = user.id;
  req.session.currentLang = user.config.language;
  req.session.currentMode = user.config.mode;
  if (req.session.redirectAfterLogin) {
    res.redirect(req.session.redirectAfterLogin);
    req.session.redirectAfterLogin = null;
  } else {
    res.redirect("/");
  }
};

const getIpFromRequest = (req) => {
  const ips = (
    req.headers["cf-connecting-ip"] ||
    req.headers["x-real-ip"] ||
    req.headers["x-forwarded-for"] ||
    req.connection.remoteAddress ||
    ""
  ).split(",");

  return ips[0].trim();
};

const recordLoginSuccess = async ({req, userLogin}) => {
  await knex("access_log").insert({
    user_login: userLogin,
    status: "success",
    message: "Login success",
    user_agent: req.get("User-Agent"),
    client_ip: getIpFromRequest(req),
  });
};

const recordLoginFailure = async ({req, userLogin, message}) => {
  await knex("access_log").insert({
    user_login: userLogin,
    status: "failed",
    message,
    user_agent: req.get("User-Agent"),
    client_ip: getIpFromRequest(req),
  });
};

router.post(
  "/logout",
  ensureAuthed,
  asyncRoute(async (req, res) => {
    /* if (req.session.saml) { */
    /*   const {nameId, sessionId, entityId} = req.session.saml; */
    /*   req.session.destroy(); */
    /*   const options = { */
    /*     name_id: nameId, */
    /*     session_index: sessionId, */
    /*   }; */
    /*   const {idp, supportLogout} = await getIdp(entityId); */
    /*   if (supportLogout) { */
    /*     sp.create_logout_request_url(idp, options, (err, url) => { */
    /*       if (err != null) return res.send(500); */
    /*       res.redirect(url); */
    /*     }); */
    /*   } else { */
    /*     res.redirect("back"); */
    /*   } */
    /* } else { */
    req.session.destroy();
    res.redirect("back");
    /* } */
  })
);

router.get(
  "/login",
  ensureNotAuthed,
  asyncRoute(async (req, res) => {
    renderUID(req, res, t`Sign in`, "login", {
      recaptchaSiteKey: process.env.RECAPTCHA_SITE_KEY,
    });
  })
);

router.post(
  "/signin",
  ensureNotAuthed,
  asyncRoute(async (req, res) => {
    const {username, password} = req.body;
    /* const params = new URL.URLSearchParams({ */
    /*   secret: process.env.RECAPTCHA_SECRET, */
    /*   response: req.body["g-recaptcha-response"], */
    /* }); */
    /* const response = await axios.post( */
    /*   "https://www.google.com/recaptcha/api/siteverify", */
    /*   params.toString() */
    /* ); */
    /* if (!response.data.success) { */
    /*   throw new Error("login fail: Failed captcha verification"); */
    /* } */
    const {
      rows: [user],
    } = await knex.raw(
      `
        -- expand
        -- id: 'loki'
        select id, password_hash, config
        from user_profile
        where id = :id
        and status = 'active'
      `,
      {id: username}
    );

    if (!user) {
      await recordLoginFailure({
        req,
        userLogin: username,
        message: "Login failed - User not found",
      });
      throw new Error("User not found");
    }
    const [salt, realPasswd] = user.password_hash.split(":");
    const hashed = scryptSync(password, salt, 64);
    if (!timingSafeEqual(hashed, Buffer.from(realPasswd, "hex"))) {
      await recordLoginFailure({
        req,
        userLogin: user.id,
        message: "Login failed - Invalid credentials",
      });
      throw new Error("Invalid credentials");
    }
    await recordLoginSuccess({req, userLogin: user.id});
    await onSignedIn(user, req, res);
  })
);

router.use("/admin", ensureAuthed, ensureAdmin, adminRouter);
router.use("/", ensureAuthed, ensureTenant, tenantRouter);

export default router;
