import * as R from "ramda";
import express from "express";
import {
  scryptSync,
  randomBytes,
  generateKeyPair,
  timingSafeEqual,
} from "crypto";
import {compose, map, curry} from "crocks";
import {exec} from "child_process";
import qs from "qs";
import multer from "multer";
import axios from "axios";
import fs from "fs-extra";
import sshpk from "sshpk";
import puppeteer from "puppeteer";
import {exportXlsx, sqlToTableExt} from "../lib/table-ext";
import knex from "../transport/database";
import * as F from "../frontend-helpers";
/* import * as SC from "../lib/sys-config"; */

const getErrStackTrace = (err) => {
  const sp = err.stack ? err.stack.search(/\/app\/src/) : -1;
  if (sp === -1) {
    return "<no stack trace>";
  }
  const cc = err.stack.slice(sp).search(/\n/);
  return err.stack.slice(0, sp + cc).replaceAll("    ", "");
};

const sanitize = R.mapObjIndexed((v, k) =>
  R.includes("password", k) ? "--sanitized--" : v
);

const logSysErr = async (req, err) => {
  try {
    await knex("error_log").insert({
      referer: req.get("Referrer") || "",
      user_agent: req.get("user-agent"),
      ip_addr: req.ip,
      at: new Date(),
      method: req.method,
      url: `${req.protocol}://${req.get("host")}${req.originalUrl}`,
      body: JSON.stringify(sanitize(req.body)),
      summary: `${err}`,
      details: getErrStackTrace(err),
    });
  } catch (errMsg) {
    console.log({
      req_id: req.id,
      referer: req.get("Referrer") || "",
      user_agent: req.get("user-agent"),
      ip_addr: req.ip,
      at: new Date(),
      method: req.method,
      url: `${req.protocol}://${req.get("host")}${req.originalUrl}`,
      body: JSON.stringify(sanitize(req.body)),
      summary: `${err}`,
      details: getErrStackTrace(err),
      errMsg,
    });
  }
};

export const setParams = (req, res, next) => {
  req.topParams = R.mergeRight(req.topParams || {}, req.params);
  next();
};

export const logEvent = (action) => (req, res, next) => {
  /* eslint-disable fp/no-mutation */
  res.eventLogData = {action, details: []};
  next();
};

export const allFlashes = (req) =>
  R.pickBy(R.complement(R.isEmpty), {
    /* "<frame name>;<url (optional)>" */
    reloadOnNavframeClose: req.flash("reload-on-navframe-close"),
    closeUidPromptByNavframe: req.flash("close-uid-prompt-by-navframe"),
    reloadNavframe: req.flash("reload-navframe"),
    setInputValue: req.flash("set-input-value"),
    info: req.flash("info"),
    warn: req.flash("warn"),
    error: req.flash("error"),
  });

export const navStayOnError = (frame) => (req, res, next) => {
  req.navOnError = ["alert-and-stay", frame];
  next();
};

export const errorInJson = (req, res, next) => {
  req.errorInJson = true;
  next();
};

const noCacheHeaders = {
  "Cache-Control": "no-cache, no-store, must-revalidate",
  Pragma: "no-cache",
  Expires: "0",
};

export const getUtils = (label) => {
  const renderUID = async (req, res, title, mixin, params) => {
    const final = R.pipe(
      R.defaultTo({}),
      R.mergeRight(params)
    )(res.locals.uidLocals);

    if (req.app.locals.appVersion === "dev") {
      axios
        .post("http://localhost:3001/report-mixin", {
          name: mixin,
        })
        .catch(R.identity);

      if (fs.existsSync(".mixin-outsync")) {
        res.locals.mixinOutsync = true;
        fs.removeSync(".mixin-outsync");
      }
    }

    const output = {
      title: R.pipe(
        R.append(res.locals.titleAffix),
        R.reject(R.isNil),
        R.join(" | ")
      )([title]),
      mixin,
      params: final,
      baseUrl: req.baseUrl,
      currentUrl: `${req.baseUrl}${req.url}`,
    };
    res.set(noCacheHeaders);
    if (req.get("async")) {
      return res.json({
        ...output,
        flashes: allFlashes(req),
      });
    }
    // res.render("uid", output, callback);
    const args = {...res.app.locals, ...res.locals, ...output};
    if (!req.app.ssr) {
      return res.send("rendering engine not yet ready");
    }
    F.clearLastTokens();
    const html = req.app.ssr(args);
    if (res.locals.forPrint) {
      return html;
    }
    res.send(html);
  };
  return {
    renderUID,
    renderUIDToText: curry(async (req, res, mixin, params) => {
      res.locals.forPrint = true;
      return renderUID(req, res, "Whatever", mixin, params);
    }),
    asyncRoute:
      (route, noRedirect = false) =>
      (req, res, next = console.error) => {
        res.handlerFile = label;

        Promise.resolve(route(req, res, next))
          .then(() => {})
          .catch((err) => {
            console.log(err);
            // .detail is from knex, .message is typical js error object, or it's just a string
            const errMsg =
              err.routine && err.routine === "exec_stmt_raise"
                ? R.last(err.message.split(" - "))
                : // : err.constraint
                  // ? err.constraint.replace(/_/g, " ")
                  err.detail || err.message || err;
            if (req.method === "POST") {
              console.log(`asyncRoute Error (POST): ${errMsg}`);
              if (noRedirect) {
                res.json({errMsg});
              } else if (req.errorInJson) {
                res.status(500);
                res.json({error: errMsg});
              } else {
                if (req.flash) {
                  req.flash("error", errMsg);
                }
                if (req.navOnError) {
                  const [, frame] = req.navOnError;

                  res.json({
                    nav: {
                      frame,
                      baseUrl: res.req.baseUrl,
                      flashes: allFlashes(res.req),
                    },
                  });
                } else {
                  res.redirect("back");
                }
              }
            } else {
              console.log(`asyncRoute Error (GET): ${errMsg}`);
              next(err);
            }
            req.err = err; // for event logging in src/routes/index.js
            if (res.eventLogData) {
              res.eventLogData.details = R.append(
                `Error: ${err}`,
                res.eventLogData.details
              );
            }
            logSysErr(req, err);
          });
      },
  };
};

export const keyize = (name) =>
  name
    .toLowerCase()
    .replace(/[^\w]+/g, "-")
    .replace(/-$/, "");

export const capitalize = (name) =>
  name.charAt(0).toUpperCase() + name.slice(1);

export const titleize = R.pipe(
  R.split(/[\s-_]/),
  R.map((str) => capitalize(str)),
  R.join(" ")
);

export const trimHeredoc = (doc) => {
  const lines = doc.split("\n").slice(1, -1);
  const nSpaces = lines[0].match("^ *")[0].length;
  return R.pipe(
    R.map((line) => line.slice(nSpaces)),
    R.join("\n")
  )(lines);
};

const mapIndexed = R.addIndex(R.map);

const processArrayInBody = (req, res, next) => {
  const temp = R.pipe(
    R.keys,
    R.reduce((out, key) => {
      const value = req.body[key];
      return R.is(Array, value) && R.includes("[]", key)
        ? R.pipe(
            mapIndexed((v, i) => [key.replace("[]", `[${i}]`), v]),
            R.fromPairs,
            R.mergeLeft(out)
          )(value)
        : R.assoc(key, value, out);
    }, {})
  )(req.body);

  req.body = qs.parse(qs.stringify(temp), {
    allowDots: false,
    arrayLimit: 99999,
  });
  return next();
};

export const parseBody = [
  multer({dest: "/tmp/"}).array("files"),
  processArrayInBody,
];

export const ensureHosts = (hosts, router) => (req, res, next) => {
  const host = req.get("host");
  if (req.app.locals.appVersion === "dev" && R.endsWith("ngrok.io", host)) {
    return router(req, res, next);
  }
  if (R.includes(host, hosts)) {
    return router(req, res, next);
  }
  return next();
};

export const parseValue = (req, res, next) => {
  req.value = R.trim(req.body.value || "");
  return next();
};

export const ensureValue = (req, res, next) => {
  req.value = R.trim(req.body.value || "");
  if (req.value === "") {
    req.flash("error", "Input is expected but missing");
    res.redirect("back");
  } else {
    next();
  }
};

export const anonymousRouter = (callback) => callback(express.Router());

export const redirectMiddleware = (path) => (req, res) =>
  res.redirect(req.baseUrl + path);

export const tableExt = async ({req, db = knex, options, sql, sqlParams}) =>
  sqlToTableExt(db)(sql)(req.query)(options)(sqlParams);

export const exportTableAsXlsx = (table, filename, req, res) => {
  const columns = compose(
    R.fromPairs,
    map((k) => [k, {displayName: k, width: 200}]),
    R.reject(R.startsWith("_")),
    R.without(["actions"])
  )(table.fields);
  const xlsx = exportXlsx([
    {
      headerStyle: {
        fill: {fgColor: {rgb: "FFEEEEEE"}},
        font: {sz: 12},
      },
      cellStyle: {font: {sz: 12}},
      columns,
      data: table.rows,
    },
  ]);
  res.attachment(filename);
  res.send(xlsx);
};

/* eslint-disable no-unused-expressions */
export const execP = (cli, options) =>
  new Promise((resolve, reject) => {
    exec(cli, options, (error, stdout) => {
      error ? reject(error.message) : resolve(stdout);
    });
  });

export const delay = (ms) =>
  new Promise((resolve) => {
    setTimeout(resolve, ms);
  });

// merge jsonb into a jsonb column
export const mergeJSONB = async (table, where, col, data) =>
  knex(table)
    .where(where)
    .update({
      [col]: knex.raw(`${col} || :data`, {data}),
    });

// drop jsonb object by key
export const jsonbDropKeys = async (table, where, col, keys, trx) =>
  (trx || knex)(table)
    .where(where)
    .update({
      [col]: knex.raw(`${col} - (:keys ::text[])`, {keys}),
    });

// drop jsonb array by idx
export const jsonbDrop = async (table, where, col, idx, trx) =>
  (trx || knex)(table)
    .where(where)
    .update({
      [col]: knex.raw(`${col} - (:idx ::int)`, {idx}),
    });

export const renderPDFBuffer = async (html) => {
  const browser = await puppeteer.launch({
    headless: true,
    args: ["--no-sandbox", "--disable-setuid-sandbox"],
  });
  const pupPage = await browser.newPage();
  await pupPage.setRequestInterception(true);
  const fakeUrl = "http://127.0.0.1:3000/";

  pupPage.on("request", (request) => {
    if (request.url() === fakeUrl) {
      request.respond({
        contentType: "text/html; charset=utf-8",
        body: html,
      });
    } else {
      request.continue();
    }
  });
  await pupPage.goto(fakeUrl);
  const pdfBuffer = await pupPage.pdf({
    format: "A4",
    margin: {
      top: "0.5in",
      bottom: "0.5in",
      left: "0.5in",
      right: "0.5in",
    },
  });
  await browser.close();
  return pdfBuffer;
};

// usage:
// const html = await renderUIDToText(
//   req,
//   res,
//   "designer-preview",
//   pageParams
// );
// await H.renderPDF(res, html);
export const renderPDF = async (res, html, filename) => {
  const buffer = await renderPDFBuffer(html);

  res.set({
    "Content-Type": "application/pdf",
    "Content-Length": buffer.length,
    "Content-Disposition": `inline; filename="${filename}"`,
  });
  res.end(buffer);
};

export const genKeyPair = () =>
  new Promise((res, rej) => {
    generateKeyPair(
      "rsa",
      {
        modulusLength: 2048,
        publicKeyEncoding: {
          type: "spki",
          format: "pem",
        },
        privateKeyEncoding: {
          type: "pkcs1",
          format: "pem",
        },
      },
      (err, publicKey, privateKey) => {
        if (err) {
          return rej(err);
        }
        const key = sshpk.parseKey(publicKey, "pem");
        return res({publicKey: key.toString("ssh"), privateKey});
      }
    );
  });

/* eslint-disable fp/no-mutating-methods */
export const streamToString = (stream) => {
  const chunks = [];
  return new Promise((resolve, reject) => {
    stream.on("data", (chunk) => chunks.push(Buffer.from(chunk)));
    stream.on("error", (err) => reject(err));
    stream.on("end", () => resolve(Buffer.concat(chunks).toString("utf8")));
  });
};

/* eslint-enable fp/no-mutating-methods */
export const resetPeriodOptions = [
  "now", // only directly reset usage, not modify current reset_period
  "none",
  "daily",
  "weekly",
  "monthly",
  "quarterly",
  "yearly",
];

export const getNextUID = async () => {
  const {uid} = (
    await knex.raw(
      `
        select max((config->>'uid')::int) as uid
        from user_profile
        where config->'uid' is not null
      `
    )
  ).rows[0];
  const nextUid = (uid || 1000) + 1;
  if (nextUid > 65535) {
    throw new Error("Failed to get the next Posix UID, please contact admin.");
  }
  return nextUid;
};

const reservedLoginIds = [
  "root",
  "bin",
  "daemon",
  "adm",
  "lp",
  "sync",
  "shutdown",
  "halt",
  "mail",
  "operator",
  "games",
  "ftp",
  "nobody",
  "tss",
  "dbus",
  "systemd-coredump",
  "systemd-resolve",
  "unbound",
  "mysql",
  "munge",
  "slurm",
  "sssd",
  "sshd",
];

export const validateLoginId = (id) => {
  if (R.includes(id, reservedLoginIds)) {
    throw new Error("Reserved login ID");
  }
};

export const checkRequiredFieldInConfig = ({
  profileCols,
  config,
  extraColCodes,
  id,
}) => {
  const requiredCols = compose(
    R.concat(extraColCodes),
    R.pluck("code"),
    R.filter(R.propEq("required", true))
  )(profileCols);

  R.forEach((colCode) => {
    if (R.isNil(config[colCode])) {
      console.log("colCode is not exist", config[colCode]);
      throw new Error(`${id}(${colCode}) is required`);
    }
  }, requiredCols);
};

export const generatePasswordHash = (password) => {
  if (!password) {
    return "";
  }
  const newSalt = randomBytes(16).toString("hex");
  const newHash = scryptSync(password, newSalt, 64).toString("hex");
  return `${newSalt}:${newHash}`;
};

export const verifyLoginPassword = (hash, password) => {
  const [salt, realPasswd] = hash.split(":");
  if (R.isNil(realPasswd)) {
    return false;
  }
  const hashed = scryptSync(password, salt, 64);
  return timingSafeEqual(hashed, Buffer.from(realPasswd, "hex"));
};

/* eslint-disable fp/no-mutating-methods */
export const trim = (strings, ...args) => {
  const result = [];

  args.forEach((arg, i) => {
    result.push(strings[i], arg);
  });
  const text = result.join("") + strings[strings.length - 1];

  const nSpaces = text.split("\n")[1].match(/^ */)[0].length;

  return R.compose(
    R.slice(1, -1),
    R.join("\n"),
    R.map((line) => line.slice(nSpaces)),
    R.split("\n")
  )(text);
};
