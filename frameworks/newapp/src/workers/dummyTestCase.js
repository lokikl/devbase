const delay = (ms) =>
  new Promise((resolve) => {
    setTimeout(resolve, ms);
  });

export default {
  // schedule: "0 4 3 * *",
  /* startOnBoot: true, */
  /* throttle: 5000, */
  run: async () => {
    console.log(`running dummyTestCase ${new Date()}`);
    await delay(1000);
  },
};
