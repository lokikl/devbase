import {compose} from "crocks";
// used in uid
import * as R from "ramda";
import i18next from "i18next";
import translationEN from "../config/locales/translation.en_US.json";
import translationCHT from "../config/locales/translation.zh_TW.json";
import translationCHS from "../config/locales/translation.zh_CN.json";
import translationJP from "../config/locales/translation.ja_JP.json";
import translationKO from "../config/locales/translation.ko_KR.json";

export const languageOptions = [
  ["en_US", "English"],
  ["zh_TW", "繁體中文"],
  ["zh_CN", "简体中文"],
  ["ja_JP", "日本語"],
  ["ko_KR", "한국어"],
];
export const defaultLanguage = "en_US";

i18next.init({
  lng: defaultLanguage,
  resources: {
    en_US: {
      translation: translationEN,
    },
    zh_TW: {
      translation: translationCHT,
    },
    zh_CN: {
      translation: translationCHS,
    },
    ja_JP: {
      translation: translationJP,
    },
    ko_KR: {
      translation: translationKO,
    },
  },
});

export const languages = R.map((lng) => lng[0], languageOptions);

export {R, i18next};

const lastTokens = [];
export const clearLastTokens = () => {
  /* eslint-disable fp/no-mutating-methods */
  lastTokens.splice(0, lastTokens.length);
};
export const getLastTokens = () => R.uniq(lastTokens);

export const t = (strings, ...args) => {
  const token = strings.join("$$");
  lastTokens.push(token);
  const [head, ...tails] = i18next.t(token).split("$$");
  return R.reduce(
    (txt, idx) => `${txt}${args[idx]}${tails[idx]}`,
    head,
    R.range(0, tails.length)
  );
};

export const rem = (px) => `${px / 16.0}rem`;
export const em = (px) => `${px / 16.0}em`;

export const convertStyles = (attributes) =>
  R.pipe(
    R.toPairs,
    R.map(([k, v]) =>
      R.has(k, attributes) ? R.join(":", [v, attributes[k]]) : null
    ),
    R.prepend(attributes.style),
    R.without([null]),
    R.join(";")
  );

export const gridStyles = {
  columns: "grid-template-columns",
  rows: "grid-template-rows",
  gap: "grid-gap",
  "align-items": "align-items",
  "justify-content": "justify-content",
};

export const keyize = (name) =>
  name
    .toLowerCase()
    .replace(/[^\w]+/g, "-")
    .replace(/-$/, "");

export const capitalize = (name) =>
  name.charAt(0).toUpperCase() + name.slice(1);

export const titleize = R.pipe(
  R.split(/[\s-_]/),
  R.map((str) => capitalize(str)),
  R.join(" ")
);

export const formatTimeHMS = (d) =>
  R.join(":", [
    d.getHours(),
    String(d.getMinutes()).padStart(2, "0"),
    String(d.getSeconds()).padStart(2, "0"),
  ]);

// 11:59
export const formatTime = (d) =>
  `${d.getHours()}:${String(d.getMinutes()).padStart(2, "0")}`;

// 2021.03.16
export const formatDate = (d) => {
  const dd = String(d.getDate()).padStart(2, "0");
  const mm = String(d.getMonth() + 1).padStart(2, "0"); // January is 0!
  return `${d.getFullYear()}.${mm}.${dd}`;
};

export const addDays = (days, date) => {
  const d = new Date(date);
  d.setDate(d.getDate() + days);
  return d;
};

export const addMonths = (months, date) => {
  const d = new Date(date);
  d.setMonth(d.getMonth() + months);
  return d;
};

// 2021.03
export const formatMonth = (d) => formatDate(d).replace(/.\d\d$/, "");

// 2021.03.16 11:59
export const formatDateTime = (d) => `${formatDate(d)} ${formatTime(d)}`;

// 2021.03.16 11:59 HKT
export const formatDateTimeZone = (d) => {
  const tz = new Date()
    .toLocaleTimeString("en-us", {timeZoneName: "short"})
    .split(" ")[2];
  return `${formatDateTime(d)} ${tz}`;
};

// (2021.11, 2022.01) => [2021.11, 2021.12, 2022.01]
export const monthRange = (startMonth, endMonth) => {
  if (startMonth === endMonth) {
    return [startMonth];
  }
  const nextMonth = formatMonth(addMonths(1, new Date(startMonth)));
  return [startMonth, ...monthRange(nextMonth, endMonth)];
};

export const timeElapsed = (secs, take = 999) => {
  const secsNum = parseInt(secs, 10);
  const days = Math.floor(secsNum / 3600 / 24);
  const hours = Math.floor((secsNum - days * 24 * 3600) / 3600);
  const minutes = Math.floor((secsNum - days * 24 * 3600 - hours * 3600) / 60);
  const seconds = secsNum - days * 24 * 3600 - hours * 3600 - minutes * 60;
  const format = compose(R.join(""), R.take(take), R.dropWhile(R.equals("")));
  return format([
    days > 0 ? `${days}d` : "",
    hours > 0 ? `${hours}h` : "",
    minutes > 0 ? `${minutes}m` : "",
    seconds >= 0 ? `${seconds}s` : "",
  ]);
};

export const formatPrice = (number) =>
  number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

export const formatSize = (bytes, si = false, dp = 1) => {
  const thresh = si ? 1000 : 1024;

  if (Math.abs(bytes) < thresh) {
    return `${bytes} B`;
  }

  const units = si
    ? ["kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
    : ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"];
  const r = 10 ** dp;

  const convert = (b, u = -1) => {
    if (Math.round(Math.abs(b) * r) / r >= thresh && u < units.length - 1) {
      return convert(b / thresh, u + 1);
    }
    return {b, u};
  };
  const {b, u} = convert(bytes);
  return `${b.toFixed(dp)} ${units[u]}`;
};
export const getIsMobile = () => {
  const w = $(document).innerWidth();
  return w < 992 || $(".detect-rwd").data("current") === "mobile";
};
