import pug from "pug";
import * as R from "ramda";
import {map, compose} from "crocks";
import * as fs from "fs-extra";
import chalk from "chalk";

import {glob as _glob} from "glob";
import md5File from "md5-file";

/* eslint-disable fp/no-loops */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
/* eslint-disable guard-for-in */
/* eslint-disable fp/no-mutating-methods */

const glob = (query) =>
  new Promise((resolve) => _glob(query, {}, (er, files) => resolve(files)));

export const getPugMeta = () => {
  const pugSrc = "mixin a(a)\n  div= a\n//replaceme\n+#{mixin}(params)";
  const code = pug.compileClient(pugSrc, {
    compileDebug: false,
  });
  return compose(
    ([s, e]) => ({
      s: s.join("\n"),
      e: R.drop(1, e).join("\n"),
    }),
    R.groupWith((_, b) => !R.includes("--replaceme--", b)),
    R.split("\n")
  )(code);
};

const splitLines = (regexp) =>
  compose(
    (arr) => (R.test(regexp, arr[0][0]) ? arr : R.drop(1, arr)),
    R.groupWith((a, b) => !R.test(regexp, b))
  );

const parsePugMixinSection = (pugSrc) => {
  const modified = ["// start here", pugSrc, "//end here", "+#{mixin}()"].join(
    "\n"
  );
  const code = pug.compileClient(modified, {
    compileDebug: false,
  });
  return compose(
    R.slice(1, -1),
    R.dropLastWhile(R.complement(R.includes("end here"))),
    R.dropWhile(R.complement(R.includes("start here"))),
    R.split("\n"),
    R.replace(/pug_merge/g, "pug.merge"),
    R.replace(/pug_classes/g, "pug.classes"),
    R.replace(/pug_style/g, "pug.style"),
    R.replace(/pug_attr/g, "pug.attr"),
    R.replace(/pug_attrs/g, "pug.attrs"),
    R.replace(/pug_escape/g, "pug.escape"),
    R.replace(/pug_rethrow/g, "pug.rethrow"),
    R.replace(/pug_html/g, "ctx.html"),
    R.replace(/pug_interp/g, "ctx.interp"),
    R.replace(/pug_mixins/g, "ctx.mixins"),

    R.replace(/asset/g, "ctx.asset"),
    R.replace(/appVersion/g, "ctx.appVersion"),
    R.replace(/siteName/g, "ctx.siteName"),
    R.replace(/baseUrl/g, "ctx.baseUrl"),
    R.replace(/currentUrl/g, "ctx.currentUrl"),
    R.replace(/isMobile/g, "ctx.isMobile"),
    R.replace(/csrfToken/g, "ctx.csrfToken"),
    R.replace(/currentLang/g, "ctx.currentLang"),
    R.replace(/currentMode/g, "ctx.currentMode")
  )(code);
};

export const parsePugMixins = (pugSrc) =>
  compose(
    R.fromPairs,
    map((t) => [t[0].split('"')[1], t.join("\n")]),
    map(parsePugMixinSection),
    map(R.join("\n")),
    splitLines(/^mixin /),
    R.split("\n")
  )(pugSrc);

const mixinFiles = {};
const compiled = {};

export const getMixins = (names) =>
  compose(R.pluck("content"), R.pick(names))(compiled);

export const getIndex = () => R.mapObjIndexed(R.prop("fdeps"), compiled);

const getInsideQuotes = map(compose(R.nth(1), R.split('"')));

const analyzeContent = (key) => {
  const meta = compiled[key];
  const m = R.match(/ctx.mixins\["[a-zA-Z0-9-_]+"\]../g, meta.content);
  const declared = compose(
    R.without([key]),
    getInsideQuotes,
    R.filter(R.endsWith(" ="))
  )(m);
  const refsExplicit = compose(getInsideQuotes, R.reject(R.endsWith(" =")))(m);
  const refsCommented = compose(
    map(compose(R.dropLast(2), R.last, R.split(" "))),
    R.match(/- ?require: ?[a-zA-Z0-9-_]+/g)
  )(meta.content);
  const refs = R.uniq(R.concat(refsExplicit, refsCommented));
  meta.deps = compose(R.without(key), R.without(declared))(refs);
  meta.internals = declared;
};

const deriveFullDeps = () => {
  for (const key in compiled) {
    const m = compiled[key];
    m.fdeps = R.clone(m.deps);
    const q = R.clone(m.deps);
    while (!R.isEmpty(q)) {
      const k = q.pop();
      if (R.has(k, compiled)) {
        const {deps} = compiled[k];
        const newkeys = compose(R.without(q), R.without(m.fdeps))(deps);
        for (const nk of newkeys) {
          m.fdeps.push(nk);
          q.push(nk);
        }
      }
    }
  }
};

export const indexOneFile = async (f) => {
  const filemd5 =
    process.env.APP_VERSION === "dev" ? md5File.sync(f) : "-no-matters-";
  if (mixinFiles[f] && mixinFiles[f].md5 === filemd5) {
    console.log(`${f} has no change since last index`);
    return;
  }
  const c = await fs.readFile(f, "utf8");

  mixinFiles[f] = {
    content: c,
    md5: filemd5,
  };

  const mixins = await parsePugMixins(c);
  for (const name in mixins) {
    compiled[name] = {
      content: mixins[name],
    };
  }
  const names = R.keys(compiled);
  for (const key in mixins) {
    analyzeContent(key, names);
  }
  deriveFullDeps();
  return R.keys(mixins);
};

/* eslint-disable fp/no-delete */
export const onMixinFileRemoved = async (f) => {
  const c = mixinFiles[f].content;
  const mixins = await parsePugMixins(c);
  for (const name in mixins) {
    delete compiled[name];
  }
  delete mixinFiles[f];
};

export const prepareIndex = async () => {
  if (!R.isEmpty(compiled)) {
    return;
  }
  const files = await glob("./src/mixins/*.pug");
  // read and parse
  for (const f of files) {
    await indexOneFile(f);
  }
  // get immediate dependencies
  const names = R.keys(compiled);
  for (const key in compiled) {
    analyzeContent(key, names);
  }
  // derive full dependencies
  deriveFullDeps();
};

const validateMixins = () => {
  const keys = R.keys(compiled);
  for (const key of keys) {
    const missings = R.without(keys, compiled[key].deps);
    const r = chalk.redBright;
    const err = r("");
    if (missings.length > 0) {
      const m = r(missings.join(","));
      console.log(`${err} mixin ${r(key)} has unknown references: ${m}`);
    }
    const overrideGlobals = R.intersection(keys, compiled[key].internals);
    if (overrideGlobals.length > 0) {
      const m = r(overrideGlobals.join(","));
      console.log(`${err} mixin ${r(key)} overrides global mixins: ${m}`);
    }
  }
};

export const compileSSR = async () => {
  console.log(`[recompile for ssr]`);
  const src = await fs.readFile("src/views/uid.pug", "utf8");
  const [f1, f2] = src.split("//- inject mixins");
  const pugSrc = compose(
    (str) => f1 + str + f2,
    R.join("\n\n"),
    R.pluck("content"),
    R.values
  )(mixinFiles);
  if (process.env.APP_VERSION === "dev") {
    validateMixins();
  }
  return pug.compile(pugSrc);
};
