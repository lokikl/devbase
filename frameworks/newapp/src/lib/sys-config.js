import * as R from "ramda";
import yaml from "js-yaml";
import fs from "fs-extra";
import knex from "../transport/database";

/* eslint-disable fp/no-loops */
/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
const validateProps = (fields, content) => {
  for (const key in fields) {
    const fn = fields[key];
    const v = content[key];
    if (R.isNil(v) || !fn(v)) {
      throw new Error(`${key} is either missing or invalid`);
    }
  }
};

const types = {
  "general-settings": {
    path: "config/general-settings.yml",
    mode: "yaml",
    load: (text) => yaml.load(text, {}),
    validate: async (content) => {
      validateProps(
        {
          items: R.is(Array),
        },
        content
      );
    },
    // onUpdate: R.always(true),
  },
};

export const items = R.keys(types);

const getType = (type) => {
  if (!R.has(type, types)) {
    throw new Error(`invalid sys_config type: ${type}`);
  }
  return types[type];
};

const caches = {};

export const getMode = (type) => getType(type).mode;

const getCurrentVersion = async (type) => {
  const t = getType(type);
  const version = R.path([type, "version"], caches);
  if (R.isNil(version)) {
    const initPath = t.path;
    console.log(`reading ${initPath} ...`);
    const initVal = await fs.readFile(initPath, "utf8");
    const content = t.load(initVal);

    caches[type] = {
      raw: initVal,
      content,
      version: "-system-",
    };
  }
  return version || "-system-";
};

export const get = async (type, getRaw = false) => {
  const t = getType(type);
  const version = await getCurrentVersion(type);
  const {
    rows: [record],
  } = await knex.raw(
    `
      -- expand
      -- type: 'file-sets'
      -- version: '-system-'
      select id, content from sys_config
      where type = :type and (
        :version = '-system-' or id > :version
      )
      order by at desc
      limit 1
    `,
    {type, version}
  );
  if (record) {
    caches[type] = {
      raw: record.content,
      content: t.load(record.content),
      version: record.id,
    };
  }
  return getRaw ? caches[type].raw : caches[type].content;
};

export const set = async (type, by, raw) => {
  const t = getType(type);
  const content = await t.load(raw);
  await t.validate(content);
  await knex("sys_config").insert({type, by, content: raw});
  if (t.onUpdate) {
    await t.onUpdate(content, by);
  }
};

export const update = async (type, by, raw) => {
  const t = getType(type);
  await t.onUpdate(raw, by);
};
