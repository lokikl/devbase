import {compose, map, chain} from "crocks";
import * as R from "ramda";
import knex from "../transport/database";

// example
// const data = [
//   {
//     name: "apple",
//     longtxt: "loki's content",
//     price: 100,
//     weight: 0.5,
//     time: new Date(),
//     config: {abc: 123},
//   },
// ];
// await rebuildMView({
//   name: "aaa",
//   schema: {
//     name: "varchar(255)",
//     longtxt: "text",
//     price: "int",
//     weight: "real",
//     time: "timestamp",
//     config: "json",
//   },
//   data,
// });
// console.log(await knex("aaa").select("*"));

export const rebuild = async ({name, schema, data}) => {
  const serialized = JSON.stringify(data).replaceAll("'", "''");
  const spec = compose(
    R.join(","),
    map(([field, type]) => `${field} ${type}`),
    R.toPairs
  )(schema);
  await knex.raw(`drop materialized view if exists "${name}"`);
  await knex.raw(
    `
      create materialized view "${name}" as
      select * from jsonb_to_recordset('${serialized}'::jsonb) as r(${spec})
    `
  );
};

export const replicateData = (times) => (data) =>
  chain(R.always(data))(R.range(0, times));
