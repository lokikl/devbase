// devbase: ensure=always

import {compose, map} from "crocks";
import * as R from "ramda";
import excel from "node-excel-export";

const DefaultTableOptions = {
  itemsPerPage: 30,
  page: 1,
  extraColumns: [],
};

const formFilteredCTE = (db, options) => {
  const filteredCTE = db.from("_source");

  R.mapObjIndexed((v, k) => {
    const t = R.path(["filterable", k], options);
    if (t === "select") {
      filteredCTE.whereRaw(`${k}::text =any(:v ::text[])`, {
        v: v.split(";"),
      });
    } else if (t === "date_range") {
      const [startDate, endDate] = R.split(" - ")(v);
      if (startDate && endDate) {
        filteredCTE.whereRaw(
          `to_char(${k} at time zone 'hkt', 'YYYY.MM.DD') between :startDate and :endDate`,
          {startDate, endDate}
        );
      }
    } else {
      filteredCTE.whereRaw(`${k}::text ilike ?`, [`%${v}%`]);
    }
  })(options.filters || {});
  return filteredCTE;
};

const formOutputCTE = (db, options, offset) => {
  const outputCTE = db
    .select("*", db.raw("null::json"))
    .from("_filtered")
    .offset(offset)
    .limit(options.itemsPerPage);

  if (options.sort) {
    const [orderBy, order] = options.sort;
    outputCTE.orderBy(orderBy, order);
    if (options.sortIdCol) {
      const [idBy, idOrder] = options.sortIdCol;
      outputCTE.orderBy(idBy, idOrder);
    }
  }
  return outputCTE;
};

const formFilterOptionSQL = compose(
  R.join(","),
  map((pair) => {
    const [name, type] = pair;
    if (type === "select") {
      return `array_agg(distinct ${name}) as ${name}`;
    }
    return `array_agg(distinct to_char(${name} at time zone 'hkt', 'YYYY.MM.DD')) as ${name}`;
  }),
  R.filter((pair) => R.includes(pair[1], ["select", "date_range"])),
  R.toPairs
);

const sqlToTableExt =
  (db) =>
  (sql) =>
  (query, isPlain = false) =>
  (opts) =>
  async (params = {}) => {
    const decodeQuery = isPlain
      ? R.identity
      : R.pipe(
          R.prop(opts.id),
          (base64) =>
            base64 ? Buffer.from(base64, "base64").toString() : "{}",
          (uri) => (uri ? decodeURIComponent(uri) : "{}"),
          (json) => JSON.parse(json)
        );
    const options = R.pipe(
      decodeQuery,
      R.mergeRight(opts),
      R.mergeRight(DefaultTableOptions)
    )(query);
    options.page *= 1;

    const offset = (options.page - 1) * options.itemsPerPage;

    const filterOptionsSQL = formFilterOptionSQL(options.filterable);
    const filteredCTE = formFilteredCTE(db, options);
    const outputCTE = formOutputCTE(db, options, offset);

    // incase there are no select in filterable options
    const fullFilterOptionsSQL = `
      select
        _source.*,
        json_build_object(
          ${filterOptionsSQL ? `'fopts', row_to_json(tFOpts),` : ""}
          'count', tCount.count) as _stats
      from
        _source,
        ${
          filterOptionsSQL
            ? `(
            select ${filterOptionsSQL} from _source
          ) tFOpts,`
            : ""
        }
        (
          select count(1) as count from _filtered
        ) tCount
      limit 1`;

    const combined = db.raw(
      `
      with
        _source as (${sql}),
        _filtered as :filteredCTE,
        _output as :outputCTE,
        _stats as (
          ${fullFilterOptionsSQL}
        )
        select * from _stats
        union all
        select * from _output
    `,
      R.mergeRight(params, {filteredCTE, outputCTE})
    );

    // console.log(combined.toQuery());
    const {rows, fields: sqlFields} = await combined;

    const allFields = R.pipe(
      R.pluck("name"),
      R.reject(R.startsWith("_")),
      R.without(options.withoutColumns || [])
    )(sqlFields);

    if (options.onlyColumns) {
      options.extraColumns = R.without(options.onlyColumns, allFields);
    }

    const fields = options.columns
      ? options.columns
      : R.without(options.extraColumns, allFields);

    if (rows.length > 0) {
      // first row is always for stats, i.e. total rows count after filtered, and filter options
      const [{_stats: stats}] = rows;
      const tableRows = R.tail(rows);

      return {
        rows: tableRows,
        fields,
        options,
        stats: R.mergeRight(stats, {
          allFields,
          pagesCount: Math.ceil((stats.count * 1.0) / opts.itemsPerPage),
          startAt: offset + 1,
          endAt: offset + tableRows.length,
        }),
      };
    }
    // handle no record
    return {rows, fields, options};
  };

const exportXlsx = (sheets) => {
  const content = R.map(
    ({title, name, data, columns, headerStyle, cellStyle}) => {
      const heading = title
        ? [[{value: title, style: {alignment: {horizontal: "center"}}}]]
        : null;
      const merges = title
        ? [
            {
              start: {row: 1, column: 1},
              end: {row: 1, column: R.keys(columns).length},
            },
          ]
        : [];
      return {
        heading,
        name,
        merges,
        specification: R.mapObjIndexed((v) =>
          R.mergeLeft({
            headerStyle: R.mergeRight(headerStyle, v.headerStyle || {}),
            cellStyle: R.mergeRight(cellStyle, v.cellStyle || {}),
          })(v)
        )(columns),
        data,
      };
    },
    sheets
  );

  return excel.buildExport(content);
};

const genTableExtQueryString = compose(
  (encoded) => Buffer.from(encoded).toString("base64"),
  (opt) => encodeURIComponent(JSON.stringify(opt))
);

// see campaign access control page for examples
const parseForcing = (forcing) => {
  const lines = compose(R.split("\n"), R.defaultTo(""))(forcing);
  const filters = compose(
    R.values,
    R.reduce((out, line) => {
      const matches = R.match(/^f(\d+) +([^ ]+) *= *(.+)$/, line);
      if (R.isEmpty(matches)) return out;
      const [, fId, key, value] = matches;
      return R.assoc(
        fId,
        compose(R.assoc(key, value), R.defaultTo({}))(out[fId]),
        out
      );
    }, {}),
    R.filter(R.startsWith("f"))
  )(lines);
  const forceFilter = R.isEmpty(filters) ? [{}] : filters;

  const showCols = compose(
    map(compose(R.trim, R.drop(1))),
    R.filter(R.startsWith("+"))
  )(lines);

  const hideCols = compose(
    map(compose(R.trim, R.drop(1))),
    R.filter(R.startsWith("-"))
  )(lines);

  const highlight = compose(
    map((line) => {
      const matches = R.match(/^hl +([^ ]+) +([^ ]+) *= *(.+)$/, line);
      if (R.isEmpty(matches)) return false;
      const [, color, key, value] = matches;
      return {color, key, value};
    }),
    R.filter(R.startsWith("hl"))
  )(lines);

  const noSettings = R.includes("no settings", lines);

  return {forceFilter, showCols, hideCols, extraOpts: {highlight, noSettings}};
};

export {sqlToTableExt, exportXlsx, genTableExtQueryString, parseForcing};
