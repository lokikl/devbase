// devbase: ensure=always

import "source-map-support/register";

import createError from "http-errors";
import express from "express";
import path from "path";

import logger from "morgan";
import axios from "axios";

import http from "http";
import * as R from "ramda";
import {compose} from "crocks";

import session from "express-session";
import connectMemcached from "connect-memcached";
import flash from "connect-flash";
import csurf from "csurf";

import chalk from "chalk";
import * as appHelpers from "./routes/helpers";
import * as frontendHelpers from "./frontend-helpers";

import indexRouter from "./routes/index";
import onExpressStarted from "./express_started";

const bootAt = new Date();
// ##devbase-anchor express-routes-import

const app = express();
app.enable("trust proxy");
app.disable("x-powered-by"); // skip the cosmetic header

// view engine setup
app.enable("view cache");
app.set("views", path.join(__dirname, "../src/views"));
app.set("view engine", "pug");

// handle request body
app.use(
  express.json({
    verify: (req, res, buf) => {
      req.rawBody = buf;
    },
  })
);
app.use(express.urlencoded({extended: true}));
app.post("*", appHelpers.parseBody);

// session handling
const MemcachedSessionStore = connectMemcached(session);

app.use(
  session({
    name: "<%= project_name %>.sid",
    secret: "<%= project_name %>.cloud-secret-123!#",
    cookie: {
      maxAge: 4 * 60 * 60 * 1000, // 4hr
    },
    store: new MemcachedSessionStore({
      hosts: [process.env.MEMCACHED_ENDPOINT],
    }),
    resave: false, // no need to memcache because memcache has touch
    rolling: true, // auto extend on every response
    saveUninitialized: false, // to save storage and performance
  })
);
app.use(flash());

app.use((req, res, next) => {
  res.locals.appFlash = (level) => req.flash(level);
  next();
});

// serve assets
app.use(
  process.env.ASSETS_CONTEXT,
  express.static(path.join(__dirname, "public")),
  // express.static(path.join(__dirname, "public"), {maxage: 86400000}),
  (req, res) => res.status(404).end()
);

// short circuit wrong favicon.ico call from browser
app.get("/favicon.ico", () => 404);

if (app.get("env") === "development") {
  // app.use(logger("dev"));
  app.use(
    logger((tokens, req, res) => {
      if (R.startsWith("/uid", tokens.url(req, res))) {
        return null;
      }
      const status = tokens.status(req, res);
      const statusFn = status >= 400 ? chalk.red : chalk.dim;
      const length = Math.ceil(tokens.res(req, res, "content-length") / 1024);
      const lengthFn = length > 200 ? chalk.red : chalk.dim;
      const method = tokens.method(req, res);
      const methodFn = method === "GET" ? chalk.dim : chalk.cyan;
      const url = compose(
        R.join(chalk.dim("/")),
        R.split("/")
      )(tokens.url(req, res));
      const resTime = Math.ceil(tokens["response-time"](req, res));
      const resTimeFn = resTime > 1000 ? chalk.red : chalk.dim;
      return R.join(" ", [
        methodFn(method === "GET" ? " GET" : "POST"),
        url,
        statusFn(status),
        lengthFn(`${length}kb`),
        resTimeFn(`${resTime}ms`),
      ]);
    })
  );
} else {
  app.use(
    logger(
      ":method :url :status :response-time ms" +
        " :res[content-length] :date[clf] :remote-addr"
    )
  );
}

// csrf protection
const csrfProtection = csurf();
app.get("*", csrfProtection);

app.get("*", (req, res, next) => {
  res.locals.csrfToken = req.csrfToken();
  next();
});
// app.post("*", appHelpers.parseBody, csrfProtection);

// add url into locals (for making link active)
app.use((req, res, next) => {
  res.locals.pathname = req.baseUrl + req.path;
  // header needed by navy
  res.set("Current-Url", req.originalUrl);
  next();
});

/* eslint-disable fp/no-mutation */
// install frontend helpers
app.locals = R.mergeRight(app.locals, frontendHelpers);
app.locals.assetsContext = process.env.ASSETS_CONTEXT;
app.locals.appVersion = process.env.APP_VERSION;
app.locals.siteName = process.env.SITE_NAME;

if (app.get("env") === "development") {
  const loaded = compose(
    R.without([`/app/dist/dev-boot.js`]),
    R.without([`/app/dist/index.js`]),
    R.filter(R.startsWith("/app/dist/")),
    R.keys
  )(require.cache);
  /* eslint-disable global-require */
  /* eslint-disable fp/no-loops */
  /* eslint-disable no-restricted-syntax */
  /* eslint-disable guard-for-in */
  /* eslint-disable fp/no-delete */
  /* eslint-disable import/no-extraneous-dependencies */
  /* eslint-disable fp/no-mutating-methods */
  const watcher = require("chokidar").watch([loaded]);

  // listen on ready to avoid blasting at the first time
  watcher.on("ready", () => {
    watcher.on("all", (_, changedPath) => {
      console.log(`${changedPath} changed, reloading express...`);
      for (const key in require.cache) {
        if (R.startsWith("/app", key)) {
          delete require.cache[key];
        }
      }
      axios.post("http://127.0.0.1:3001/_replay/replay-last/replay");
    });
  });

  app.use((req, res, next) => {
    require("./routes/index").default(req, res, next);
  });
} else {
  app.use("/", indexRouter);
}
// ##devbase-anchor express-routes-load

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
/* eslint-disable fp/no-mutation */
/* eslint-disable no-unused-vars */
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  if (req.get("async") || req.errorInJson === true) {
    res.json({error: err.message});
  } else {
    // res.send(err.message);
    res.render("error", {
      err,
    });
  }
});

/**
 * Event listener for HTTP server "error" event.
 */
const onError = (error) => {
  if (error.syscall !== "listen") {
    throw error;
  }
  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(`Socket requires elevated privileges`);
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(`Port is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
};

app.set("port", 3000);
const server = http.createServer(app);
server.listen(3000);
server.on("error", onError);

server.on("listening", async () => {
  const addr = server.address();
  console.log(`Server listened ${JSON.stringify(addr)}`);
  await onExpressStarted(app);
  console.log(`Express app booted in ${new Date() - bootAt}ms`);
});
