// devbase: ensure=always

import {compose} from "crocks";

import * as R from "ramda";
import fs from "fs-extra";

/* eslint-disable import/no-extraneous-dependencies */
import chokidar from "chokidar";

const entrance = process.argv[2];

/* eslint-disable import/no-dynamic-require */
require(`./${entrance}`);

const loaded = compose(
  R.without([`/app/dist/dev-boot.js`]),
  R.without([`/app/dist/${entrance}`]),
  R.filter(R.startsWith("/app/dist/")),
  R.keys
)(require.cache);

if (process.env.TOUCH_FILES) {
  const touchFiles = process.env.TOUCH_FILES.split(",");
  /* eslint-disable fp/no-mutating-methods */
  const watcher = chokidar.watch(loaded);

  watcher.on("ready", () => {
    watcher.on("all", (_, changedPath) => {
      console.log(
        `${process.env.LOG} [${entrance}] ${changedPath} changed, reloading...`
      );
      const time = new Date();
      /* eslint-disable fp/no-loops */
      /* eslint-disable no-restricted-syntax */
      /* eslint-disable guard-for-in */
      for (const file of touchFiles) {
        const path = `/app/${file}`;
        if (fs.existsSync(path)) {
          fs.utimesSync(path, time, time);
        } else {
          fs.writeFileSync(path, String(Math.random()));
        }
      }
    });
  });
}
