import "source-map-support/register";

import * as R from "ramda";
import {StringCodec} from "nats";
import socket from "socket.io";
import chalk from "chalk";
import {getEnv} from "./devbase_helpers";
import {connectMQ, subscribeMQ} from "./transport/mq";
import knex from "./transport/database";
import {decrypt} from "./transport/crypto";
import * as N from "./lib/notification";

const sc = StringCodec();

if (!getEnv("NATS_ENDPOINT")) {
  throw new Error("NATS_ENDPOINT is undefined.");
}

const workerLabel = chalk.green("ws");

const colorLevel = (level) =>
  level === "info"
    ? chalk.dim(level)
    : level === "error"
    ? chalk.red(level)
    : level;

const logger = () =>
  process.env.APP_VERSION === "dev"
    ? (msg, level = "info") => {
        console.log(workerLabel, colorLevel(level), msg);
      }
    : (msg) => {
        console.log(msg);
      };

const log = logger();

log("Starting ws on port 4000");

const io = socket(null, {
  path: "/",
  serveClient: false,
}).listen(4000);

connectMQ().then(async () => {
  const sub = subscribeMQ("ws-back", {queue: "master"});
  /* eslint-disable no-restricted-syntax */
  /* eslint-disable fp/no-loops */
  for await (const m of sub) {
    const data = JSON.parse(sc.decode(m.data));
    io.to(data.topic).emit("msg", data);
  }
});

/* eslint-disable no-param-reassign */
io.use(async (s, next) => {
  if (s.handshake.auth) {
    const {token} = s.handshake.auth;
    const [userId, hash] = token.split("/");
    const record = await knex("user_profile")
      .where("id", userId)
      .select("api_key_secret")
      .first();
    if (R.isNil(record)) {
      return next(new Error("not authorized"));
    }
    const sk = R.takeLast(32, record.api_key_secret);
    const expiry = +decrypt(sk, hash);
    const current = +new Date();
    if (current > expiry) {
      return next(new Error("token expired"));
    }
    s.userId = userId;

    return next();
  }
  return next(new Error("not authorized"));
});

io.on("connection", async (s) => {
  log("new back connection");

  s.join(`user:${s.userId}:notifications-count-updated`);
  await N.sendUnreadCount(s.userId);

  s.on("disconnecting", async () => {
    log("back disconnecting");
  });

  s.on("disconnect", () => {
    log("back disconnected");
  });

  // white list for security
  const whitelistedTopics = ["reload:running-jobs"];

  s.on("join-room", async ({name}) => {
    log(`(${s.userId}) join room: ${name}`);
    if (R.includes(name, whitelistedTopics)) {
      s.join(name);
    }
  });

  s.on("unjoin-room", async ({name}) => {
    log(`(${s.userId}) unjoin room: ${name}`);
    s.leave(name);
  });
});
