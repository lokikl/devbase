import {compose, map} from "crocks";
/* jshint esversion: 6 */

/* eslint-disable jquery/no-class, jquery/no-val */
/* eslint-disable jquery/no-attr, jquery/no-prop, jquery/no-css */
/* eslint-disable jquery/no-text, jquery/no-html, jquery/no-ready, jquery/no-submit */

import * as R from "ramda";

import {debounce} from "throttle-debounce";

/* eslint-disable import/no-extraneous-dependencies */
import ace from "ace-builds/src-noconflict/ace";
import {uidPrompt, uid} from "./uid-prompt";

require("ace-builds/src-noconflict/mode-yaml");
//
// require("ace-builds/src-noconflict/mode-json5");
// require("ace-builds/src-noconflict/snippets/json5");
//
// require("ace-builds/src-noconflict/mode-css");
// require("ace-builds/src-noconflict/snippets/css");

// require("ace-builds/src-noconflict/mode-scss");
// require("ace-builds/src-noconflict/snippets/scss");

// require("ace-builds/src-noconflict/worker-css");

// require("ace-builds/src-noconflict/mode-jade");
// require("ace-builds/src-noconflict/snippets/jade");

require("ace-builds/src-noconflict/mode-ini");

require("ace-builds/src-noconflict/mode-text");
require("ace-builds/src-noconflict/snippets/text");

// require("ace-builds/src-noconflict/mode-html");
require("ace-builds/src-noconflict/mode-javascript");
require("ace-builds/src-noconflict/snippets/javascript");

require("ace-builds/src-noconflict/theme-tomorrow");
require("ace-builds/src-noconflict/theme-tomorrow_night");

require("ace-builds/src-noconflict/ext-keybinding_menu");
require("ace-builds/src-noconflict/ext-language_tools");
require("ace-builds/src-noconflict/ext-searchbox");

// ctrl+,
require("ace-builds/src-noconflict/ext-settings_menu");

const initACEEditor = ({
  editorsBackup,
  $node,
  mode = "css",
  callback,
  debounceT = 100,
}) => {
  const editor = ace.edit($node[0]);
  editor.session.setMode(`ace/mode/${mode}`);
  if ($("body").hasClass("dark-mode")) {
    editor.setTheme("ace/theme/tomorrow_night");
  } else {
    editor.setTheme("ace/theme/tomorrow");
  }

  const options = {
    // showPrintMargin: true,
    behavioursEnabled: false,
    tabSize: 2,
    enableBasicAutocompletion: true,
    enableLiveAutocompletion: true,
    enableSnippets: true,
    useWorker: false,
    fontSize: 16,
  };

  editor.setOptions(options);

  editor.commands.addCommand({
    name: "showKeyboardShortcuts",
    bindKey: {win: "Ctrl-Alt-h", mac: "Command-Alt-h"},
    exec: (cmdeditor) => {
      ace.config.loadModule("ace/ext/keybinding_menu", (module) => {
        module.init(cmdeditor);
        cmdeditor.showKeyboardShortcuts();
      });
    },
  });

  editor.commands.addCommand({
    name: "selectMoreAfter2",
    bindKey: {win: "Alt-n", mac: "Alt-n"},
    scrollIntoView: "cursor",
    readOnly: true,
    exec: (cmdeditor) => {
      cmdeditor.execCommand("selectMoreAfter");
    },
  });

  $("body")
    .off("click.ace-action", "[data-ace-action]")
    .on("click.ace-action", "[data-ace-action]", (e) => {
      const action = $(e.currentTarget).data("ace-action");
      if (action === "shortcuts") {
        editor.execCommand("showKeyboardShortcuts");
      }
    });

  if (callback) {
    editor.getSession().on(
      "change",
      debounce(debounceT, false, () => {
        callback(editor.getSession().getValue().replace(/ +\n/, "\n"), editor);
      })
    );
  }

  // context menu for e.g. color picker
  const openContextMenu = (left, top, items) => {
    const $win = $(window);
    const dismissContextMenu = () => {
      $win.off("click.close-context-menu");
      $(".my-context-menu").remove();
    };
    dismissContextMenu();
    const html = compose(
      (str) => `<div class="my-context-menu">${str}</div>`,
      R.join(""),
      map((label) => `<div class="item">${label}</div>`),
      R.pluck("label")
    )(items);

    $(html).css({top, left}).appendTo($("body"));

    $win.on("click.close-context-menu", (e) => {
      const $target = $(e.target);
      if ($target.is(".my-context-menu > .item")) {
        const item = R.find(R.propEq("label", $target.text()), items);
        if (item) {
          if (item.ctrlCallback && e.ctrlKey) {
            item.ctrlCallback($target);
          } else {
            item.callback($target);
          }
        }
      }
      dismissContextMenu();
    });
  };

  editor.container.addEventListener(
    "contextmenu",
    (e) => {
      const selected = editor.getSelectedText();
      const {row, column} = editor.getCursorPosition();
      const line = editor.session.getLine(row);
      if (selected !== "") return true;

      const fnReplaceLine = (newLine) =>
        editor.session.replace(
          new ace.Range(row, 0, row, Number.MAX_VALUE),
          newLine
        );
      /* eslint-disable fp/no-mutating-methods */
      const menuItems = [];

      // color picker
      const regexp =
        /(#[0-9a-fA-F]{8})|(#[0-9a-fA-F]{6})|(#[0-9a-fA-F]{3})|(hsl\(.*\))|(rgb\(.*\))/;
      const match = line.match(regexp);
      if (
        match &&
        column >= match.index &&
        column <= match.index + match[0].length
      ) {
        const lineBefore = R.slice(0, match.index, line);
        const lineAfter = R.slice(
          match.index + match[0].length,
          Infinity,
          line
        );

        menuItems.push({
          label: "Pick a color",
          async callback($btn) {
            await uidPrompt({
              $node: $btn,
              html: await uid("p-edit-one-value", {
                url: "resolve",
                type: "color",
                name: `Color`,
                value: match[0],
                noSubmit: true,
              }),
              direction: "outside,inside",
              dismissOnOutsideClick: "cancel",
              transparentBackdrop: true,
              onReady: async ($prompt) => {
                $("input", $prompt).on("color-changed", (e2, color) => {
                  fnReplaceLine(lineBefore + color + lineAfter);
                });
              },
            });
          },
        });
      }
      // end of color matching
      if (R.isEmpty(menuItems)) return true;
      e.preventDefault();
      openContextMenu(e.pageX, e.pageY, menuItems);
      return false;
    },
    false
  );

  $node.addClass("ace-loaded").data("ace", editor);

  const {cacheAce} = $node.data();
  if (cacheAce && editorsBackup[cacheAce]) {
    const {selection, scrollTop, scrollLeft, undo, redo} =
      editorsBackup[cacheAce];
    editor.session.selection.fromJSON(selection);
    editor.session.setScrollTop(scrollTop);
    editor.session.setScrollLeft(scrollLeft);
    editor.session.$undoManager.$undoStack = undo;
    editor.session.$undoManager.$redoStack = redo;
  }
  return editor;
};

const initAce = () => {
  const $body = $("body");

  $body.on("statechanged", () => {
    $("[data-init-ace]", $body).each((i, node) => {
      const $node = $(node);
      const data = $node.data("init-ace");
      const $input = $(data.bindInput);

      initACEEditor({
        $node,
        mode: data.mode,
        callback: (value) => {
          if ($input.length > 0) {
            $input.val(value);
          }
          $(data.enableBtn).removeClass("disabled");
        },
      });
      $node.removeAttr("data-init-ace");
    });
  });
};

export {initAce};
