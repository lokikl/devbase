// devbase: ensure=always

import * as R from "ramda";

/* eslint-disable fp/no-let */
/* eslint-disable fp/no-loops */
/* eslint-disable no-restricted-syntax */

const interpolate = (literals, expressions) => {
  let string = ``;
  for (const [i, val] of expressions.entries()) {
    string += literals[i] + val;
  }
  string += literals[literals.length - 1];
  return string;
};

const trimPaddingSpaces = (string) => {
  const lines = string.split("\n").slice(1, -1);
  const nSpaces = lines[0].match("^ *")[0].length;
  return R.pipe(
    R.map((line) => line.slice(nSpaces)),
    R.join("\n")
  )(lines);
};

const pugCompile = (literals, ...expressions) => {
  let string = interpolate(literals, expressions);
  string = trimPaddingSpaces(string);
  return pug.compile(string);
};

// overlap SVG lines on typical DOM elements, e.g.
// div(
//   data-node="p1"
//   data-offset=[0, 0]
//   data-lines-to=[["p2", 0]]
// )
// div(
//   data-node="p2"
//   data-offset=[0, 0]
// )

const getNodePos = ($diagram, $node) => {
  const [xo, yo] = $node.data("offset");
  const pos = $node.position();
  return [Math.round(pos.left + xo), Math.round(pos.top + yo)];
};

const formLine = ([p1x, p1y], [p2x, p2y], params = {}) => {
  const halfXOffset = R.defaultTo(0, params.halfXOffset);
  const halfYOffset = R.defaultTo(0, params.halfYOffset);
  const xBackward = R.defaultTo(0, params.xBackward);
  const halfX = Math.round((p1x + p2x) / 2 + halfXOffset);
  const halfY = Math.round((p1y + p2y) / 2 + halfYOffset);
  if (xBackward > 0) {
    return [
      p1x,
      p1y,
      p1x - xBackward,
      p1y,
      p1x - xBackward,
      halfY,
      halfX,
      halfY,
      p2x + xBackward,
      halfY,
      p2x + xBackward,
      p2y,
      p2x,
      p2y,
    ];
  }
  return [p1x, p1y, halfX, p1y, halfX, p2y, p2x, p2y];
};

const refreshDiagram = ($diagram) => {
  const width = Math.round($diagram.outerWidth());
  const height = Math.round($diagram.outerHeight());

  const pos = R.pipe(
    R.map((node) => [$(node).data("node"), getNodePos($diagram, $(node))]),
    R.fromPairs
  )($("[data-node]", $diagram).toArray());

  const lines = R.chain((node) => {
    const {node: self, linesTo} = $(node).data();
    return R.map(
      (target) => formLine(pos[self], pos[target[0]], target[1]),
      linesTo
    );
  })($("[data-lines-to]", $diagram).toArray());

  const html = pugCompile`
    svg.diagram-lines(style="width: ${width}px; height: ${height}px;")
      each line in lines
        polyline(points=line.join(' '))
  `({lines});
  $("svg.diagram-lines", $diagram).remove();
  $(html).prependTo($diagram);
};

export {refreshDiagram};
