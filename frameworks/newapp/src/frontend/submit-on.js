// devbase: ensure=always

/* eslint-disable jquery/no-class, jquery/no-data, jquery/no-val */
/* eslint-disable jquery/no-attr, jquery/no-prop, jquery/no-css */
/* eslint-disable jquery/no-text, jquery/no-html, jquery/no-submit */

import * as R from "ramda";
import {debounce} from "throttle-debounce";

// data-submit-on-change={
//   form: "parent",
//   method: "get",
//   action: "/hello",
//   replacing: ".dependent",
//   stopPropagation: false,
//   injectFormClass: "vf-disabled",
// }
const initSubmitOn = () => {
  const $body = $("body");

  const triggerSubmit = (e, $node, opts) => {
    // compare value change to reduce calls
    const val = $node.val();
    const notCheckbox = !$node.is(`input[type="radio"], [type="checkbox"]`);
    if (notCheckbox && val !== "") {
      if (val === $node.data("last-val")) {
        return false;
      }
      $node.data("last-val", val);
    }
    if (opts.stopPropagation) {
      e.stopPropagation();
    }
    const $form = opts.form === "parent" ? $node.closest("form") : $(opts.form);
    const rollback = {};

    if (opts.injectFormClass) {
      $form.addClass(opts.injectFormClass);
    }

    if (opts.action) {
      rollback.action = $form.prop("action");
      $form.prop("action", opts.action);
    }
    if (opts.method) {
      rollback.method = $form.prop("method");
      $form.prop("method", opts.method);
    }
    if (opts.replacing) {
      rollback.replacing = $form.data("nav-replacing");

      $form
        .attr("data-nav-replacing", opts.replacing)
        .data("nav-replacing", opts.replacing);
    }
    if (opts.skippingInput) {
      const $inputs = $form.find(opts.skippingInput);
      console.log("Disabling inputs ...", $inputs);
      $inputs.prop("disabled", true);
    }
    $form.submit();

    if (opts.action) {
      $form.prop("action", rollback.action);
    }
    if (opts.method) {
      $form.prop("method", rollback.method);
    }
    if (opts.replacing) {
      if (R.isNil(rollback.replacing)) {
        $form.removeAttr("data-nav-replacing").data("nav-replacing", null);
      } else {
        $form
          .attr("data-nav-replacing", rollback.replacing)
          .data("nav-replacing", rollback.replacing);
      }
    }
    if (opts.skippingInput) {
      const $inputs = $form.find(opts.skippingInput);
      console.log("Re-Enabling inputs ...", $inputs);
      $inputs.prop("disabled", false);
    }
    if (opts.injectFormClass) {
      $form.removeClass(opts.injectFormClass);
    }
  };

  $body.on("click", "[data-submit-on-click]", (e) => {
    const $node = $(e.currentTarget);
    triggerSubmit(e, $node, $node.data("submit-on-click"));
  });

  $body.on("change", "[data-submit-on-change]", (e) => {
    const $node = $(e.currentTarget);
    triggerSubmit(e, $node, $node.data("submit-on-change"));
  });

  $body.on(
    "keyup",
    "[data-submit-on-keyup]",
    debounce(200, false, (e) => {
      if (e.which !== 13) {
        const $node = $(e.currentTarget);
        triggerSubmit(e, $node, $node.data("submit-on-keyup"));
      }
    })
  );
};

export {initSubmitOn};

