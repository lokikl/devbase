// devbase: ensure=always

// usage:
// .my-numeral-input
//   input.tright.w100i(
//     type="number" data-vf-required
//   )
//   .overlay <-------- Don't need this line, this will be added by this plugin to show the inputted number with proper comma format

import * as R from "ramda";
import numeral from "numeral";

const formatNumeral = (number) => numeral(number).format("0,0");

const initNumeralInput = () => {
  const $body = $("body");

  $body.on(
    "blur.show-numeral-input-display",
    ".my-numeral-input input",
    (e) => {
      const $input = $(e.currentTarget);
      const formatted = formatNumeral($input.val());
      const $wrapper = $input.parent();
      let $overlay = $(".overlay", $wrapper);
      if ($overlay.length === 0) {
        $overlay = $("<div>").addClass("overlay").appendTo($wrapper);
        if ($input.hasClass("tcenter")) $overlay.addClass("tcenter");
        if ($input.hasClass("tleft")) $overlay.addClass("tleft");
        if ($input.hasClass("tright")) $overlay.addClass("tright");
      }
      $overlay.text(formatted);
    }
  );

  $body.on(
    "focus.hide-numeral-input-display",
    ".my-numeral-input input",
    (e) => {
      const $input = $(e.currentTarget);
      const $wrapper = $input.parent();
      const $overlay = $(".overlay", $wrapper);
      if ($overlay.length > 0) {
        $overlay.remove();
      }
    }
  );

  $body.on("statechanged", () => {
    $(".my-numeral-input input", $body).trigger(
      "blur.show-numeral-input-display"
    );
  });
};

export {initNumeralInput};
