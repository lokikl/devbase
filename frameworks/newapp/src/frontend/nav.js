// devbase: ensure=always

/* eslint-disable jquery/no-class, jquery/no-data, jquery/no-val */
/* eslint-disable jquery/no-attr, jquery/no-prop, jquery/no-css */
/* eslint-disable jquery/no-text, jquery/no-html, jquery/no-wrap */

// intersect internal links and form post
// ajax with fetch() and render page gracefully
// avoided reloading css and js
//
// therefore suggest turn this on for backend, or js-heavy app
// but turn off for public frontend

// to activate, please add "nav-base" class into uid.pug

import * as R from "ramda";
import {uidPrompt, uid, quickInfo} from "./uid-prompt";

/* eslint-disable no-promise-executor-return */
const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const raf = () =>
  new Promise((resolve) => window.requestAnimationFrame(resolve));

const pathnameFromUrl = R.pipe(
  R.split("/"),
  R.slice(3, Infinity),
  R.join("/"),
  R.split("?"),
  R.head,
  (str) => `/${str}`
);

// const defaultAnimation = "fade";
const defaultAnimation = "none";

const determineAnimation = (prev, next) => {
  if (prev === next) {
    return defaultAnimation;
  }
  // if (R.startsWith(prev, next)) {
  //   return "forward";
  // }
  // if (R.startsWith(next, prev)) {
  //   return "backward";
  // }
  const pathOrder = $("body").data("path-order");
  if (pathOrder) {
    const prevIdx = R.indexOf(prev.split("/")[1], pathOrder);
    const nextIdx = R.indexOf(next.split("/")[1], pathOrder);
    if (nextIdx < prevIdx) {
      return "slide-left";
    }
    return "slide-right";
  }
  return defaultAnimation;
};

// get url of a frame / root if no frame found
const getCurrentUrl = ($node) => {
  const $navFrame = $node.closest("[data-nav-frame]");
  if ($navFrame.length > 0) {
    return $navFrame.data("nav-frame-url");
  }
  return window.location.pathname + window.location.search;
};

// get the baseUrl of a frame / root if no frame found
const getBaseUrl = ($node) => {
  const $navFrame = $node.closest("[data-nav-frame]");
  if ($navFrame.length > 0) {
    return $navFrame.data("nav-frame-baseurl");
  }
  return $("[data-base-url]").data("base-url");
};

const getCsrfToken = () => $("[data-csrf-token]").data("csrf-token");

const fetchGet =
  (url, headers = {}) =>
  async () =>
    fetch(url, {
      method: "GET",
      redirect: "follow",
      credentials: "same-origin",
      headers: R.assoc("async", "1", headers),
    });

const fetchPost = (url, data, options = {}) => {
  const formData = new FormData();
  formData.set("_csrf", getCsrfToken());
  R.mapObjIndexed((v, k) => formData.append(k, v))(data);
  const headers = {async: "1"};
  if (options.currentUrl) {
    headers["redirect-to"] = options.currentUrl;
  }
  return async () =>
    fetch(url, {
      method: "post",
      body: formData,
      headers,
    });
};

const navReloadQueue = [];

const appendNavReload = (url) => {
  /* eslint-disable fp/no-mutating-methods */
  navReloadQueue.push(url);
};

/* eslint-disable fp/no-rest-parameters */
/* eslint-disable fp/no-let */
// to avoid recursive calls
const withMutexLock = (fn) => {
  let isLocked = false;
  return async (...args) => {
    if (isLocked) {
      console.log(`fn skipped due to the mutex lock`);
      return true;
    }
    isLocked = true;
    try {
      await fn(...args);
    } finally {
      isLocked = false;
      if (!R.isEmpty(navReloadQueue)) {
        const url = navReloadQueue.pop();
        window.navReload(url);
      }
    }
  };
};

const partialReplace = (replacing, $page, html) => {
  console.log("partial replace");
  const filterTopMosts = R.filter(
    (node) => $(node).parent().closest(replacing).length === 0
  );
  const $current = filterTopMosts($(replacing, $page).toArray());
  const $new = filterTopMosts($(replacing, $(html)).toArray());
  if ($current.length !== $new.length) {
    console.log(`NAV replacing error: count mismatch ${replacing}`);
    return false;
  }
  /* eslint-disable fp/no-loops */
  /* eslint-disable no-restricted-syntax */
  /* eslint-disable guard-for-in */
  for (const [i, current] of $current.entries()) {
    const $newNode = $($new[i]);
    $(current).replaceWith($newNode);
  }
};

const initNav = () => {
  const $body = $("body");

  const updateNavFrame = async (config, data, replacing) => {
    const $frame = $(`[data-nav-frame="${config.frame}"]`);
    $frame.closest(".start-up-hidden").removeClass("start-up-hidden");
    if (R.has("mixin", config)) {
      const args = R.dissoc("nav", data);
      const g = {
        baseUrl: config.baseUrl,
        currentUrl: config.currentUrl,
      };
      const html = await uid(config.mixin, args, g);
      // support replacing
      if (replacing) {
        partialReplace(replacing, $frame, html);
      } else {
        $frame
          .html(html)
          .attr("data-mixin", config.mixin)
          .data("mixin", config.mixin)
          .data("mixin-data", args)
          .data("mixin-globals", g);
      }
      $(window).trigger("scroll.position-uid-prompt");
      $("[autofocus]:visible", $frame).focus();
    }
    /* eslint-disable no-await-in-loop */
    for (const level in config.flashes || {}) {
      const [msg] = config.flashes[level];
      if (level === "reloadOnNavframeClose") {
        if (msg === true) {
          $frame.closest(".up-backdrop").data("reloadOnNavframeClose", msg);
        } else {
          const [frameName, msgurl] = msg.split(";");
          const $f = $(`[data-nav-frame="${frameName}"]`);
          const url = msgurl || true;
          $f.closest(".up-backdrop").data("reloadOnNavframeClose", url);
        }
      } else if (level === "reloadNavframe") {
        const $f = $(`[data-nav-frame="${msg}"]`);
        const url = $f.data("nav-frame-url");
        appendNavReload(url);
      } else if (level === "closeUidPromptByNavframe") {
        await raf();
        const $up = $(`[data-nav-frame="${msg}"]`).closest(".up-backdrop");
        if ($up.length > 0) {
          await $up.data("dismissAndResolve")();
        }
      } else if (level === "setInputValue") {
        const [q, ...values] = msg.split(";");
        const value = values.join(";");
        $(q).val(value);
      } else {
        await raf();
        const $canvas = $frame.closest(".up-canvas");
        if (
          $canvas.length > 0 &&
          !$canvas.closest(".up-backdrop").hasClass("dismissed")
        ) {
          quickInfo(msg, level, level !== "error", $canvas.children().first());
        } else {
          quickInfo(msg, level, level !== "error");
        }
      }
    }
  };

  const fetchAndRender = withMutexLock(
    async ({
      fetchFn,
      fetchUrl,
      pushState,
      delayMS = 0,
      needLoader = false,
      dir = defaultAnimation,
      replacing = false,
      ignoreLeaveGuard = false,
      onError = false,
    }) => {
      if (window.onbeforeunload) {
        if (!ignoreLeaveGuard) {
          const msg = window.onbeforeunload();
          const {action} = await uidPrompt({
            $node: $body,
            html: await uid("p-edit-one-value", {
              type: "yesno",
              name: "Confirm",
              desc: msg,
            }),
            direction: "center",
          });
          if (action === "no") {
            return false;
          }
        }
        window.onbeforeunload = null;
      }
      const fn = !fetchFn && fetchUrl ? fetchGet(fetchUrl) : fetchFn;

      const $base = $(".nav-base", $body);
      if (needLoader) {
        $(`<div class="loader">Loading...</div>`).appendTo($base);
      }
      if (delayMS > 0) {
        await delay(delayMS);
      }
      const resp = await fn();

      // handle both fetch & axios
      if (resp.ok || resp.status === 200) {
        const respUrl = resp.url || resp.headers["current-url"];
        const json = await resp.json();

        // navframe: in server side GET api, we need to return a json
        // with a nav object in order to activate navframe
        if (json.nav) {
          if (json.navRedirect) {
            window.location.href = json.navRedirect;
            return false;
          }
          const config = json.nav;
          await updateNavFrame(config, json, replacing);

          $(`[data-nav-frame="${config.frame}"]`)
            .data("nav-frame-url", respUrl)
            .data("nav-frame-baseurl", config.baseUrl)
            .data("nav-frame-currenturl", config.currentUrl);
          if (needLoader) {
            $(".loader", $base).remove();
          }
          $body.trigger("statechanged");
          return false;
        }

        // NEW FEATURE: full client-side rendering
        const g = {
          baseUrl: json.baseUrl,
          currentUrl: json.currentUrl,
        };
        const html = await uid(json.mixin, json.params, g);

        // dismiss all uid prompts
        $(".up-backdrop").each((i, node) => {
          $(node).data("dismissAndResolve")("cancel-by-statechanged");
        });

        const $page = $(`<div class="nav-base">`)
          .html(html)
          .attr("data-mixin", json.mixin)
          .data("mixin", json.mixin)
          .data("mixin-data", json.params)
          .data("mixin-globals", g);
        $("html,body").scrollTop(0);
        const $next = $page.insertAfter($base).addClass(`nav-load__${dir}`);

        if (dir !== "none") {
          $base.addClass("nav-unload");
          await raf();
          await delay(10);
          $base.addClass(`nav-unload__${dir}`);
          $next.addClass("nav-entering");
          delay(300).then(() => $base.remove());
          $next.removeClass(`nav-load__${dir}`).removeClass("nav-entering");
        } else {
          $base.remove();
        }

        $next
          .attr("data-base-url", json.baseUrl)
          .attr("data-current-url", json.currentUrl);

        const pageTitle = json.title;
        window.document.title = pageTitle;
        if (pushState === true) {
          window.history.pushState({}, pageTitle, respUrl);
        } else if (pushState === false) {
          window.history.replaceState({}, pageTitle, respUrl);
        }

        for (const level in json.flashes || {}) {
          const [msg] = json.flashes[level];
          quickInfo(msg, level, level !== "error");
        }

        $body.trigger("statechanged");
        $("[autofocus]:visible", $body).focus();
      } else {
        if (needLoader) {
          $(".loader", $base).remove();
        }
        $(".nav-masked").removeClass("nav-masked");
        const {status, statusText} = resp;
        const msg = `${status} ${statusText}`;
        try {
          const json = await resp.json();
          if (onError) {
            onError(json);
          } else {
            quickInfo(json.error || msg, "error");
          }
        } catch (err) {
          quickInfo(msg, "error");
        }
      }
    }
  );

  // load nav frame with initial href
  $body.on("statechanged", () => {
    $(".nav-masked").removeClass("nav-masked");

    $(`[data-initial-href]`, $body).each(async (i, node) => {
      const $frame = $(node);
      const href = $frame.data("initial-href");
      $frame.removeAttr("data-initial-href");

      await fetchAndRender({
        fetchUrl: href,
        pushState: false,
        delayMS: 0,
        needLoader: false,
        onError: async (data) => {
          const frame = $frame.data("nav-frame");
          const baseUrl = $frame.data("nav-frame-baseurl");
          const config = {frame, mixin: "p-error", baseUrl};
          await updateNavFrame(config, data);
        },
      });
    });
  });

  window.navFetchAndRender = fetchAndRender;

  window.onpopstate = async () => {
    const $base = $(".nav-base", $body);
    $base.addClass(`nav-unload__${defaultAnimation}`);
    const url = document.location;

    await fetchAndRender({
      fetchFn: fetchGet(url),
      pushState: false,
      dir: defaultAnimation,
    });
  };

  window.navReload = (url) => {
    fetchAndRender({
      fetchUrl: url || document.location.href,
      pushState: false,
      needLoader: true,
    });
  };

  const linkTarget = "a:not(.nav-skip):not([target=_blank])";
  const formTarget = "form:not(.nav-skip)";
  const isInternalUrl = R.startsWith(document.location.origin);

  // const getTargetFrame = ($btn) => {
  //   const frame = $btn.data("frame");
  //   if (frame) {
  //     return frame;
  //   }
  //   const $frame = $btn.closest("[data-nav-frame]");
  //   if ($frame.length > 0) {
  //     return $frame.data("nav-frame");
  //   }
  //   return false;
  // };

  $body.on("click.nav-click", linkTarget, async (e) => {
    const $btn = $(e.currentTarget);
    if ($btn.closest(".nav-skip").length > 0) return true;
    const url = $btn[0].href;
    if (R.isNil(url) || url === "") return true;
    if (!isInternalUrl(url)) return true;
    if ($btn.attr("href")[0] === "#") return true;
    e.preventDefault();

    if (url === window.location.href) {
      return true;
    }
    $btn.addClass("nav-masked");

    await fetchAndRender({
      fetchFn: fetchGet(url),
      pushState: true,
      delayMS: $btn.data("nav-delay"),
      dir: determineAnimation(window.location.pathname, pathnameFromUrl(url)),
      ignoreLeaveGuard: $btn.hasClass("ignore-leave-guard"),
      replacing: $btn.data("nav-replacing"),
    });
  });

  // use navframe in unobtrusive complete CSR
  $body.on("click.update-nav-frame", "[data-update-nav-frame]", async (e) => {
    const $btn = $(e.currentTarget);
    const {frame, mixin, params} = $btn.data("update-nav-frame");
    const baseUrl = getBaseUrl($btn);
    const config = {frame, mixin, baseUrl};
    await updateNavFrame(config, params);
  });

  $body.on("submit.nav-form-submit", formTarget, async (e) => {
    const $form = $(e.currentTarget);
    if ($form.closest(".nav-skip").length > 0) return true;
    e.preventDefault();
    const method = ($form.attr("method") || "").toLowerCase();
    let fetchFn = false;
    if (method === "get") {
      const url = new URL($form.prop("action"));

      R.forEach((input) => {
        url.searchParams.set(input.name, input.value);
      })($form.serializeArray());

      fetchFn = () =>
        fetch(url.toString(), {
          headers: {async: "1"},
        });
    } else if (method === "post") {
      const formData = new FormData($form[0]);
      formData.set("_csrf", getCsrfToken());

      fetchFn = () =>
        fetch($form.attr("action"), {
          method: "post",
          body: formData,
          headers: {
            "redirect-to": getCurrentUrl($form),
            async: "1",
          },
        });
    } else {
      return false;
    }
    if ($form.hasClass("slow-form")) {
      $form.addClass("nav-submitting");
    }

    $form.addClass("nav-masked");

    await fetchAndRender({
      fetchFn,
      pushState: false,
      ignoreLeaveGuard: $form.hasClass("ignore-leave-guard"),
      replacing: $form.data("nav-replacing"),
    });
    $form.removeClass("nav-submitting");
  });

  const mapIndexed = R.addIndex(R.map);
  const addIdx = mapIndexed((item, idx) => [idx, item]);

  const sendFile = async (file, url, onProgress) => {
    const xhr = new XMLHttpRequest();
    return new Promise((resolve) => {
      xhr.upload.addEventListener("progress", (event) => {
        if (event.lengthComputable) {
          onProgress(event.loaded, event.total);
        }
      });

      xhr.addEventListener("loadend", () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
          resolve(JSON.parse(xhr.response));
        } else {
          resolve({err: xhr.statusText});
        }
      });
      xhr.open("POST", url, true);
      xhr.setRequestHeader("Content-Type", "application/octet-stream");
      xhr.send(file);
    });
  };

  // note 1: fetch() doesn't support progress, so XHR based solution required
  // note 2: axios used XHR, but failed in the entity too large error
  const streamOneFile = async ({
    file,
    url,
    chunkSizeKb = Infinity,
    onProgress,
  }) => {
    const chunkSize = chunkSizeKb * 1024;
    if (file.size <= chunkSize) {
      return sendFile(file, url, onProgress);
    }
    const nChunks = Math.ceil(file.size / chunkSize);
    for (const i in R.range(0, nChunks)) {
      const offset = i * chunkSize;
      const slice = file.slice(offset, offset + chunkSize);
      const furl = i === "0" ? url : `${url}&append=1`;

      await sendFile(slice, furl, (loaded) =>
        onProgress(offset + loaded, file.size)
      );
    }
    return {};
  };

  /* eslint-disable no-restricted-syntax */
  /* eslint-disable no-await-in-loop */
  /* eslint-disable fp/no-loops */
  $body.on("click.upload-multiple", `[data-upload-multiple]`, async (e) => {
    const $btn = $(e.currentTarget);
    const {
      accept,
      to: url,
      sizeLimitMB,
      chunkSizeKb,
      progressPrompt,
      autoResolve,
      onlySingleFile = false,
    } = $btn.data("upload-multiple");

    $(".upload-multiple-input", $body).remove();
    const $input = $(
      `<input type="file" multiple="${!onlySingleFile}" accept="${accept}">`
    )
      .addClass("upload-multiple-input")
      .css("transform", "translate(0, -9999px)")
      .appendTo($body);

    $input.on("change", async () => {
      const {files} = $input[0];
      const maxFileSize = sizeLimitMB * 1024 * 1024;
      const fileSizes = R.map((file) => file.size, files);
      const exceededFile = R.findIndex(R.lt(maxFileSize), fileSizes);
      if (exceededFile > -1) {
        $input.remove();

        quickInfo(
          `File (${files[exceededFile].name}) is larger than ${sizeLimitMB}MB`,
          "error"
        );
        return false;
      }

      await uidPrompt({
        $node: $("body"),
        html: await uid(progressPrompt, {}),
        direction: "center",
        onReady: async ($prompt, dismissAndResolve) => {
          $(`[data-resolve="cancel"]`, $prompt).addClass("hidden");
          const totalSize = R.sum(fileSizes);
          const filesCount = files.length;
          $(".files-total", $prompt).text(filesCount);
          const $progress = $(".progress", $prompt).text("0%");
          const $filesCurrent = $(".files-current", $prompt);
          const $file = $(".file", $prompt);
          const $status = $(".status", $prompt);
          for (const [idx, file] of addIdx(files)) {
            const doneSize = R.sum(R.take(idx, fileSizes));
            $filesCurrent.text(idx + 1);
            $file.text(file.name);
            const delim = R.includes("?", url) ? "&" : "?";
            const furl = `${url}${delim}name=${encodeURI(file.name)}`;
            const res = await streamOneFile({
              file,
              url: furl,
              chunkSizeKb,
              onProgress: (loaded, total) => {
                $progress.text(
                  `${(((doneSize + loaded) * 100) / totalSize).toFixed(1)}%`
                );
                const progress = Math.round((loaded * 100) / total);

                $status.text(
                  progress === 100
                    ? "processing ..."
                    : `uploading ... ${progress}%`
                );
              },
            });
            if (res.err) {
              $status.parent().text(`Error: ${res.err.message || res.err}`);
              $(`[data-resolve="cancel"]`, $prompt).removeClass("hidden");
              return false;
            }
          }
          $progress.text("100%");
          $file.text("-");
          $status.text("-");
          $input.remove();
          $(`[data-resolve="cancel"]`, $prompt).removeClass("hidden");
          if (autoResolve) {
            dismissAndResolve(autoResolve);
          }
        },
      });

      await fetchAndRender({
        fetchFn: fetchGet(getCurrentUrl($btn)),
        pushState: false,
      });
    });
    $input.trigger("click");
  });
};

export {initNav, fetchGet, fetchPost, getCurrentUrl, getBaseUrl};
