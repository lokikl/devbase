// devbase: ensure=always

/* eslint-disable jquery/no-class, jquery/no-data, jquery/no-val */
/* eslint-disable jquery/no-attr, jquery/no-prop, jquery/no-css */
/* eslint-disable jquery/no-text, jquery/no-html */

import * as R from "ramda";
import murmur from "murmurhash-js";
import * as F from "../frontend-helpers";
import * as fcsr from "./fcsr";

const hashSeed = String(Math.random());
const getHash = (key) => murmur.murmur3(key, hashSeed);

imagesLoaded.makeJQueryPlugin($);

const delay = (ms) =>
  new Promise((resolve) => {
    setTimeout(resolve, ms);
  });

const raf = () =>
  new Promise((resolve) => {
    window.requestAnimationFrame(resolve);
  });

const untilImagesLoaded = ($node) =>
  new Promise((resolve) => {
    $node.imagesLoaded().always(resolve);
  });

/* eslint-disable fp/no-rest-parameters */
/* eslint-disable no-restricted-syntax */
/* eslint-disable fp/no-let */
/* eslint-disable fp/no-loops */

const quickInfo = async (msg, level, autoDismiss = true, $target = null) => {
  $(".alert-wrap").remove();
  const html = `
    <div class="alert-wrap">
      <div class="alert alert-${level}">
        <span>${msg}</span>
        <button class="close" type="button" data-dismiss="alert">
          <span>&times</span>
        </button>
      </div>
    </div>
  `;
  const $alert = $(html).prependTo($target || $("body"));
  if ($target) {
    $alert.addClass("alert-sticky");
  }
  await delay(10);
  await raf();
  $alert.addClass("alert-displayed");
  if (autoDismiss) {
    await delay(2000);
    $alert.addClass("alert-removed");
    await delay(500);
    $alert.remove();
  }
};

// new positioning, included ",", e.g. "outside,start"
// vertical: outside / inside / top / start / center / end / bottom
// horizontal: outside / inside / left / start / center / end / right
// outside = top / bottom / left / right     (based on position)
// inside = start / end    (based on position)
const positionPrompt = ($form, nodeBox, direction = "outside,inside") => {
  const {width, height} = $form[0].getBoundingClientRect();
  const $window = $(window);
  const scrHeight = $window.height();
  const scrWidth = $window.width();
  if (direction === "center") {
    const top = scrHeight / 2 - height / 2;
    const left = scrWidth / 2 - width / 2;

    $form.css({
      position: "fixed",
      top: Math.max(2, top),
      left: Math.max(2, left),
    });
  } else if (direction === "center-top") {
    const top = 40;
    const left = scrWidth / 2 - width / 2;

    $form.css({
      position: "fixed",
      top: Math.max(2, top),
      left: Math.max(2, left),
    });
  } else {
    let top = nodeBox.top - 26;
    let left = 0;
    if (R.indexOf(",", direction) > -1) {
      const [inV, inH] = direction.split(",");
      const isLowerPart = nodeBox.top + nodeBox.height / 2 > scrHeight / 2;
      const v =
        {
          outside: isLowerPart ? "top" : "bottom",
          inside: isLowerPart ? "end" : "start",
        }[inV] || inV;
      const isRightPart = nodeBox.left + nodeBox.width / 2 > scrWidth / 2;
      const h =
        {
          outside: isRightPart ? "left" : "right",
          inside: isRightPart ? "end" : "start",
        }[inH] || inH;

      top = {
        top: nodeBox.top - height,
        start: nodeBox.top,
        center: nodeBox.top + nodeBox.height / 2 - height / 2,
        end: nodeBox.bottom - height,
        bottom: nodeBox.top + nodeBox.height,
      }[v];

      left = {
        left: nodeBox.left - width,
        start: nodeBox.left,
        center: nodeBox.left + nodeBox.width / 2 - width / 2,
        end: nodeBox.right - width,
        right: nodeBox.left + nodeBox.width,
      }[h];
    } else if (direction === "left") {
      // deprecated: historical cases
      left = nodeBox.left - width - 2;
    } else if (direction === "right") {
      left = nodeBox.left + nodeBox.width + 2;
    } else if (direction === "bottom") {
      left = nodeBox.left;
      top = nodeBox.top + nodeBox.height;
    } else if (direction === "select") {
      left = nodeBox.left - 1;
      top = nodeBox.top - 1;
      $form.css({width: nodeBox.width + 2});
    } else if (direction === "bottom-left") {
      left = nodeBox.left - width + nodeBox.width;
      top = nodeBox.top + nodeBox.height;
    }
    // handle off-screen
    const offset = $form.offsetParent()[0].getBoundingClientRect();
    top -= offset.top;
    left -= offset.left;
    if (top + height > scrHeight) {
      top = scrHeight - height - 2;
    }
    if (left + width > scrWidth) {
      left = scrWidth - width - 2;
    }
    $form.css({top: Math.max(2, top), left: Math.max(2, left)});
  }
};

const uidPrompt = ({
  $wrapper,
  $node,
  html,
  onReady,
  direction = "right",
  alwaysFloat,
  dismissOnOutsideClick = false,
  autoDismissInSecs = false,
  backdropNoFade = false,
  transparentBackdrop = false,
  backdropClasses = "",
}) =>
  new Promise((resolve) => {
    const $body = $wrapper || $("body");
    $body.addClass("uid-prompt-openned");
    // const $body = $node.closest(".app-initialized,body");
    const nodeBox = $node[0].getBoundingClientRect();
    const canvasClasses = [
      "up-canvas",
      alwaysFloat ? "always-float" : "",
      `toward-${direction}`,
    ].join(" ");
    const $backdrop = $(`
      <div class="up-backdrop ${backdropClasses}">
        <div class="${canvasClasses}" data-dir="${direction}">
          ${html}
        </div>
      </div>
    `).appendTo($body);

    if (backdropNoFade || transparentBackdrop) {
      $backdrop.addClass("no-animation");
    }
    if (transparentBackdrop) {
      $backdrop.addClass("transparent");
    }
    const $form = $backdrop.children(".up-canvas");

    const dismissAndResolve = (action) => {
      const reloadOnClose = $backdrop.data("reloadOnNavframeClose");
      $(window).off("scroll.position-uid-prompt");
      $backdrop.off("mousedown.dismiss-uid-prompt-on-outside-click");
      $form.parent().addClass("dismissed");
      if ($("> .up-backdrop:not(.dismissed)", $body).length === 0) {
        $body.removeClass("uid-prompt-openned");
      }
      resolve({$form, action});

      delay(200).then(() => {
        if (reloadOnClose) {
          if (reloadOnClose === true) {
            window.navReload();
          } else {
            window.navReload(reloadOnClose);
          }
        }
        $form.parent().remove();
      });
    };

    $backdrop.data("dismissAndResolve", dismissAndResolve);

    if (dismissOnOutsideClick) {
      $backdrop.on("mouseup.dismiss-uid-prompt-on-outside-click", (e) => {
        if ($("body").hasClass("dragging")) return;
        if ($(e.target).is($backdrop)) {
          dismissAndResolve(dismissOnOutsideClick);
        }
      });
    }

    $form.on("click.uid-prompt-resolve", "[data-resolve]", (e) => {
      const $btn = $(e.currentTarget);
      if ($btn.closest(".up-canvas").is($form)) {
        dismissAndResolve($btn.data("resolve"));
      }
    });

    $form.on("submit.uid-prompt-resolve", "form[data-submit-resolve]", (e) => {
      const $that = $(e.currentTarget);
      if ($that.closest(".up-canvas").is($form)) {
        e.preventDefault();
        e.stopPropagation();
        $that.trigger("vf-validate");
        if (!$that.hasClass("vf-invalid")) {
          dismissAndResolve($(e.currentTarget).data("submit-resolve"));
        }
      }
    });

    untilImagesLoaded($backdrop).then(() => {
      positionPrompt($form, nodeBox, direction);

      $(window).on("scroll.position-uid-prompt", () => {
        untilImagesLoaded($backdrop).then(() => {
          const box =
            direction !== "center" && $node.closest("body").length > 0
              ? $node[0].getBoundingClientRect()
              : nodeBox;
          positionPrompt($form, box, direction);
        });
      });

      window.requestAnimationFrame(async () => {
        $form.parent().addClass(`displayed`);
        $form.removeClass(`toward-${direction}`);
        $("[autofocus]:visible", $form).focus();

        $("[data-press-enter-to-resolve]", $form).each((i, node) => {
          $(node).on("keyup", (e) => {
            if (e.key === "Enter") {
              const action = $(node).data("press-enter-to-resolve");
              dismissAndResolve(action);
            }
          });
        });
        if (onReady) {
          onReady($form, dismissAndResolve);
        }
        $("body").trigger("statechanged");

        $form.on("mousedown.uid-prompt-move", "h1, h2, h3, h4, h5", (e) => {
          if (e.which !== 1) return true;
          const $header = $(e.currentTarget);
          if (!$header.is($("h1, h2, h3, h4, h5", $form).first())) return true;
          const {top, left} = $form.position();
          $form.addClass("moving");
          const onMove = (me) => {
            const dx = me.clientX - e.clientX;
            const dy = me.clientY - e.clientY;
            $(window).off("scroll.position-uid-prompt");

            $form.css({
              top: top + dy,
              left: left + dx,
            });
          };
          $body.on("mousemove.uid-prompt-move", onMove);

          $(document).on("mouseup.uid-prompt-move", () => {
            $body.off("mousemove.uid-prompt-move");
            $(document).off("mouseup.uid-prompt-move");
            $form.removeClass("moving");
          });
          return false;
        });
        if (autoDismissInSecs) {
          setTimeout(() => {
            dismissAndResolve("cancel");
          }, autoDismissInSecs * 1000);
        }
      });
    });
  });

const uid = async (mixin, args, globals = {}) => {
  F.clearLastTokens();
  const assetsContext = $("[data-assets-context]").data("assets-context");
  const app = $("body").data("app");
  if (app.appVersion === "dev") {
    console.log("render mixin", mixin);
    console.log("with args", args);
  }
  const currentLang = $("[data-current-lang]").data("current-lang");
  const currentMode = $("[data-current-mode]").data("current-mode");
  return fcsr.render(mixin, args, {
    asset: (filename) => `${assetsContext}/${filename}`,
    ...app,
    baseUrl: $("[data-base-url]").data("base-url"),
    currentUrl: $("[data-current-url]").data("current-url"),
    isMobile: F.getIsMobile(),
    csrfToken: $("[data-csrf-token]").data("csrf-token"),
    currentLang,
    currentMode,
    ...globals,
  });
};

// replace a dom node with uid result, apply caching to reduce dom manipulation and avoid flickering
const uidReplace = async ($node, mixin, params) => {
  const html = await uid(mixin, params);
  const hash = getHash(html);
  if (hash === $node.data("_uidHash")) return;
  const $newNode = $(html).data("_uidHash", hash);
  $node.replaceWith($newNode);
};

export {uidPrompt, quickInfo, uid, uidReplace};
