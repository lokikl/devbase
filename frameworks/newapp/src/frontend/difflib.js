// devbase: ensure=always

/* eslint-disable import/no-extraneous-dependencies */
import * as difflib from "jsdifflib";

const initDifflib = () => {
  const $body = $("body");

  $body.on("statechanged", () => {
    $("[data-difflib]").each((i, node) => {
      const $node = $(node);
      const {left, right, leftLabel, rightLabel} = $node.data("difflib");

      const leftLines = difflib.stringAsLines(left);
      const rightLines = difflib.stringAsLines(right);

      const sm = new difflib.SequenceMatcher(leftLines, rightLines);
      const opcodes = sm.get_opcodes();

      $node.removeAttr("data-difflib");
      $node.removeData("difflib");

      node.appendChild(
        difflib.buildView({
          baseTextLines: leftLines,
          newTextLines: rightLines,
          opcodes,
          // set the display titles for each resource
          baseTextName: leftLabel,
          newTextName: rightLabel,
          contextSize: null,
          viewType: 1, // 1 for inline, 0 for side-to-side
        })
      );
    });
  });
};

export {initDifflib};
