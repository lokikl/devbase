import * as R from "ramda";
import {debounce} from "throttle-debounce";
import * as fcsr from "./fcsr";

/* eslint-disable jquery/no-text */
/* eslint-disable jquery/no-class */
/* eslint-disable fp/no-let */

const initDevWS = () => {
  const $body = $("body");

  if ($body.data("dev-ws")) return;

  $body.on("click", ".btn-clear-local-storage", () => {
    localStorage.clear();
    window.location.reload();
  });

  // connect ws
  const socket = io(window.location.hostname, {
    path: "/dev-server",
    transports: ["websocket"], // no polling
  });
  $body.data("dev-ws", "connected");

  if ($("[data-mixin-outsync]").length > 0) {
    console.log("Mixins outsync, clearing local storage");
    localStorage.clear();
    $("[data-mixin-outsync]").removeAttr("data-mixin-outsync");
  }

  const delay = (ms) =>
    new Promise((resolve) => {
      setTimeout(resolve, ms);
    });

  /* eslint-disable fp/no-loops */
  /* eslint-disable no-constant-condition */
  /* eslint-disable no-await-in-loop */
  /* eslint-disable no-undef */
  /* eslint-disable no-continue */
  const waitUntilReady = async (url) => {
    while (true) {
      try {
        await axios.get(url);
        break;
      } catch (err) {
        await delay(300);
        continue;
      }
    }
  };

  /* eslint-disable jquery/no-prop */
  /* eslint-disable jquery/no-clone */
  const msgHandlers = {
    refresh: () => {
      console.log("refreshing page");
      window.location.reload();
    },
    "js-changed": async () => {
      console.log("hot reloading app.js");
      const $js = $("script.app-js");
      const href = $js.prop("src").split("?")[0];
      const newSrc = `${href}?v=${Math.random()}`;

      // test until the server is ready
      await waitUntilReady(newSrc);

      $("script").remove();

      if ($body.data("ws")) {
        $body.data("ws").disconnect();
      }
      $body.off();

      $body.addClass("render-all-on-js-loaded");
      const $newjs = $(`<script class="app-js" src="${newSrc}">`);
      $newjs.appendTo("body");
    },
    "scss-changed": () => {
      console.log("hot-reloading app.css");
      const $link = $("link.app-css");
      const href = $link.prop("href").split("?")[0];
      $link.prop("href", `${href}?v=${Math.random()}`);
    },
    "mixin-changed": (msg) => {
      console.log("hot-reloading mixin");
      fcsr.onChange(msg.index, msg.mixins);
    },
  };

  socket.on("connect", async () => {
    console.log("dev ws connected");
  });

  socket.on("disconnect", async () => {
    console.log("dev ws disconnected");
  });

  socket.on("connect_error", (err) => {
    console.log(err.message);
  });

  socket.on("msg", async (msg) => {
    const type = msg.cmd;
    if (R.has(type, msgHandlers)) {
      msgHandlers[type](msg);
    }
  });

  window.initIconPicker = () => {
    const $search = $("input.search");

    /* eslint-disable jquery/no-val */
    $search.keyup(
      debounce(300, false, () => {
        const q = $search.val();
        if (q === "") {
          $(`span.btn4[data-click-to-copy]`).removeClass("hidden");
        } else {
          $(`span.btn4[data-click-to-copy]`).addClass("hidden");
          $(`span.btn4[data-click-to-copy*="${q}"]`).removeClass("hidden");
        }
      })
    );
  };
};

export {initDevWS};
