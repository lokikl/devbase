import * as R from "ramda";
import pugRuntime from "pug-runtime";
import * as FH from "../frontend-helpers";
import {uid} from "./uid-prompt";

const evaluate = (context, expr) => {
  const evaluator = Function.apply(null, [
    ...R.keys(context),
    "expr",
    "return eval('expr = undefined;' + expr)",
  ]);
  return evaluator.apply(null, [...R.values(context), expr]);
};

const ctx = {index: null, html: "", mixinSrc: {}, mixins: {}, interp: null};

const assetsContext = "assets";
const asset = (filename) => `${assetsContext}/${filename}`;

/* eslint-disable fp/no-mutating-assign */
const fn = ({mixin, params, globals}) => {
  ctx.html = "";
  Object.assign(ctx, globals);
  ctx.mixins[mixin](params);
  return ctx.html;
};

const evaluateMixins = (mixins, cacheAfterEval = true) => {
  const evalCode = R.values(mixins).join("\n");
  evaluate({ctx, asset, pug: pugRuntime, ...FH}, evalCode);
  if (cacheAfterEval) {
    ctx.mixinSrc = R.mergeRight(ctx.mixinSrc, mixins);
    localStorage.setItem("mixinSrc", JSON.stringify(ctx.mixinSrc));
  }
};

const fetchMixins = async (names) => {
  const r = await fetch(`/uid/get-mixins/${names.join(",")}`);
  const {mixins} = await r.json();
  evaluateMixins(mixins);
};

export const reRenderAllMixins = async () => {
  const $targets = $("[data-mixin]");
  if ($targets.length > 0) {
    $targets.each(async (i, node) => {
      const $target = $(node);
      const mixin = $target.data("mixin");
      const params = $target.data("mixin-data");
      const globals = $target.data("mixin-globals");
      const html = await uid(mixin, params, globals);
      /* eslint-disable jquery/no-html */
      $target.html(html);
    });
  } else {
    window.navReload();
  }
};

export const onChange = async (index, mixins) => {
  ctx.index = index;
  localStorage.setItem("index", JSON.stringify(ctx.index));
  evaluateMixins(mixins);
  await reRenderAllMixins();
};

export const render = async (mixin, params, globals) => {
  if (R.isNil(ctx.index)) {
    const app = $("body").data("app");
    const indexVer = localStorage.getItem("indexVer");
    if (indexVer !== app.appVersion) {
      // this will only happened in production
      // in development, index will be updated by dev-server through onChange()
      const res = await fetch(`/uid/index`);
      const {index} = await res.json();
      localStorage.setItem("index", JSON.stringify(index));
      localStorage.setItem("indexVer", app.appVersion);
      localStorage.setItem("mixinSrc", "{}");
    } else {
      ctx.mixinSrc = JSON.parse(localStorage.getItem("mixinSrc") || "{}");
      evaluateMixins(ctx.mixinSrc, false);
    }
    ctx.index = JSON.parse(localStorage.getItem("index"));
  }

  const exists = R.keys(ctx.mixins);
  const required = [mixin, ...ctx.index[mixin]];
  const missing = R.without(exists, required);

  if (!R.isEmpty(missing)) {
    await fetchMixins(missing);
  }
  return fn({mixin, params, globals});
};
