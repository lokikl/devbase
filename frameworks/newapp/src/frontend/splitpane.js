// devbase: ensure=always
// import {get, set, del} from './idb-keyval';

/* eslint-disable jquery/no-attr */
/* eslint-disable jquery/no-css */
/* eslint-disable jquery/no-class */

// load stored width/height of all pane
// const restoreLayout = ($body) => {
//   const appCode = $body.data('naf-code')
//   $(`.row-split-handle`, $body).map(async (idx, handle) => {
//     const widthRatio = await get(`layout:row-split:${appCode}:${idx}`)
//     if (widthRatio) { $(handle).prev().attr('pane-width-ratio', widthRatio) }
//   })
//   $(`.column-split-handle`, $body).map(async (idx, handle) => {
//     const heightRatio = await get(`layout:column-split:${appCode}:${idx}`)
//     if (heightRatio) { $(handle).prev().attr('pane-height-ratio', heightRatio) }
//   })
//   setTimeout(applyPaneSize, 100)
// }

// const storeLayoutSize = ($handle, cat, value) => {
//   const $appBody = $handle.parents('[data-naf-code]').first()
//   const appCode = $appBody.data('naf-code')
//   const idx = $(`.${cat}-handle`, $appBody).index($handle)
//   set(`layout:${cat}:${appCode}:${idx}`, value)
// }

// const applyPaneSize = () => {
//   const $body = $("body");
//   const bWidth = $body.innerWidth();
//   const bHeight = $body.innerHeight();
//
//   $("[pane-width-ratio]", $body).each((i, node) => {
//     const $pane = $(node);
//     const pWidth = $pane.attr("pane-width-ratio") * bWidth;
//     $pane.css("width", `${Math.round(pWidth)}px`);
//   });
//
//   $("[pane-height-ratio]", $body).each((i, node) => {
//     const $pane = $(node);
//     const pHeight = $pane.attr("pane-height-ratio") * bHeight;
//     $pane.css("height", `${Math.round(pHeight)}px`);
//   });
// };

const initLayoutDragger = () => {
  const $body = $("body");

  // layout dragger
  // $(window).resize(applyPaneSize)

  // horizontally
  $body.on("mousedown.resize-row-split", ".row-split-handle", (e) => {
    if (e.which !== 1) return true;
    const $me = $(e.currentTarget);
    const oLeft = $me.parent().offset().left;
    const $pane = $me.prev();
    const bWidth = $body.innerWidth();
    const onMove = (me) => {
      const pWidth = Math.max(me.clientX - oLeft, 50);
      const pWidthRatio = pWidth / bWidth;

      $pane
        .css({
          "min-width": `${pWidth}px`,
          "max-width": `${pWidth}px`,
          width: "auto",
        })
        .attr("pane-width-ratio", pWidthRatio);
    };
    $(".split-handle-mask").addClass("active");
    $body.on("mousemove.resize-row-split", onMove);

    $(document).on("mouseup.resize-row-split", () => {
      $body.off("mousemove.resize-row-split");
      $(document).off("mouseup.resize-row-split");
      $(".split-handle-mask").removeClass("active");
      $me.trigger("splitpane-updated");
      // storeLayoutSize($me, 'row-split', $pane.attr('pane-width-ratio'))
    });
    return false;
  });

  // vertically
  $body.on("mousedown.resize-column-split", ".column-split-handle", (e) => {
    if (e.which !== 1) return true;
    const $me = $(e.currentTarget);
    const oTop = $me.parent().offset().top;
    const $pane = $me.prev();
    const bHeight = $body.innerHeight();
    const onMove = (me) => {
      const pHeight = Math.max(me.clientY - oTop, 50);
      const pHeightRatio = pHeight / bHeight;

      $pane
        .css({
          "min-height": `${pHeight}px`,
          "max-height": `${pHeight}px`,
          height: "auto",
        })
        .attr("pane-height-ratio", pHeightRatio);
    };
    $(".split-handle-mask").addClass("active");
    $body.on("mousemove.resize-column-split", onMove);

    $(document).on("mouseup.resize-column-split", () => {
      $body.off("mousemove.resize-column-split");
      $(document).off("mouseup.resize-column-split");
      $(".split-handle-mask").removeClass("active");
      $me.trigger("splitpane-updated");
      // storeLayoutSize($me, 'column-split', $pane.attr('pane-height-ratio'))
    });
    return false;
  });
};

export {initLayoutDragger};
