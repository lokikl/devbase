import * as R from "ramda";
import fetchRetry from "fetch-retry";

/* eslint-disable jquery/no-text */
/* eslint-disable jquery/no-class */
/* eslint-disable fp/no-let */

const rFetch = fetchRetry(fetch);

const initWS = () => {
  const $body = $("body");

  let ws = false;
  let notificationCount = 0;

  const updateNotificationCount = () => {
    const $count = $("[data-notification-count]");
    if ($count.length > 0) {
      $count.text(
        notificationCount === 0
          ? ""
          : notificationCount > 99
          ? "99+"
          : notificationCount
      );
      if (notificationCount > 0) {
        $count.parent().addClass("color-red");
      } else {
        $count.parent().removeClass("color-red");
      }
    }
  };

  /* eslint-disable jquery/no-prop */
  /* eslint-disable jquery/no-clone */
  const msgHandlers = {
    "notifications-count-updated": (msg) => {
      notificationCount = Number(msg.count);
      updateNotificationCount();

      // browser notification when tab is hidden
      if (document.visibilityState !== "visible" && !R.isNil(msg.message)) {
        /* eslint-disable jquery/no-prop */
        const notification = new Notification("New Message", {
          body: msg.message,
          icon: $(`link[rel="icon"]`).prop("href"),
        });

        notification.onclick = () => {
          window.parent.focus();
          notification.close();
        };
      }
    },
  };

  // connect ws
  $body.on("statechanged", () => {
    const $flag = $("[data-connect-ws]", $body);
    if ($flag.length === 0) return false;
    if (ws) return false;

    if (Notification.permission !== "granted") {
      Notification.requestPermission();
    }

    const tokenPath = $flag.data("connect-ws");

    const socket = io(window.location.hostname, {
      path: "/ws",
      transports: ["websocket"], // no polling
      auth: async (cb) => {
        const res = await rFetch(tokenPath, {
          retries: 300,
          retryOn: [502],
          retryDelay: 2000,
          method: "GET",
          redirect: "follow",
          credentials: "same-origin",
        });
        const output = await res.json();
        cb({token: output.token});
      },
    });

    $body.data("ws", socket);

    socket.on("connect", async () => {
      console.log("ws connected");
    });

    socket.on("disconnect", async () => {
      console.log("ws disconnected");
    });

    socket.on("connect_error", (err) => {
      console.log(err.message);
    });

    socket.on("msg", async (msg) => {
      console.log("msg", msg);
      if (R.startsWith("reload:", msg.topic)) {
        const $node = $(`[data-reload-on-ws="${msg.topic}"]`);
        if ($node.length > 0) {
          const $frame = $node.closest("[data-nav-frame]");
          if ($frame.length > 0) {
            const url = $frame.data("nav-frame-url");
            window.navReload(url);
          }
        } else {
          ws.emit("unjoin-room", {name: msg.topic});
        }
      } else {
        const type = R.last(msg.topic.split(":"));
        if (R.has(type, msgHandlers)) {
          msgHandlers[type](msg);
        }
      }
    });
    ws = socket;
  });

  // join ws topic
  $body.on("statechanged", () => {
    $("[data-reload-on-ws]").each((i, node) => {
      const topic = $(node).data("reload-on-ws");
      if (ws) {
        console.log("requesting topic");
        ws.emit("join-room", {name: topic});
      }
    });
  });

  // update the cached notification count
  $body.on("statechanged", () => {
    updateNotificationCount();
  });
};

export {initWS};
