/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable jquery/no-html */
import RFB from "novnc-core";

const initVNCView = () => {
  const $body = $("body");

  $body.on("statechanged", () => {
    $(".vnc-view").each((i, node) => {
      const $view = $(node);

      // Read parameters specified in the URL query string
      // By default, use the host and port of server that served this file
      const host = $view.data("domain");
      const port = "443";
      // const password = readQueryVariable("password");
      const path = "";
      const url = `wss://${host}:${port}/${path}`;

      // Creating a new RFB object will start a new connection
      const rfb = new RFB(node, url, {
        credentials: {password: null},
      });

      // When this function is called we have
      // successfully connected to a server
      const connectedToServer = () => {
        console.log(`Connected`);
        console.log(new Date());
        $(".vnc-view-msg").remove();
      };

      // This function is called when we are disconnected
      const disconnectedFromServer = (e) => {
        if (e.detail.clean) {
          console.log("Disconnected");
          console.log(new Date());
        } else {
          /* eslint-disable jquery/no-text */
          $(".vnc-view-msg").text(
            "Unable to connect, please contact administrator for further support."
          );
        }
      };

      // When this function is called, the server requires
      // credentials to authenticate
      const credentialsAreRequired = () => {
        const password = prompt("Password Required:");
        rfb.sendCredentials({password});
      };

      // When this function is called we have received
      // a desktop name from the server
      const updateDesktopName = (e) => {
        console.log(`updated desktop name: ${e.detail.name}`);
      };

      // Add listeners to important events from the RFB module
      rfb.addEventListener("connect", connectedToServer);
      rfb.addEventListener("disconnect", disconnectedFromServer);
      rfb.addEventListener("credentialsrequired", credentialsAreRequired);
      rfb.addEventListener("desktopname", updateDesktopName);

      // Set parameters that can be changed on an active connection
      rfb.viewOnly = false;
      rfb.scaleViewport = false;
      rfb.resizeSession = false;
    });
  });
};

export {initVNCView};
