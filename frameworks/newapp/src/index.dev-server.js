// devbase: ensure=always

import express from "express";
import socket from "socket.io";

import http from "http";
import axios from "axios";

/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-unused-vars */
/* eslint-disable fp/no-loops */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */

import * as R from "ramda";
import {map, compose} from "crocks";
import {glob as _glob} from "glob";
import fs from "fs-extra";
import crypto from "crypto";
import md5File from "md5-file";
import "colors";
import chokidar from "chokidar";

import {CLIEngine} from "eslint";
import prettier from "prettier";

/* eslint-disable import/no-extraneous-dependencies */
import sass from "node-sass";

import {exec} from "child_process";
import {logInfo, devWS} from "./devbase_helpers";
import {publishMQ} from "./transport/mq";

const glob = (query) =>
  new Promise((resolve) => _glob(query, {}, (er, files) => resolve(files)));

const babel = require("@babel/core");

const execP = (cli, options) =>
  new Promise((resolve, reject) => {
    exec(cli, options, (error, stdout) => {
      if (error) {
        console.log("error.message", error.message);
      }
      return error ? reject(error.message) : resolve(stdout);
    });
  });

const projectName = process.env.DB_NAME;

const log = (msg) => console.log(`* ${msg}`.brightGreen.bold);

const app = express();

// server side js (babel) ==============================================================
// watches file changes
// run eslint (with fixes enabled)
// if no errors, run babel
const eslintCLI = new CLIEngine({
  fix: true,
  cache: true,
  ignorePath: ".gitignore",
  extensions: ["js"],
});
const formatter = eslintCLI.getFormatter();
/* eslint-disable fp/no-let */
/* eslint-disable fp/no-mutation */
let ignoreNextChange = false;

const runBabel = (filename) => {
  const opts = new babel.OptionManager().init({filename});
  if (R.isNil(opts)) return;
  opts.babelrc = false;
  opts.sourceMaps = "inline";
  opts.ast = false;
  const distFile = filename.replace("src", "dist");
  if (!fs.existsSync(distFile)) {
    fs.closeSync(fs.openSync(distFile, "w"));
  }

  babel.transformFile(filename, opts, (err, result) => {
    if (!result && !err) {
      console.error(`No result from Babel for file: ${filename}`);
      return;
    }
    if (err || !result) {
      console.error(`Babel compilation error: ${err}`);
      return;
    }
    const md5sum = crypto.createHash("md5").update(result.code).digest("hex");
    const dist = filename.replace(/^src/, "dist");
    const filemd5 = md5File.sync(dist);
    if (filemd5 === md5sum) return;

    fs.writeFileSync(dist, result.code);
    log("babel done");
  });
};

const lintAndRun = (path) => {
  const st = Number(new Date());
  if (ignoreNextChange && st < ignoreNextChange) {
    ignoreNextChange = false;
    return;
  }
  log(`Linting ${path}`);
  const report = eslintCLI.executeOnFiles([path]);
  CLIEngine.outputFixes(report);
  const results = formatter(report.results);
  const msecs = Number(new Date()) - st;
  if (`${results}` !== "") {
    log(results);
    // yarn eslint src/lib/difflib.js | grep error | grep -v 'problems' | awk '{print $NF}' | sort | uniq
    console.log("To ignore all of them:");
    const rules = compose(
      R.uniq,
      R.pluck("ruleId"),
      R.path(["results", 0, "messages"])
    )(report);
    /* eslint-disable fp/no-loops */
    /* eslint-disable no-restricted-syntax */
    for (const id of rules) {
      console.log(`/* eslint-disable ${id} */`);
    }
  } else {
    log(`Lint Okay - ${msecs}ms`);
  }

  const fixed = R.any(R.has("output"), report.results);
  if (fixed) {
    ignoreNextChange = Number(new Date()) + 1000;
    const cli = `nvr --servername /tmp/nvim/${projectName} -c 'call GracefulReload()'`;
    exec(cli, {});
  }

  const hasError = R.any((res) => res.errorCount > 0)(report.results);
  if (hasError) return;

  if (R.startsWith("src", path)) {
    runBabel(path);
  }
};

// watch files for changes, run linter and restart server
/* eslint-disable fp/no-mutating-methods */
chokidar
  .watch(["src/**/*.js", "test/**/*.js"], {
    ignored: [
      "node_modules",
      /(^|[/\\])\../, // ignore anything starting with .
    ],
  })
  .on("change", (path, _stats) => {
    try {
      lintAndRun(path);
    } catch (err) {
      console.log("failed to format js");
    }
  });
// end of handler for babel js

// mixins (pug) =========================================================================
let prettierOptions = null;

prettier
  .resolveConfig(".prettierrc.yaml")
  .then((options) => (prettierOptions = options));

const onPugChanged = async (path, _stats) => {
  const t = new Date();
  if (ignoreNextChange && t < ignoreNextChange) {
    ignoreNextChange = false;
    return;
  }
  try {
    log(`Linting ${path}`);
    const content = await fs.readFile(path, "utf8");
    const formatted = prettier.format(content, {
      ...prettierOptions,
      parser: "pug",
      plugins: [require.resolve("@prettier/plugin-pug")],
    });
    if (content !== formatted) {
      log(`Updating ${path}`);
      ignoreNextChange = Number(new Date()) + 1000;
      await fs.writeFile(path, formatted);
      const cli = `nvr --servername /tmp/nvim/${projectName} -c 'call GracefulReload()'`;
      exec(cli, {});
    }

    // notify express backend to do invalidation and auto-reloading
    axios(`http://127.0.0.1:3000/_/on-mixin-changed?p=${path}`)
      .then((res) => {
        const d = res.data;
        if (d.index) {
          if (app.io.engine.clientsCount > 0) {
            app.io.emit("msg", {
              cmd: "mixin-changed",
              index: d.index,
              mixins: d.mixins,
            });
          } else {
            fs.writeFileSync(".mixin-outsync", "yes");
          }
        }
      })
      .catch(R.identity);
  } catch (err) {
    console.log(err);
  }
};

const onPugRemoved = async (path, _stats) => {
  // notify express backend to do invalidation and auto-reloading
  axios(`http://127.0.0.1:3000/_/on-mixin-removed?p=${path}`).catch(R.identity);
};

chokidar
  .watch(["src/mixins"])
  .on("change", onPugChanged)
  .on("unlink", onPugRemoved);

// end of handler for mixins (pug)

// translations (json)
const onTranslationChanged = async (path) => {
  const file = "/app/dist/index.js";
  const now = new Date();
  fs.utimesSync(file, now, now);
};

chokidar
  .watch(["config/locales/*.json"])
  .on("change", onTranslationChanged)
  .on("unlink", onTranslationChanged);
// end of handler for translations (json)

// mixin styles (scss) ================================================================
const scssFiles = {};

const indexOneScssFile = async (f) => {
  const filemd5 = md5File.sync(f);
  if (scssFiles[f] && scssFiles[f].md5 === filemd5) {
    console.log(`${f} has no change since last index`);
    return false;
  }
  const c = await fs.readFile(f, "utf8");

  scssFiles[f] = {
    content: c,
    md5: filemd5,
  };
  return true;
};

const compileSCSS = (scss) =>
  new Promise((resolve, reject) => {
    sass.render(
      {
        data: scss,
        outputStyle: "compressed",
      },
      (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(result.css.toString("utf-8"));
        }
      }
    );
  });

export const compileGlobalCSS = async (changedFile) => {
  if (R.isEmpty(scssFiles)) {
    const files = await glob("src/style/*.scss");
    for (const f of files) {
      await indexOneScssFile(f);
    }
  }
  if (changedFile) {
    const isChanged = await indexOneScssFile(changedFile);
    if (!isChanged) {
      return false;
    }
  }

  const indexSCSSPath = "src/style/index.scss";
  const indexSCSSContent = scssFiles[indexSCSSPath].content;

  const css = await compose(
    (scss) => compileSCSS(scss),
    R.join("\n\n"),
    R.prepend(indexSCSSContent),
    R.pluck("content"),
    R.values,
    R.dissoc(indexSCSSPath)
  )(scssFiles);

  const outputPath = "dist/public/app.css";
  await fs.writeFile(outputPath, css);
  return true;
};

const onSCSSChanged = async (path) => {
  try {
    log(`${path} changed`);
    const r = await compileGlobalCSS(path);
    if (r) {
      app.io.emit("msg", {cmd: "scss-changed"});
    }
  } catch (err) {
    console.log(err);
  }
};

/* eslint-disable fp/no-delete */
const onSCSSRemoved = async (path) => {
  log(`${path} deleted`);
  delete scssFiles[path];
  await compileGlobalCSS();
  app.io.emit("msg", {cmd: "scss-changed"});
};

chokidar
  .watch(["src/style"])
  .on("change", onSCSSChanged)
  .on("unlink", onSCSSRemoved);

compileGlobalCSS();
// end of handler for mixin style (scss)

const replayBuffers = {};
let lastReplayId = null;

const replayLastReq = (cmd, printBody) => {
  if (cmd === "refresh") {
    devWS({cmd: "refresh"});
    log("refreshing browser");
    return;
  }
  if (cmd === "forget") {
    lastReplayId = null;
    log("forgot last replay");
    return;
  }
  const q = String(cmd === "replay" ? lastReplayId || "" : cmd);
  const rec = compose(R.find(R.propEq("id", q)), R.values)(replayBuffers);
  if (R.isNil(rec)) {
    return log(cmd === "replay" ? "no recorded replay" : "request not found");
  }
  lastReplayId = rec.id;
  const {method, host, url, headers, body} = rec;
  if (printBody && method === "POST" && body) {
    console.log(`replay body = ${JSON.stringify(body, null, 2)}`.grey);
  }
  if (method === "JOB") {
    return publishMQ("workers", {
      case: rec.label,
      by: headers.by,
      data: body,
    });
  }

  axios({
    method,
    url: `http://127.0.0.1:3000${url}`,
    maxRedirects: 0,
    headers: R.reject(R.isNil, {
      devhost: host,
      host,
      cookie: headers.cookie,
      async: headers.async,
      "x-forwarded-for": headers["x-forwarded-for"],
      "x-forwarded-proto": headers["x-forwarded-proto"],
      "req-at": headers["req-at"],
      "req-api-key": headers["req-api-key"],
      "req-signature": headers["req-signature"],
    }),
    data: body,
  }).catch(R.identity);
};

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post("/report", (req, res) => {
  const rec = req.body;
  const label = `${rec.method}-${rec.label}`;
  const id = R.path([label, "id"], replayBuffers) || String(Math.random());

  replayBuffers[label] = R.mergeRight(
    {
      id,
      at: Number(new Date()),
    },
    rec
  );
  res.json({ok: 1});
});

app.get("/_replay/last-reqs", (req, res) =>
  res.json(
    compose(
      map(R.props(["id", "label", "url", "status"])),
      R.reverse,
      R.sortBy(R.prop("at")),
      R.values
    )(replayBuffers)
  )
);

app.post("/_replay/replay-last/:cmd", (req, res) => {
  replayLastReq(req.params.cmd, true);
  res.json({ok: 1});
});

// recent mixin ===============================================
const mixinBuffer = {};

app.post("/report-mixin", (req, res) => {
  const {name} = req.body;

  mixinBuffer[name] = {
    name,
    at: Number(new Date()),
  };
  res.json({ok: 1});
});

app.get("/_replay/last-mixins", (req, res) =>
  res.json(
    compose(
      R.pluck("name"),
      R.reverse,
      R.sortBy(R.prop("at")),
      R.values
    )(mixinBuffer)
  )
);
// end of recent mixin ===============================================

app.get("/dev-server", (req, res) => {
  app.io.emit("msg", req.query);
  res.json({ok: 1});
});

// catch 404 and error
app.use((_req, res) => {
  res.send("404 not found");
});

app.use((err, _req, res) => {
  console.log(err);
  res.send("500 server error");
});

/**
 * Event listener for HTTP server "error" event.
 */
const onError = (error) => {
  if (error.syscall !== "listen") {
    throw error;
  }
  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(`Socket requires elevated privileges`);
      break;
    case "EADDRINUSE":
      console.error(`Port is already in use`);
      break;
    default:
      throw error;
  }
  process.exit(1);
};

app.set("port", 3001);
const server = http.createServer(app);

app.io = socket(server, {
  path: "/dev-server",
  serveClient: true,
});

app.io.on("connection", () => {
  logInfo("dev ws connected");
});
server.listen(3001);
server.on("error", onError);

server.on("listening", async () => {
  const addr = server.address();
  logInfo(`devbase dev server listened ${JSON.stringify(addr)}`);
});
