const path = require("path");
const axios = require("axios");
const EventHooksPlugin = require('event-hooks-webpack-plugin');

module.exports = [
  // for uid to generate preview
  // after imported as js script, uid will load all functions window.Helpers.*
  {
    entry: {
      "helpers": ["./src/frontend-helpers.js"]
    },
    output: {
      path: path.resolve(__dirname, "dist/public"),
      filename: 'helpers.js',
      library: 'Helpers',
      libraryTarget: 'window'
    },
    stats: 'minimal',
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "babel-loader"
        }
      ],
    },
  },
  {
    entry: {
      app: ["./src/frontend.js"],
    },
    output: {
      path: path.resolve(__dirname, "dist/public"),
      filename: '[name].js'
    },
    stats: 'minimal',
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "babel-loader"
        },
        {
          test: /\.ya?ml$/,
          type: 'json',
          use: 'yaml-loader'
        }
      ]
    },
    plugins: [
      // shouldEmit, done, additionalPass, beforeRun, run, emit, assetEmitted, afterEmit,
      // thisCompilation, compilation, normalModuleFactory, contextModuleFactory, beforeCompile,
      // compile, make, afterCompile, watchRun, failed, invalid, watchClose, infrastructureLog,
      // environment, afterEnvironment, afterPlugins, afterResolvers, entryOption, infrastructurelog
      new EventHooksPlugin({
        afterEmit: (compilation) => {
          if (process.env.NODE_ENV === 'production') return;
          try {
            let changed = false;
            compilation.chunks.forEach((chunk) => {
              if (chunk.rendered) {
                changed = true;
              }
            });
            if (compilation.errors && compilation.errors.length > 0) {
              changed = false;
            }
            if (changed) {
              axios.get("http://127.0.0.1:3001/dev-server?cmd=js-changed")
                .catch(() => {
                  console.log("failed to auto replay, maybe dev-server is not started");
                });
            }
          } catch (err) {
            console.log(err);
          }
        },
      }),
    ]
  }
];

