#!/usr/bin/env ruby
# devbase: ensure=always
# devbase: executable=yes

require 'active_support/inflector'
require 'fileutils'

name = ARGV[0] # e.g. darphinRegister
project = ARGV[1] || "frontend" # e.g. darphin

def update_frontend_js(name, project)
  file = "src/#{project}.js"
  puts "Updating #{file} ..."
  lines = File.read(file).lines

  ##devbase-anchor naf-import
  lno = lines.index { |l| l.include?('##devbase-anchor naf-import') }
  lines.insert(lno, %Q[import { init as init#{name.camelize} } from './#{project}/app/#{name.underscore}';\n])

  ##devbase-anchor naf
  lno = lines.index { |l| l.include?('##devbase-anchor naf-load') } + 1
  lines.insert(lno, %Q[  #{name}: init#{name.camelize},\n])

  File.write(file, lines.join(''))
end

def add_app_js_file(name, project)
  FileUtils.mkdir_p("src/#{project}/app")
  controller_file_path = "src/#{project}/app/#{name.underscore}.js"
  puts "Creating #{controller_file_path} ..."
  File.write(controller_file_path, <<-CODE
  /* jshint esversion: 6 */

  import * as R from "ramda";

  import {nafOn} from "../navy-app-framework";

  import {uidPrompt, uid} from "../uid-prompt";

  const init = async $body => {
    const onClick = nafOn($body, 'click');

    const popupData = R.defaultTo(
      {},
      $("[data-popup-data]", $body).data("popup-data")
    );

    onClick("open-popup", async (templateName, $node) => {
      const { action, $form } = await uidPrompt({
        $node,
        html: uid(
          templateName,
          R.merge(R.defaultTo({}, $node.data("data")), popupData),
        ),
        direction: $node.data("dir") || "left",
        dismissOnOutsideClick: "cancel"
      });
      if (action === "cancel") return true;
    });
  };

  export { init };
  CODE
    .gsub(/^  /, '')
  )
end

if name
  update_frontend_js(name, project)
  add_app_js_file(name, project)
else
  puts "Usage:"
  puts
  puts "new-naf-module [module_name] [project_name]"
  puts
  puts "e.g. new-naf-module productList catalog-default"
  puts "e.g. new-naf-module viewProduct       # frontend will be used as project_name by default"
end
