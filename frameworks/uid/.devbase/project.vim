" devbase: ensure=always
set backupcopy=yes

nnoremap sb :exec 'CtrlP .devbase'<cr>
nnoremap sc :exec 'CtrlP src/assets/scss'<cr>

" You need to add below plugin in your vimrc
" Plugin 'lokikl/vim-ctrlp-anything'
nnoremap sv :CtrlPanythingLocate frontend-events-to-ctrlp<cr>
nnoremap sj :CtrlPanythingLocate frontend-files<cr>

nnoremap <leader>r :call TmuxSplitExec("reboot-dev", "-d")<cr>
nnoremap <leader>o :call TmuxSplitExec("open-service-in-chrome uid 3000", "-d")<cr>

nnoremap <leader>l :call system("console-ctl toggle-logs")<cr>
nnoremap <leader>d :call system("console-ctl toggle-db")<cr>

nnoremap co :call TmuxSplitExec("console")<cr>

function! TmuxSplitExec(subcmd, ...)
  let tmuxArg = get(a:, 1, "")
  let cmd = "source .envrc; echo " . a:subcmd . "; " . a:subcmd
  let cmd = "tmux splitw " . tmuxArg . " \"zsh -c '" . cmd . "'\""
  call system(cmd)
endfunction

nnoremap st :call GitStatus()<cr>
function! GitStatus()
  let cmd = 'git status'
  let cmd = "tmux splitw \"zsh -c '" . cmd . "; read'\""
  call system(cmd)
endfunction
