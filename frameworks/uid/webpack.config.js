const path = require("path");
const MergeIntoSingleFilePlugin = require("webpack-merge-and-include-globally");

module.exports = [
  // for uid to generate preview
  // after imported as js script, uid will load all functions window.Helpers.*
  {
    entry: {
      "helpers": ["./src/frontend-helpers.js"]
    },
    output: {
      path: path.resolve(__dirname, "dist/public"),
      filename: 'helpers.js',
      library: 'Helpers',
      libraryTarget: 'window'
    },
    module: {
      rules: [{ test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }]
    },
  },
  {
    entry: {
      app: ["./src/frontend.js"]
    },
    output: {
      path: path.resolve(__dirname, "dist/public"),
      filename: '[name].js'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "babel-loader"
        },
        {
          test: /\.pug/,
          exclude: /node_modules/,
          loader: "pug-loader"
        }
      ]
    },
    plugins: [
      new MergeIntoSingleFilePlugin({
        files: {
          "vendor.js": [
            "node_modules/jquery/dist/jquery.js",
            "vendor/hide-show.js"
          ],
          "vendor.css": [
            "node_modules/normalize.css/normalize.css"
          ]
        }
      })
    ]
  }
];
