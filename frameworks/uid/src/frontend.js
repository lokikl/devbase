import * as R from "ramda";
import { initNavyAppFramework } from "./frontend/navy-app-framework";
import { initLayoutDragger } from "./frontend/splitpane";
import { initVF } from "./frontend/vf";
// import { initNav } from "./frontend/nav";
import { initTableExt } from "./frontend/table-ext";

import { uidPrompt, uid } from "./frontend/uid-prompt";

// ##devbase-anchor naf-import

const appInitializers = {
  // ##devbase-anchor naf-load
};

const $body = $("body");

if ($body.data("app-version") === "dev") {
  window.onerror = msg => {
    $(`<div>Uncaught Error: ${msg}</div>`)
      .css({
        "text-align": "center",
        "background-color": "red",
        color: "white",
        "font-size": "2rem"
      })
      .prependTo($body);
  };
}

const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

$(() => {
  initNavyAppFramework(appInitializers);
  initLayoutDragger();
  initVF();
  // suggest turn this on for backend, or js-heavy app
  // but turn off for public frontend
  // see nav.js for details
  // initNav();
  initTableExt();

  $body.on("statechanged", () => {
    $(".alert-dismissible", $body).each(async (i, node) => {
      const $alert = $(node);
      $alert.addClass("alert-displayed");
      await delay(3000);
      if ($alert.hasClass("alert-primary")) {
        // auto-dismiss only for primary alerts
        $alert.addClass("alert-removed");
        await delay(500);
        $alert.remove();
      }
    });

    // tippy('[title]', {
    //   allowTitleHTML: true,
    //   animateFill: true,
    //   animation: 'shift-away', // 'shift-toward', 'fade', 'scale', 'perspective'
    //   arrow: false,
    //   arrowType: 'round', // 'round'
    //   delay: [300, 0], // Number or Array [show, hide] e.g. [100, 500]
    //   distance: 10,
    //   duration: [350, 300], // Number or Array [show, hide]
    //   placement: 'top', // 'bottom', 'left', 'right', 'top-start', 'top-end', etc.
    //   size: 'regular', // 'small', 'large'
    //   trigger: 'mouseenter focus', // 'click', 'manual'
    // })

    $("[data-auto-trigger]:not(.auto-triggered)").each(function() {
      const data = $(this).data();
      let interval = 1;
      if (data.autoTriggerDelay) {
        interval = data.autoTriggerDelay * 1000;
      }
      if (data.autoTrigger === "select") {
        data.autoTrigger = "change";
      } else if (data.autoTrigger === "focusout") {
        data.autoTrigger = "focusout";
      } else if (data.autoTrigger === "click-and-dismiss") {
        data.autoTrigger = "click-and-dismiss";
      } else {
        data.autoTrigger = "click";
      }
      const $obj = $(this);
      setTimeout(() => {
        if (data.autoTriggerVisibleOnly && !$obj.is(":visible")) {
          return false;
        }
        if (data.autoTrigger === "click-and-dismiss") {
          $obj[0].click();
          $(".general-modal").modal("hide");
        } else if (data.autoTrigger === "click") {
          $obj[0].click();
        } else {
          $obj.trigger(data.autoTrigger);
        }
        $obj.addClass("auto-triggered");
        return true;
      }, interval);
    });

    // setTimeout(function() {
    //   $('.init-chosen').each(function() {
    //     var $this = $(this);
    //     $this.chosen({
    //       disable_search_threshold: 10,
    //       search_contains: true,
    //       width: '100%'
    //     })
    //     $this.removeClass('init-chosen')
    //   });
    // }, 1)

    // $('.jalert-error').each(function() {
    //   var $this = $(this);
    //   var msg = $this.html()
    //   $.alert(msg)
    //   $this.remove()
    // });

    // $('[data-sortable]').each(function() {
    //   var $this = $(this)
    //   $this.sortable({
    //     distance: 5,
    //     // containment: $this.parent(),
    //     axis: "y",
    //     update: function(event, ui) {
    //       if ($this.data('sortable-submit') === 'parent') {
    //         $this.parent().submit()
    //       }
    //     },
    //   })
    //   $this.disableSelection()
    //   $this.removeAttr('data-sortable')
    // });

    // setTimeout(function() {
    //   $('[data-wait-for-jid-and-click]').each(function() {
    //     var $this = $(this);
    //     var jid = $this.data('wait-for-jid-and-click')
    //     console.log(jid);
    //     var subscription = window.stream.subscribe(`/${jid}`, function(message) {
    //       if (isValidJSONString(message)) {
    //         const percentage = JSON.parse(message).percentage
    //         $('.job-progress').attr('aria-valuenow', percentage).css('width', `${percentage}%`).text(`${percentage}%`)
    //         if (parseFloat(percentage) !== 100) {
    //           return
    //         }
    //       }
    //       $this.click()
    //     });
    //     $this.removeAttr('data-wait-for-jid-and-click')
    //   });
    // }, 1)

    // tablesorter
    // $('.tablesorter-childRow td').hide();
    // $('.tablesorter').each(function() {
    //   var $this = $(this);
    //   var options = $.extend({}, $this.data('tablesorter'), {
    //     theme: 'bootstrap',
    //     widgets: ['staticRow'],
    //     cssChildRow: "tablesorter-childRow"
    //   });
    //   $this.tablesorter(options);
    //
    //   $this.delegate('.toggle', 'click' ,function(){
    //     $(this).closest('tr').nextUntil('tr:not(.tablesorter-childRow)').find('td').toggle();
    //   });
    // });
  });

  function isValidJSONString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  $body.on("click.toggle-password", "[data-toggle-password]", e => {
    const $node = $(e.currentTarget);
    const $pass = $($node.data("toggle-password"));
    const currentType = $pass.prop("type");
    if (currentType === "password") {
      $node.text("Hide password");
      $pass.prop("type", "text");
    } else {
      $node.text("Show password");
      $pass.prop("type", "password");
    }
  });

  $body.on("click.dismiss-alert", "[data-dismiss='alert']", e => {
    const $node = $(e.currentTarget);
    $node.parent().remove();
  });

  $body.on("click.delete-dom", "[data-delete-dom]", function() {
    const $this = $(this);
    $($this.data("delete-dom")).remove();
  });

  $body.on("click.confirm", "[data-confirm]", async e => {
    const $node = $(e.currentTarget);
    if (!$node.hasClass("confirmed")) {
      e.stopImmediatePropagation();
      e.stopPropagation();
      e.preventDefault();
      $node.addClass("hidden-mobile");
      const question = $node.data("confirm");
      const {action} = await uidPrompt({
        $node,
        html: uid("p-confirm", {question}),
        direction: "center",
      });
      $node.removeClass("hidden-mobile");
      if (action === "confirm") {
        $node.addClass("confirmed");
        $node[0].click();
      }
    }
    $node.removeClass("confirmed");
    return true;
  });

  function triggerSubmitCallback(e) {
    const $this = $(this);
    if ($this.hasClass("stop-propagation")) {
      e.stopPropagation();
    }

    if ($this.data("prompt")) {
      const r = prompt($this.data("prompt"));
      if (r) {
        const $target = $($this.data("prompt-update"));
        $target.val(r);
      } else {
        return false;
      }
    }

    const query = $this.data("trigger-submit");
    const $q = query === "parent" ? $this.parents("form") : $(query);
    if ($this.data("trigger-submit-action")) {
      $q.prop("action", $this.data("trigger-submit-action"));
    }
    if ($this.data("trigger-submit-method")) {
      $q.prop("method", $this.data("trigger-submit-method"));
    }
    if ($this.data("disable-validation")) {
      $q.parsley().destroy();
    }

    const disabledHiddens = [];
    if ($this.data("trigger-submit-disable-hidden")) {
      $("input:not([type=hidden]),select,textarea", $q).each(function() {
        const $input = $(this);
        if (!$input.is(":visible") && !$input.prop("disabled")) {
          $input.prop("disabled", true);
          /* eslint-disable fp/no-mutating-methods */
          disabledHiddens.push($input);
        }
      });
    }

    setTimeout(() => {
      const $form = $q.submit();
      const originalValue = $this.data("original-value");
      const parsley = $form.data("Parsley");
      if (originalValue && parsley && parsley.isValid() === false) {
        $this.val(originalValue);
      }
      disabledHiddens.forEach($input => {
        $input.prop("disabled", false);
      });
    }, 1);
    return true;
  }
  $body.on(
    "click.trigger-submit",
    "[data-trigger-submit]:not(select,input)",
    triggerSubmitCallback
  );
  $body.on(
    "change",
    "input[data-trigger-submit],select[data-trigger-submit]",
    triggerSubmitCallback
  );

  $body.trigger("statechanged");
});
