// devbase: ensure=always

import * as R from "ramda";

const raf = () => new Promise(resolve => window.requestAnimationFrame(resolve));

const initVF = () => {
  const $body = $("body");

  const renderError = ($input, title, desc) => {
    $(`<div class="vf-hint"></div>`)
      .text(desc)
      .insertAfter($input);
    $input.addClass("vf-error");
  };

  const vfCheckers = {
    required: $input => {
      if ($input.val().length < 1) {
        renderError($input, "Required Field", "This field can not be blank.");
        return false;
      }
      return true;
    },
    password: ($input, minLen) => {
      const len = $input.val().length;
      if (len > 0 && len < (minLen || 10)) {
        renderError(
          $input,
          "Insecure Password",
          `Please choose a password with more than ${minLen || 10} characters.`
        );
        return false;
      }
      return true;
    },
    pattern: ($input, pattern) => {
      const v = $input.val();
      if (v.length > 1 && R.isNil(v.match(pattern))) {
        renderError($input, "Invalid Format", "Format is not valid.");
        return false;
      }
      return true;
    }
  };

  const clearErrors = $form => {
    $(".vf-error", $form).removeClass("vf-error");
    $(".vf-hint", $form).remove();
  };

  const validateForm = $form => {
    let allValid = true;
    clearErrors($form);
    let errInputs = [];
    R.forEachObjIndexed((fn, key) => {
      $(`[data-vf-${key}]:not([disabled])`, $form).each((i, input) => {
        if (
          !R.includes(input, errInputs) &&
          fn($(input), $(input).data(`vf-${key}`)) === false
        ) {
          errInputs = R.concat([input], errInputs);
          allValid = false;
        }
      });
    })(vfCheckers);
    raf().then(() => {
      $(".vf-hint", $form).addClass("displayed");
    });
    return allValid;
  };

  $body.on("change.vf-revalidate", ".vf-error", e => {
    const $form = $(e.currentTarget).closest("form");
    validateForm($form);
  });

  $body.on("submit.vf-validation", "form", e => {
    const $form = $(e.currentTarget);
    if (validateForm($form)) {
      return true;
    }
    e.preventDefault();
    e.stopImmediatePropagation();
    e.stopPropagation();
    return false;
  });

  $body.on("click.vf-hint-clicked", ".vf-hint", e => {
    $(e.currentTarget).removeClass("displayed");
  });
};

export { initVF };
