// devbase: ensure=always
import * as R from "ramda";

const initTableExt = () => {
  const $body = $("body");

  const encodeOptions = R.pipe(
    R.dissoc("id"),
    R.dissoc("itemsPerPage"),
    R.dissoc("sortable"),
    R.dissoc("filterable"),
    opt => btoa(JSON.stringify(opt))
  );

  $body.on("statechanged", () => {
    const $exportBtn = $(".table-ext a.export-xlsx-btn", $body);
    if ($exportBtn.length > 0) {
      const $wrap = $exportBtn.closest(".table-ext");
      const options = $wrap.data("options");
      const encoded = encodeOptions(options);
      const href = $exportBtn.prop("href");
      const newHref = `${href}?${options.id}=${encoded}`;
      $exportBtn.prop("href", newHref).removeClass("hidden");
    }
  });

  $body.on("click.import-xlsx", `[data-import-xlsx]`, async e => {
    const $btn = $(e.currentTarget);
    const href = $btn.data("import-xlsx");

    $(".upload-xlsx", $body).remove();
    const $input = $('<input type="file" accept=".xlsx">')
      .addClass("upload-xlsx")
      .css("transform", "translate(0, -9999px)")
      .appendTo($body);

    $input.on("change", async () => {
      const {
        files: [file]
      } = $input[0];
      if (file.size > 1 * 1024 * 1024) {
        alert("File too large (1MB)");
        return false;
      }
      /* eslint-disable no-undef */
      const formData = new FormData();
      formData.append("xlsx", file);
      const r = await fetch(href, {
        method: "POST",
        body: formData
      });
      const rJson = await r.json();
      // console.log("rJson" + ": " + JSON.stringify(rJson));
      // @TODO loki, 12/05/2020: popup dialog to show changes and confirm with users
      window.location.reload();
      $input.remove();
    });
    $input.trigger("click");
  });

  const getCurrentQuery = () => {
    const paramsArray = window.location.search.substr(1).split("&");
    const params = {};
    /* eslint-disable no-restricted-syntax */
    for (const pa of paramsArray) {
      const param = pa.split("=", 2);
      if (param.length === 2) {
        params[param[0]] = decodeURIComponent(param[1].replace(/\+/g, " "));
      }
    }
    return params;
  };

  const updateTable = async options => {
    const encoded = encodeOptions(options);
    const query = getCurrentQuery();
    query[options.id] = encoded;
    await window.navFetchAndRender({
      fetchUrl: `?${$.param(query)}`,
      pushState: false,
      delayMS: 0,
      needLoader: true
    });
  };

  $body.on("click.table-ext-switch-page", "[data-te-page]", e => {
    const $btn = $(e.currentTarget);
    const $wrap = $btn.closest(".table-ext");
    const options = $wrap.data("options");
    if ($btn.is("[disabled]")) return true;
    options.page = $btn.data("te-page");
    updateTable(options);
  });

  $body.on("click.table-ext-sort", "[data-te-sort]", e => {
    const $btn = $(e.currentTarget);
    const $wrap = $btn.closest(".table-ext");
    const options = $wrap.data("options");
    const key = $btn.data("te-sort");
    if (options.sort[0] === key) {
      options.sort[1] = options.sort[1] === "asc" ? "desc" : "asc";
    } else {
      options.sort = [key, "asc"];
    }
    updateTable(R.dissoc("page", options));
  });

  $body.on("change.table-ext-apply-filter", "[data-te-filter]", e => {
    const $input = $(e.currentTarget);
    const $wrap = $input.closest(".table-ext");
    const options = $wrap.data("options");
    const key = $input.data("te-filter");
    options.filters = R.defaultTo({}, options.filters);
    const val = R.trim($input.val());
    if (val === "") {
      options.filters = R.dissoc(key, options.filters);
    } else {
      options.filters = R.assoc(key, val, options.filters);
    }
    updateTable(R.dissoc("page", options));
  });
};

export { initTableExt };
