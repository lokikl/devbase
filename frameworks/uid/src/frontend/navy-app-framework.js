// devbase: ensure=always
/* jshint esversion: 6 */
import * as R from "ramda";

const newID = () =>
  [
    Math.random().toString(36),
    Math.random().toString(36),
    Math.random().toString(36),
  ]
    .join("")
    .replace(/[^a-z]+/g, "")
    .substr(2, 10);

// registry event on statechanged
const initNavyAppFramework = apps => {
  const $body = $("body");

  $body.on("statechanged", () => {
    setTimeout(() => {
      if ($("[data-init-app]").length > 1) {
        alert("More than one app invoking, aborted");
      } else {
        $("[data-init-app]").each(function eachNAFApp() {
          const $this = $(this);
          /* eslint-disable fp/no-mutation */
          /* eslint-disable fp/no-mutating-methods */
          window.appStates = window.appStates || [];
          const appCode = $this.data("init-app");
          const $appBody = $this.parent();
          $appBody.attr("data-naf-code", appCode);
          window.appStates.push({
            code: appCode,
            body: $appBody,
            nafId: $this.data("naf-id"),
          });
          if (apps[appCode]) {
            apps[appCode]($appBody);
          } else {
            console.log(`No handler defined for NAF app: ${appCode}`);
          }
          $this
            .removeAttr("data-init-app")
            .addClass("app-initialized")
            .on("naf:modalclose", e => {
              const [prevAppState, appState] = window.appStates.slice(-2);
              prevAppState.body.trigger(
                `close_${appState.code}.${appState.nafId}`,
                {}
              );
              window.appStates.pop();
            });
          // restoreLayout($appBody) // layout-dragger
        });
      }
    }, 100);
  });
};

const appReturn = output => {
  const [prevAppState, appState] = window.appStates.slice(-2);
  const outputWithState = R.mergeRight(output, appState);
  appState.body
    .parents(".jconfirm-box")
    .children(".jconfirm-closeIcon")
    .click();
  if (appState.nafId) {
    prevAppState.body.trigger(
      `callback_${outputWithState.code}.${appState.nafId}`,
      outputWithState
    );
  } else {
    console.log(
      "NAF Warning: please use openNAFApp instead of backstageRequest"
    );
    prevAppState.body.trigger(
      `callback_${outputWithState.code}`,
      outputWithState
    );
  }
};

const nafOnClick = ($body, event = "click", newFormat = false) => (
  dataTag,
  callback
) => {
  console.log("nafOnClick is depreated, use nafOn instead");
  const isDev = $("body").data("app-version") === "dev";
  const eventName = `${event}.${dataTag}`;
  if (newFormat) {
    $body.off(eventName).on(eventName, `[data-${dataTag}]`, e => {
      if (isDev) {
        console.log(`naf.${$body.data("naf-code")}: ${event} / ${dataTag}`);
      }
      const $node = $(e.currentTarget);
      callback($node.data(dataTag), $node, e);
    });
  } else {
    $body.off(eventName).on(eventName, `[data-${dataTag}]`, callback);
  }
};

const nafOn = ($body, event) => (dataTag, callback) => {
  const isDev = $("body").data("app-version") === "dev";
  const eventName = `${event}.${dataTag}`;
  $body.off(eventName).on(eventName, `[data-${dataTag}]`, (e, params) => {
    if (isDev) {
      console.log(`naf.${$body.data("naf-code")}: ${event} / ${dataTag}`);
    }
    const $node = $(e.currentTarget);
    callback($node.data(dataTag), $node, e, params);
  });
};

const backStageRequest = (method, url, data) => {
  window.navyRequest({
    url,
    method,
    data,
    aimed: "#backstage",
    navyPeek: true,
  });
};

const nafOpen = R.curry(($body, {method, app, params, callback, onClose}) => {
  const nafId = newID();
  backStageRequest(
    method || "post",
    `/app/${app}`,
    R.mergeRight(params || {}, {nafId})
  );
  if (callback) {
    $body.on(`callback_${app}.${nafId}`, (e, value) => {
      console.log(`callback of ${app} received`);
      $body.off(`callback_${app}.${nafId}`);
      callback(value);
    });
  }
  if (onClose) {
    $body.on(`close_${app}.${nafId}`, (e, value) => {
      console.log(`${app} closed`);
      $body.off(`close_${app}.${nafId}`);
      onClose(value);
    });
  }
});

export {
  initNavyAppFramework,
  appReturn,
  backStageRequest,
  nafOn,
  nafOnClick,
  nafOpen,
};
