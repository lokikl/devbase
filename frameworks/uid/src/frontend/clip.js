// devbase: ensure=always

/*
 * This module is for generating dynamic section divider with the clip-path CSS property.
 * This module leverage UID divider tool to construct the clip path interactively.
 *
 * To enable this plugin
 * 1) Add to your frontend.js
 * 2) In UID, turn on Load External JS in preview options
 * 3) Call mixin +divider({})#divider1
 * 4) Right click on the line and summon the UID divider tool
 * 5) Add data tag in the underneath element like
 *    .the-next-section(data-clip={id: "divider1", height: 150, minWidth: 1400})
 */

import * as R from "ramda";
import { throttle } from "throttle-debounce";

const raf = () => new Promise(resolve => window.requestAnimationFrame(resolve));

const parsePath = R.pipe(
  R.replace(/\n/g, " "),
  R.replace(/-/g, " -"),
  R.replace(/,/g, " "),
  R.replace(/([^\d -.])/g, "$1 "),
  R.replace(/ +/g, " "),
  R.split(/(?=[^\d -.])/),
  R.map(R.trim)
);

const mapIndexed = R.addIndex(R.map);

const scalePath = (xOffset, xScale, yScale) =>
  R.map(pp => {
    const parts = pp.split(" ");
    if (R.includes(parts[0], ["M", "C", "L"])) {
      return R.pipe(
        mapIndexed((n, i) => (i % 2 === 0 ? n * xScale + xOffset : n * yScale)),
        R.prepend(parts[0]),
        R.join(" ")
      )(parts.slice(1));
    }
    return pp;
  });

const initClip = () => {
  const $body = $("body");

  const update = () => {
    $("[data-clip]", $body).each((_, node) => {
      const $node = $(node);
      const { id, height, minWidth } = $node.data("clip");
      $node.css({
        "margin-top": `-${height}px`,
        "padding-top": `${height}px`,
        "clip-path": `url('#${id}')`
      });

      const nodeWidth = $node.innerWidth();
      const xScale = minWidth ? Math.max(1, minWidth / nodeWidth) : 1;
      const xOffset = xScale !== 1 ? -(xScale - 1) / 2 : 0;

      const yScale = height / $node.innerHeight();

      const $path = $(`#${id} > path`);
      if (!$path.data("orig")) {
        $path.data("orig", parsePath($path.attr("d")));
      }
      const origPath = $path.data("orig");
      const newPath = scalePath(xOffset, xScale, yScale)(origPath);
      $path.attr("d", newPath.join(" "));
    });
  };

  $body.on("statechanged", update);
  $(window).resize(update);
  update();
};

export { initClip };
