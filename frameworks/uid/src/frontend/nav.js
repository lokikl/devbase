// devbase: ensure=always

import * as R from "ramda";

const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

const raf = () => new Promise(resolve => window.requestAnimationFrame(resolve));

const pathnameFromUrl = R.pipe(
  R.split("/"),
  R.slice(3, Infinity),
  R.join("/"),
  R.split("?"),
  R.head,
  str => `/${str}`
);

// const defaultAnimation = "fade";
const defaultAnimation = "none";

const determineAnimation = (prev, next) => {
  if (prev === next) {
    return defaultAnimation;
  }
  // if (R.startsWith(prev, next)) {
  //   return "forward";
  // }
  // if (R.startsWith(next, prev)) {
  //   return "backward";
  // }
  const pathOrder = $("body").data("path-order");
  if (pathOrder) {
    const prevIdx = R.indexOf(prev.split("/")[1], pathOrder);
    const nextIdx = R.indexOf(next.split("/")[1], pathOrder);
    if (nextIdx < prevIdx) {
      return "slide-left";
    }
    return "slide-right";
  }
  return defaultAnimation;
};

const getAlerts = $page => {
  const $alerts = $(".alert", $page);
  if ($alerts.length > 0) {
    return $alerts;
  }
  return $page.filter(".alert");
};

const fetchGet = url => async () =>
  fetch(url, {
    method: "GET",
    redirect: "follow",
    credentials: "same-origin",
  });

const fetchPost = (url, data) => {
  const formData = new FormData();
  R.mapObjIndexed((v, k) => formData.append(k, v))(data);
  return async () =>
    fetch(url, {
      method: "post",
      body: formData,
    });
};

/* eslint-disable fp/no-rest-parameters */
const withMutexLock = fn => {
  let isLocked = false;
  return async (...args) => {
    if (isLocked) {
      return true;
    }
    isLocked = true;
    try {
      await fn(...args);
    } finally {
      isLocked = false;
    }
  };
};

const initNav = () => {
  const $body = $("body");

  let currentUrl = false;

  const fetchAndRender = withMutexLock(
    async ({
      fetchFn,
      fetchUrl,
      pushState,
      delayMS = 0,
      needLoader = false,
      dir = defaultAnimation,
    }) => {
      if (!fetchFn && fetchUrl) {
        fetchFn = fetchGet(fetchUrl);
      }
      const $base = $(".nav-base", $body);
      const tBefore = new Date();
      if (needLoader) {
        $(`<div class="loader">Loading...</div>`).appendTo($base);
      }
      if (delayMS > 0) {
        await delay(delayMS);
      }
      const resp = await fetchFn();

      if (resp.ok) {
        // dismiss all uid prompts
        $(".up-backdrop").each((i, node) => {
          $(node).data("dismissAndResolve")("cancel-by-statechanged");
        });
        // highlight current tab in nav
        $(`.nav > a`)
          .removeClass("active")
          .each((i, node) => {
            const $btn = $(node);
            if (R.startsWith($btn.prop("href"), resp.url)) {
              $btn.addClass("active");
            }
          });

        const html = await resp.text();
        const $page = $(html);

        const $next = $(".nav-base", $page)
          .insertAfter($base)
          .addClass(`nav-load__${dir}`);
        $base.addClass("nav-unload");
        await raf();
        await delay(10);
        $base.addClass(`nav-unload__${dir}`);
        $next.addClass("nav-entering");
        await delay(400);

        $base.remove();
        $next.removeClass(`nav-load__${dir}`).removeClass("nav-entering");

        const pageTitle = $page.filter("title").text();

        window.document.title = pageTitle;
        if (pushState) {
          window.history.pushState({}, pageTitle, resp.url);
        } else {
          window.history.replaceState({}, pageTitle, resp.url);
        }
        getAlerts($page).appendTo($body);
        currentUrl = resp.url;
        $body.trigger("statechanged");
      } else {
        const {status, statusText} = resp;
        $base.replaceWith(
          `<div class="nav-base text-center">${status} ${statusText}</div>`
        );
      }
    }
  );

  window.navFetchAndRender = fetchAndRender;

  window.onpopstate = async event => {
    const $base = $(".nav-base", $body);
    $base.addClass("nav-upload__forward");
    const url = document.location;
    await fetchAndRender({
      fetchFn: fetchGet(url),
      pushState: false,
      dir: determineAnimation(pathnameFromUrl(currentUrl), url.pathname),
    });
  };

  const linkTarget = "a:not(.nav-skip):not([target=_blank])";
  const formTarget = "form:not(.nav-skip)";
  const isInternalUrl = R.startsWith(document.location.origin);

  $body.on("click.nav-click", linkTarget, async e => {
    const $btn = $(e.currentTarget);
    const url = $btn[0].href;
    if (R.isNil(url) || url === "") return true;
    if (!isInternalUrl(url)) return true;
    e.preventDefault();

    if (url === window.location.href) {
      return true;
    }
    await fetchAndRender({
      fetchFn: fetchGet(url),
      pushState: true,
      delayMS: $btn.data("nav-delay"),
      dir: determineAnimation(window.location.pathname, pathnameFromUrl(url)),
    });
  });

  $body.on("submit.nav-form-submit", formTarget, async e => {
    e.preventDefault();
    const $form = $(e.currentTarget);
    const method = $form.attr("method").toLowerCase();
    const action = $form.attr("action");
    let fetchFn = false;

    if (method === "get") {
      const url = new URL(action);
      R.forEach(input => {
        url.searchParams.set(input.name, input.value);
      })($form.serializeArray());
      fetchFn = () => fetch(url.toString());
    } else {
      fetchFn = () =>
        fetch($form.attr("action"), {
          method: "post",
          body: new FormData($form[0]),
        });
    }
    await fetchAndRender({
      fetchFn,
      pushState: false,
    });
  });

  $body.on("click.upload-file", `[data-upload-file]`, async e => {
    const $btn = $(e.currentTarget);
    const {accept, to: url, sizeLimitMB} = $btn.data("upload-file");

    $(".upload-file-input", $body).remove();
    const $input = $(`<input type="file" accept="${accept}">`)
      .addClass("upload-file-input")
      .css("transform", "translate(0, -9999px)")
      .appendTo($body);

    $input.on("change", async () => {
      const {
        files: [file],
      } = $input[0];
      if (file.size > sizeLimitMB * 1024 * 1024) {
        alert(`File too large (${sizeLimitMB}MB)`);
        return false;
      }
      /* eslint-disable no-undef */
      const formData = new FormData();
      formData.append("file", file);
      const fetchFn = () =>
        fetch(url, {
          method: "POST",
          body: formData,
        });
      await fetchAndRender({
        fetchFn,
        pushState: false,
      });
      // const rJson = await r.json();
      // // console.log("rJson" + ": " + JSON.stringify(rJson));
      // // @TODO loki, 12/05/2020: popup dialog to show changes and confirm with users
      // window.location.reload();
      $input.remove();
    });
    $input.trigger("click");
  });
};

export {initNav, fetchGet, fetchPost};
