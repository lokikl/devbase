// devbase: ensure=always

import * as R from "ramda";
import imagesLoaded from "imagesloaded";
import clientIncludes from "../uid/client-includes.pug";
import * as frontendHelpers from "../frontend-helpers";

imagesLoaded.makeJQueryPlugin($);

const SLUG_PATTERN = /^[a-z]+[a-zA-Z0-9-_]*$/;

const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

/* eslint-disable fp/no-rest-parameters */
/* eslint-disable no-restricted-syntax */
const interpolate = (literals, expressions) => {
  let string = ``;
  for (const [i, val] of expressions.entries()) {
    string += literals[i] + val;
  }
  string += literals[literals.length - 1];
  return string;
};

const trimPaddingSpaces = (string) => {
  const lines = string.split("\n").slice(1, -1);
  const nSpaces = lines[0].match("^ *")[0].length;
  return R.pipe(
    R.map((line) => line.slice(nSpaces)),
    R.join("\n")
  )(lines);
};

const trim = (literals, ...expressions) => {
  const string = interpolate(literals, expressions);
  return trimPaddingSpaces(string);
};

const pugCompile = (literals, ...expressions) => {
  let string = interpolate(literals, expressions);
  string = trimPaddingSpaces(string);
  return pug.compile(string);
};

const nextAnimationFrame = () =>
  new Promise((resolve) => window.requestAnimationFrame(resolve));

const quickInfo = async (msg, level = "primary", autoDismiss = true) => {
  const html = pug.render(trim`
    .alert.alert-dismissible.alert-${level}.fade.show
      span ${msg}
      button.close(type="button", data-dismiss="alert")
        span &times;
  `);
  const $alert = $(html).appendTo($("body"));
  await nextAnimationFrame();
  $alert.addClass("alert-displayed");
  await delay(2000);
  if (autoDismiss) {
    $alert.addClass("alert-removed");
    await delay(500);
    $alert.remove();
  }
};

const waitUntilAllImagesLoaded = ($node) =>
  new Promise((resolve) => {
    $node.imagesLoaded().always(resolve);
  });

const positionPrompt = ($form, nodeBox, direction) => {
  const {width, height} = $form[0].getBoundingClientRect();

  const $window = $(window);
  const scrHeight = $window.height();
  const scrWidth = $window.width();

  let top = nodeBox.top - 26;
  let left = 0;
  if (direction === "left") {
    left = nodeBox.left - width - 2;
  } else if (direction === "right") {
    left = nodeBox.left + nodeBox.width + 2;
  } else if (direction === "bottom") {
    left = nodeBox.left;
    top = nodeBox.top + nodeBox.height;
  } else if (direction === "select") {
    left = nodeBox.left - 1;
    top = nodeBox.top - 1;
    $form.css({width: nodeBox.width + 2});
  } else if (direction === "bottom-left") {
    left = nodeBox.left - width + nodeBox.width;
    top = nodeBox.top + nodeBox.height + 4;
  } else if (direction === "center") {
    top = scrHeight / 2 - height / 2;
    left = scrWidth / 2 - width / 2;
  }
  if (top + height > scrHeight) {
    top = scrHeight - height - 2;
  }
  if (left + width > scrWidth) {
    left = scrWidth - width - 2;
  }
  $form.css({top: Math.max(0, top), left: Math.max(0, left)});
};

const uidPrompt = ({
  $wrapper,
  $node,
  html,
  extraDataUrl,
  onReady,
  direction = "right",
  dismissOnOutsideClick = false,
  autoDismissInSecs = false,
  backdropNoFade = false,
  transparentBackdrop = false,
}) =>
  new Promise((resolve) => {
    const $body = $wrapper || $("body");
    $body.addClass("uid-prompt-openned");
    // const $body = $node.closest(".app-initialized,body");
    const nodeBox = $node[0].getBoundingClientRect();
    const $backdrop = $(`
        <div class="up-backdrop">
          <div class="up-canvas toward-${direction}">
            ${html}
          </div>
        </div>
      `).appendTo($body);

    if (backdropNoFade || transparentBackdrop) {
      $backdrop.addClass("no-animation");
    }
    if (transparentBackdrop) {
      $backdrop.addClass("transparent");
    }
    const $form = $backdrop.children(".up-canvas");

    waitUntilAllImagesLoaded($backdrop).then(() => {
      positionPrompt($form, nodeBox, direction);
      $(window).on("scroll.position-uid-prompt", (e) => {
        const newBox = $node[0].getBoundingClientRect();
        positionPrompt($form, newBox, direction);
      });

      const randomId = `${Math.random()}`.split(".")[1];

      const dismissAndResolve = (action) => {
        $(window).off("scroll.position-uid-prompt");
        $backdrop.off("mousedown.dismiss-uid-prompt-on-outside-click");
        $form.parent().addClass("dismissed");
        if ($(".up-backdrop:not(.dismissed)").length === 0) {
          $body.removeClass("uid-prompt-openned");
        }
        resolve({$form, action});
        delay(300).then(() => $form.parent().remove());
      };

      $backdrop.data("dismissAndResolve", dismissAndResolve);

      if (dismissOnOutsideClick) {
        $backdrop.on("mousedown.dismiss-uid-prompt-on-outside-click", (e) => {
          if ($(e.target).closest(".up-canvas").length === 0) {
            dismissAndResolve(dismissOnOutsideClick);
          }
        });
      }

      if (autoDismissInSecs) {
        setTimeout(() => {
          dismissAndResolve("cancel");
        }, autoDismissInSecs * 1000);
      }

      window.requestAnimationFrame(async () => {
        $form.parent().addClass(`displayed`);
        $form.removeClass(`toward-${direction}`);
        $("[autofocus]:visible", $form).focus();
        $("[data-press-enter-to-resolve]", $form).each((i, node) => {
          $(node).on("keyup", (e) => {
            if (e.key === "Enter") {
              const action = $(node).data("press-enter-to-resolve");
              dismissAndResolve(action);
            }
          });
        });
        if (onReady) {
          onReady($form, dismissAndResolve);
        }
        $form.on("click.uid-prompt-resolve", "[data-resolve]", (e) => {
          dismissAndResolve($(e.currentTarget).data("resolve"));
        });
        $form.on(
          "submit.uid-prompt-resolve",
          "form[data-submit-resolve]",
          (e) => {
            e.preventDefault();
            e.stopPropagation();
            dismissAndResolve($(e.currentTarget).data("submit-resolve"));
          }
        );
        $form.on(
          "mousedown.uid-prompt-move",
          "h1:first-of-type, h2:first-of-type, h3:first-of-type, h4:first-of-type, h5:first-of-type",
          (e) => {
            if (e.which !== 1) return true;
            const {top, left} = $form.position();
            $form.addClass("moving");
            const onMove = (me) => {
              const dx = me.clientX - e.clientX;
              const dy = me.clientY - e.clientY;
              $form.css({
                top: top + dy,
                left: left + dx,
              });
            };
            $body.on("mousemove.uid-prompt-move", onMove);
            $(document).on("mouseup.uid-prompt-move", () => {
              $body.off("mousemove.uid-prompt-move");
              $(document).off("mouseup.uid-prompt-move");
              $form.removeClass("moving");
            });
            return false;
          }
        );
      });
    });
  });

const padSpacesToLines = (padStr, lines, sliceFirstLine = false) =>
  R.pipe(
    R.split("\n"),
    R.map((l) => `${padStr}${l}`),
    R.join("\n"),
    (output) => (sliceFirstLine ? output.slice(padStr.length) : output)
  )(lines);

const uid = (mixin, args) => {
  const assetsContext = $("[data-assets-context]").data("assets-context");
  if ($("body").data("app-version") === "dev") {
    console.log(`render mixin: ${mixin}`);
    console.log(JSON.stringify(args, null, 2));
    // send mixin signal to uid server
    const formData = new FormData();
    formData.set("payload", JSON.stringify({mixin, params: args}));
    fetch("/open-mixin", {method: "post", body: formData});
  }
  return clientIncludes(
    R.merge(frontendHelpers, {
      asset: (filename) => `${assetsContext}/${filename}`,
      mixin,
      args,
    })
  );
};

const handleMultipleInput = ($prompt, label, defaultOption) => {
  const addItem = () => {
    const html = uid(`one-${label}`, defaultOption);
    $(html)
      .appendTo($(`.${label}-wrap`, $prompt))
      .find("input")
      .first()
      .focus();
  };
  $prompt.on(`click.add-${label}`, `[data-add-${label}]`, addItem);
  $prompt.on(
    `keydown.add-${label}-on-enter`,
    `[data-add-${label}-on-enter]`,
    (e) => {
      if (e.which === 13) {
        addItem();
        return false;
      }
    }
  );
  $prompt.on(`click.remove-${label}`, `[data-remove-${label}]`, (e) => {
    $(e.currentTarget).closest(`.one-${label}-grid`).remove();
    return false;
  });
};

const initUIDExposedMixins = R.pipe(
  ($body) => $("i.uid-mixin", $body).toArray(),
  R.map((node) => {
    const {name, content} = $(node).data();
    return `-\n  pug_mixins["${name}"] = ${padSpacesToLines(
      "    ",
      content,
      true
    )}`;
  }),
  R.append("+#{mixin}(args)"),
  (str) => {
    const assetsContext = $("[data-assets-context]").data("assets-context");
    return R.prepend(
      `- var asset = (filename) => "${assetsContext}/" + filename;`,
      str
    );
  },
  R.join("\n"),
  (mixins) => pug.compile(mixins),
  (fn) =>
    $("body").data("app-version") === "dev"
      ? (mixin, args, helpers = {}) => {
          console.log(`render mixin: ${mixin}`);
          console.log(`args: ${JSON.stringify(args)}`);
          console.warn(
            "initUIDExposedMixins is deprecated, please use uid() instead"
          );
          return fn(R.mergeRight(helpers, {mixin, args, R}));
        }
      : (mixin, args, helpers) => fn(R.mergeRight(helpers, {mixin, args, R}))
);

export {
  SLUG_PATTERN,
  pugCompile,
  trim,
  uidPrompt,
  quickInfo,
  uid,
  handleMultipleInput,
  // initUIDExposedMixins is deprecated, please use uid() instead
  initUIDExposedMixins,
};
