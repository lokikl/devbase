// used in uid
import * as R from "ramda";
import i18next from "i18next";
import translationEN from "./uid/locales/translation.en.json";
import translationCHT from "./uid/locales/translation.cht.json";

i18next.init({
  lng: "en",
  resources: {
    en: {
      translation: translationEN,
    },
    cht: {
      translation: translationCHT,
    },
  },
});

const languages = ["en", "cht"];

const __ = i18next.t.bind(i18next);

const keyize = (name) =>
  name
    .toLowerCase()
    .replace(/[^\w]+/g, "-")
    .replace(/-$/, "");

const capitalize = (name) => name.charAt(0).toUpperCase() + name.slice(1);

const titleize = R.pipe(
  R.split(/[\s-_]/),
  R.map((str) => capitalize(str)),
  R.join(" ")
);

export {R, keyize, capitalize, titleize, i18next, languages, __};
