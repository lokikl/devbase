// devbase: ensure=always

import * as R from "ramda";
import excel from "node-excel-export";
import knex from "../transport/database";

const toTable = ({ rows, fields }) => ({
  rows,
  fields: R.pipe(R.map(R.prop("name")), R.reject(R.startsWith("_")))(fields)
});

const DefaultTableOptions = {
  itemsPerPage: 30,
  page: 1
};

const formFilteredCTE = options => {
  const filteredCTE = knex.from("_source");
  R.mapObjIndexed((v, k) => {
    if (
      R.isNil(options.filterable[k]) ||
      options.filterable[k] === "freetext"
    ) {
      filteredCTE.where(k, "ilike", `%${v}%`);
    } else {
      filteredCTE.where(k, v);
    }
  })(options.filters || {});
  return filteredCTE;
};

const formOutputCTE = (options, offset) => {
  const outputCTE = knex
    .select("*", knex.raw("null::json"))
    .from("_filtered")
    .offset(offset)
    .limit(options.itemsPerPage);

  if (options.sort) {
    const [orderBy, order] = options.sort;
    outputCTE.orderBy(orderBy, order);
  }
  return outputCTE;
};

const formFilterOptionSQL = R.pipe(
  R.toPairs,
  R.filter(([k, v]) => R.equals(v, "select")),
  R.map(R.head),
  R.map(name => `array_agg(distinct ${name}) as ${name}`),
  R.join(",")
);

const sqlToTableExt = sql => query => opts => async (params = {}) => {
  const options = R.pipe(
    base64 => (base64 ? Buffer.from(base64, "base64").toString() : "{}"),
    json => JSON.parse(json),
    R.pipe(
      R.dissoc("id"),
      R.dissoc("itemsPerPage"),
      R.dissoc("sortable"),
      R.dissoc("filterable")
    ),
    R.mergeRight(opts),
    R.mergeRight(DefaultTableOptions)
  )(query[opts.id]);

  const offset = (options.page - 1) * options.itemsPerPage;

  const filterOptionsSQL = formFilterOptionSQL(options.filterable);
  const filteredCTE = formFilteredCTE(options);
  const outputCTE = formOutputCTE(options, offset);

  // incase there are no select in filterable options
  const fullFilterOptionsSQL = `
      select
        _source.*,
        json_build_object(
          ${filterOptionsSQL ? `'fopts', row_to_json(tFOpts),` : ""}
          'count', tCount.count) as _stats
      from
        _source,
        ${
          filterOptionsSQL
            ? `(
            select ${filterOptionsSQL} from _source
          ) tFOpts,`
            : ""
        }
        (
          select count(1) as count from _filtered
        ) tCount
      limit 1`;

  const combined = knex.raw(
    `
      with
        _source as (${sql}),
        _filtered as :filteredCTE,
        _output as :outputCTE,
        _stats as (
          ${fullFilterOptionsSQL}
        )
        select * from _stats
        union all
        select * from _output
    `,
    R.mergeRight(params, { filteredCTE, outputCTE })
  );

  // console.log(combined.toQuery());
  const result = await combined;

  if (result.rows.length > 0) {
    // first row is always for stats, i.e. total rows count after filtered, and filter options
    const {
      rows: [{ _stats: stats }]
    } = result;
    result.rows = R.tail(result.rows);

    return R.mergeRight(toTable(result), {
      options,
      stats: R.mergeRight(stats, {
        pagesCount: Math.ceil((stats.count * 1.0) / opts.itemsPerPage),
        startAt: offset + 1,
        endAt: offset + result.rows.length
      })
    });
  }
  // handle no record
  return R.mergeRight(toTable(result), { options });
};

const exportXlsx = sheets => {
  const content = R.map(
    ({ title, name, data, columns, headerStyle, cellStyle }) => {
      const heading = title
        ? [[{ value: title, style: { alignment: { horizontal: "center" } } }]]
        : null;
      const merges = title
        ? [
            {
              start: { row: 1, column: 1 },
              end: { row: 1, column: R.keys(columns).length }
            }
          ]
        : [];
      return {
        heading,
        name,
        merges,
        specification: R.mapObjIndexed((v, k) => {
          v.headerStyle = R.mergeRight(headerStyle, v.headerStyle || {});
          v.cellStyle = R.mergeRight(cellStyle, v.cellStyle || {});
          return v;
        })(columns),
        data
      };
    },
    sheets
  );

  return excel.buildExport(content);
};

export { sqlToTableExt, exportXlsx };
