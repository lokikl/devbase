" devbase: ensure=always
set backupcopy=yes

nnoremap sb :exec 'CtrlP .devbase'<cr>
nnoremap sm :exec 'CtrlP lib'<cr>
nnoremap sr :exec 'CtrlP lib/core/router'<cr>
nnoremap sh :exec 'CtrlP lib/ui'<cr>
nnoremap sa :exec 'CtrlP lib/core/viewmodel'<cr>
nnoremap su :exec 'CtrlP lib/core/util'<cr>

" You need to add below plugin in your vimrc
" Plugin 'lokikl/vim-ctrlp-anything'
nnoremap sd :CtrlPanythingLocate db-files-to-ctrlp<cr>
" nnoremap se :CtrlPanythingLocate express-log-to-ctrlp<cr>

nnoremap <leader>r :call TmuxSplitExec("reboot-dev", "-d")<cr>
nnoremap <leader>f :call TmuxSplitExec("dartfmt -w /app/lib")<cr>
nnoremap <leader>o :call ConsoleCtl("reset")<cr>

nnoremap co :call TmuxSplitExec("console")<cr>
nnoremap <leader>c :call ConsoleCtl("toggle")<cr>

nnoremap st :call GitStatus()<cr>
autocmd! BufWritePost *.dart call ConsoleCtl("hot-reload")

function! TmuxSplitExec(subcmd, ...)
  let tmuxArg = get(a:, 1, "")
  let cmd = "source .envrc; echo " . a:subcmd . "; " . a:subcmd
  let cmd = "tmux splitw " . tmuxArg . " \"zsh -c '" . cmd . "'\""
  call system(cmd)
endfunction

function! GitStatus()
  let cmd = 'git status'
  let cmd = "tmux splitw \"zsh -c '" . cmd . "; read'\""
  call system(cmd)
endfunction

function! ConsoleCtl(subcmd)
  call system("nohup console-ctl " . a:subcmd . " &")
endfunction
