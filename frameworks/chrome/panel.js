const getCurrentTab = () =>
  new Promise((resolve) => {
    chrome.tabs.query({active: true, lastFocusedWindow: true}, (tabs) => {
      resolve(tabs[0]);
    });
  });

// chrome.tabs.onUpdated.addListener(async (tabId, changeInfo, tab) => {
//   const currentTab = await getCurrentTab();
//   if (tabId === currentTab.id) {
//     if (changeInfo.url) {
//       $('.log').text(changeInfo.url);
//     }
//   }
// });

$(() => {
  // curl -XPOST https://dash.pointup-dev.com/_/resetdb
  $(".btn-reset-db").click(async () => {
    const currentTab = await getCurrentTab();
    const host = currentTab.url.split("/")[2];
    const url = `https://${host}/_/resetdb`;
    // $(".log").text(url);
    await fetch(url, {
      cache: "no-cache",
      method: "POST",
    });
    chrome.tabs.reload(currentTab.id);
  });
});
