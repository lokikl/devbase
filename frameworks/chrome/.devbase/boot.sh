#!/bin/bash
# devbase: ensure=always
# devbase: executable=yes

# SIGTERM-handler
term_handler() {
  if [ $pid -ne 0 ]; then
    kill -SIGTERM "$pid"
    wait "$pid"
  fi
  exit 143; # 128 + 15 -- SIGTERM
}

# setup handlers
trap 'kill ${!}; term_handler' SIGTERM

# run application
echo "=== yarn check --integrity ==================================================================================="
if ! yarn check --integrity; then
  echo "=== yarn install ============================================================================================="
  yarn install
fi

echo "=== yarn run node-sass ======================================================================================="
yarn run node-sass --output-style=compressed --source-map=true panel.scss extension/panel.css
yarn run node-sass --output-style=compressed --source-map=true panel.scss extension/panel.css -w &

echo "=== yarn run webpack ========================================================================================="
yarn run webpack -w --mode=development --devtool=inline-source-map &

pid="$!"

# wait forever
while true; do
  tail -f /dev/null & wait ${!}
done

