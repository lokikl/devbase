/**
 * $.parseParams - parse query string paramaters into an object.
 */
(function($) {
  var re = /([^&=]+)=?([^&]*)/g;
  var decodeRE = /\+/g;  // Regex for replacing addition symbol with a space
  var decode = function (str) {return decodeURIComponent( str.replace(decodeRE, " ") );};
  $.parseParams = function(query) {
    var params = {}, e;
    while ( e = re.exec(query) ) {
      var k = decode( e[1] ), v = decode( e[2] );
      if (k.substring(k.length - 2) === '[]') {
        k = k.substring(0, k.length - 2);
        (params[k] || (params[k] = [])).push(v);
      }
      else params[k] = v;
    }
    return params;
  };
})(jQuery);



$(document).ready(function () {

  function _hide(query, default_value) {
    $(query)
    .addClass('hidden')
      .find('.disable-when-hidden:hidden').attr('disabled', 'disabled');
    if ($(query).is('.disable-when-hidden:hidden')) {
      $(query).attr('disabled', 'disabled');
    }
    if (default_value) {
      $(query).find('input.clear-when-hidden').val(default_value);
      $(query).find('input.clear-when-hidden').focusout();
    }
  }

  function hide(query, default_value, keepWhenNotFound, show) {
    if (keepWhenNotFound && show) {
      $(query).each(function() {
        if ($(this).parents(keepWhenNotFound).find(show).length > 0) {
          _hide(this, default_value);
        }
      });
    } else {
      _hide(query, default_value);
    }
  }

  function show(query) {
    if (!query) return;
    var $ele = $(query);
    if ($ele.hasClass('hidden')) {
      $ele
        .removeClass('hidden');
        //.css('opacity', 0)
        //.animate({'opacity': 1}, 600);
    }
    if (!$ele.hasClass('hidden')) {
      $ele.find('.disable-when-hidden').each(function() {
        var $this = $(this);
        if ($this.parents('.hidden').length === 0) {
          $this.removeAttr('disabled');
        }
      });
      if ($ele.hasClass('disable-when-hidden')) {
        $ele.removeAttr('disabled');
      }
    }
  }

  $('body').on('click.hideshow', '[data-hide]', function() {
    var $this = $(this);
    var q = $this.data();
    var default_value = $this.data("set-default-when-hidden");
    hide(
      scopedQuery(this, q.hide), default_value, q.hideshowKeepWhenNotFound, q.show
    );
    show(
      scopedQuery(this, q.show)
    );

    $this.parents('.hideshow-parent')
      .find('.hideshow-current').removeClass('hideshow-current');

    if ($this.parents('.hideshow-parent').length > 0) {
      $this.addClass('hideshow-current');
    }

    // update the 'search' part of current href
    // e.g. qstring = "abc=123"
    var qstring = $this.data('update-querystring');
    if (qstring) {
      var href = location.href;
      // ?a=2&b=3 => {a:2, b:3}
      var query = $.parseParams(location.search.substr(1))
      var to = qstring.split('=');
      query[to[0]] = to[1];
      // {a:2, b:3, abc:123} => ?a=2&b=3&abc=123
      var output = $.param(query);
      history.pushState(null, '', location.pathname + '?' + output);
    }
  });

  function scopedQuery(ele, query) {
    var $this = $(ele);
    var $scope = $this.parents('.hideshow-scope').first(); // closest scope
    if (query !== undefined) {
      var parent_q = query.split('!^')[1];
      query = query.split('!^')[0];
    }
    if (!!$scope.length) {
      var $target = $(query, $scope);
    } else {
      var $target = $(query);
    }
    if (parent_q) {
      $target = $target.parents(parent_q);
    }
    return $target;
  }

  function initializeHideshow() {
    $('select[data-hideshow]').each(function() {
      var $this = $(this);
      if ($('option.hideshow-current', $this).length === 0) {
        $('option:selected', $this).addClass('hideshow-current')
      }
    })
    $('.hideshow-current:not(.hideshow-no-auto)').each(function() {
      var $this = $(this);
      if ($this.is('option')) {
        $this.parent().trigger('change.hideshow');
      } else {
        $this.trigger('click.hideshow');
      }
    });
  }

  $('body').on('click.hideshow', '[data-toggle]', function() {
    var q = $(this).data('toggle');
    scopedQuery(this, q).each(function() {
      var $ele = $(this);
      if ($ele.hasClass('hidden')) {
        show($ele);
      } else {
        hide($ele);
      }
    });
  });

  $('body').on('click', '[data-toggle-check]', function() {
    var q = $(this).data('toggle-check');
    $("input[type=checkbox]" + q).each(function() {
      $(this).trigger('click');
    });
  });

  $('body').on('change.hideshow', 'select[data-hideshow]', function() {
    var self = $(this)
    if(!self.hasClass("prevent_hideshow_when_disable") || !self.attr("disabled")) {
      var qhide = $(this).data('hideshow');
      hide(scopedQuery(this, qhide));
      $(this).find('option:selected').each(function() {
        var qshow = $(this).data('show');
        show(scopedQuery(this, qshow));
      });
      $(this).children('option.hideshow-current').removeClass('hideshow-current'); // prevent multiple trigger
    }
  });

  $('body').on('click.hideshow', 'input[data-hideshow]', function() {
    var self = $(this)
    if(!self.hasClass("prevent_hideshow_when_disable") || !self.attr("disabled")) {
      var qhide = $(this).data('hideshow');
      hide(scopedQuery(this, qhide));
      $(this).find('option:selected').each(function() {
        var qshow = $(this).data('show');
        show(scopedQuery(this, qshow));
      });
      $(this).children('option.hideshow-current').removeClass('hideshow-current'); // prevent multiple trigger
    }
  });

  $('body').on('statechanged', initializeHideshow);
  initializeHideshow();

});
