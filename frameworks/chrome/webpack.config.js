const path = require("path");
const MergeIntoSingleFilePlugin = require("webpack-merge-and-include-globally");

module.exports = [
  {
    entry: {
      panel: ["./panel.js"]
    },
    output: {
      path: path.resolve(__dirname, "extension"),
      filename: '[name].js'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "babel-loader"
        }
      ]
    },
    plugins: [
      new MergeIntoSingleFilePlugin({
        files: {
          "vendor.js": [
            "node_modules/jquery/dist/jquery.js",
            "vendor/hide-show.js"
          ]
        }
      })
    ]
  }
];
