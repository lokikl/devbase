# devbase: ensure=always
# https://github.com/direnv/direnv

realpath() {
  [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

path_add() {
  if [[ ! $PATH = *"$1"* ]]; then
    export PATH="$1:$PATH"
  fi
}

# Example: export_alias zz "ls -la"
export_alias() {
  local name=$1
  shift
  local alias_dir=$PWD/.direnv/aliases
  local target="$alias_dir/$name"
  mkdir -p "$alias_dir"
  path_add "$alias_dir"
  echo "#!/usr/bin/env bash" > "$target"
  echo "$@" >> "$target"
  chmod +x "$target"
}

export __project_path=$(pwd)
export __uid=$(id -u)
export __docker_sock_path=/var/run/docker.sock
export __docker_gid=$(ls -n $__docker_sock_path | awk '{print $4}')
export __dev_port=40000
export COMPOSE_FILE=./build/docker-compose.yml
export FOLDER_NAME=${__project_path##*/}
export COMPOSE_PROJECT_NAME=$(echo $FOLDER_NAME | tr -d '-')

if which ip 1>/dev/null 2>/dev/null; then # ubuntu
  local default_nic=$(ip r | grep '^default' | sed -e 's/.* dev //' | awk '{print $1}')
  export __host_ip=$(ip r | grep "dev $default_nic" | grep src | head -n1 | sed -e 's/.* src //' | awk '{print $1}')
else
  local default_nic=$(route -n get default | grep interface | awk '{print $2}')
  export __host_ip=$(ifconfig en0 | grep "inet " | awk '{print $2}')
fi

# https://github.com/direnv/direnv/issues/73#issuecomment-174295790
export_alias console 'docker-compose exec -u $__uid -e COLUMNS=400 -e LINES=80 app bash'
path_add "$__project_path/.devbase/bin"

mkdir -p node_modules
mkdir -p dist/public src/uid/assets
