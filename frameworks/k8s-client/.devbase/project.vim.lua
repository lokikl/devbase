--[[ devbase: ensure=always ]]

local exec = function(cmd)
  return function()
    exec_in_split(cmd)
  end
end

which_key_map("r", {
  l = { console_ctl("source .envrc; connect", "34"), "Open connector" },
})

which_key_map("c", {
  j = { exec("ssh deuat-jump"), "SSH to jumpbox" }, --[[ then ssh deuat ]]
})

