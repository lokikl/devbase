# devbase: ensure=always

# short forms for faster daily uses
alias k='kubectl'
alias kls="kubectl get pod"
alias klsw="watch -n3 'kubectl get pod'"
alias kp="kubectl get pod -o wide"
alias kd="kubectl get deployment -o wide"
alias ki="kubectl get ingress -o wide"
alias ks="kubectl get service -o wide"
alias kn="kubectl get node -o wide"
alias kj="kubectl get job; kubectl get cronjob"
alias kdp="kubectl describe pod"
alias kdd="kubectl describe deployment"
alias kds="kubectl describe service"

# override PROMPT_COMMAND to show current namespace
PROMPT_COMMAND=prompter
function prompter() {
  ns=`k config get-contexts --no-headers | awk '{print $NF}'`
  export PS1="\t $ns \$ "
}

function ka() {
  output=`kubectl apply -f $1`
  echo "$output" | grep -v unchanged
  echo --- unchanged ---
  echo "$output" | grep unchanged
}

function kaa() {
  ns=`k config get-contexts --no-headers | awk '{print $NF}'`
  ka ./${ns}.yml
}

function test-cronjob() {
  k create job --from=cronjob/$1 "${1}-$(date +%s)"
}

# set kubenetes namespace
function kns {
  if [[ "$1" == "" ]]; then
    k get namespaces
  else
    k config set-context $(k config current-context) --namespace=$1
  fi
}

# access a pod
function kbind {
  if [[ "$2" == "" ]]; then
    k exec -it $1 -- /bin/bash
  else
    podname="$1"
    shift 1
    k exec -it $podname -- $@
  fi
}

# access any app pod
function bind-app() {
  pod=`kubectl get pods | grep -- '-app-' | awk '{print $1}' | head -n1`
  if [[ "$pod" != "" ]]; then
    kbind $pod
  fi
}

# access psql console of db in current namespace
function bind-db() {
  dbpod=`kubectl get pods | grep -- '-db-' | awk '{print $1}'`
  if [[ "$dbpod" != "" ]]; then
    app=`k config get-contexts --no-headers | awk '{print $NF}' | sed -e 's/2$//'`
    kubectl exec -it $dbpod -- psql -U $app -P expanded=auto
  else
    echo "DB not found"
  fi
}

# export db in current namespace in a .sql file
function dump-db() {
  dbpod=`kubectl get pods | grep -- '-db-' | awk '{print $1}'`
  if [[ "$dbpod" == "" ]]; then
    echo "DB not found"
    return
  fi
  app=`k config get-contexts --no-headers | awk '{print $NF}' | sed -e 's/2$//'`
  echo "dumping db $app to ${app}.sql"
  kbind $dbpod pg_dump -U $app $app > ${app}.sql
}

# dump data in a table only, not schema
function dump-table() {
  tbl="$1"
  if [[ "$tbl" == "" ]]; then
    echo "Usage: dump-table <table-name>"
    return
  fi
  dbpod=`kubectl get pods | grep -- '-db-' | awk '{print $1}'`
  if [[ "$dbpod" == "" ]]; then
    echo "DB not found"
    return
  fi
  app=`k config get-contexts --no-headers | awk '{print $NF}' | sed -e 's/2$//'`
  echo "dumping table $tbl of db $app to ${app}-${tbl}.sql"
  kbind $dbpod pg_dump --data-only --table $tbl -U $app $app > ${app}-${tbl}.sql
}

# reset db in current namespace to empty or auto seed a .sql file
function recreate-db() {
  if [[ ! -e "$1" && "$1" != "" ]]; then
    echo "File $1 does not exists"
    return
  fi
  dbpod=`kubectl get pods | grep -- '-db-' | awk '{print $1}'`
  if [[ "$dbpod" == "" ]]; then
    echo "DB not found"
    return
  fi
  app=`k config get-contexts --no-headers | awk '{print $NF}' | sed -e 's/2$//'`

  if [[ "$2" = "data-only" ]]; then
    echo "Import data"
    cat "$1" | k exec -it $dbpod -- psql -U $app $app
    echo "Done"
  else
    echo "Terminate sessions"
    kubectl exec -it $dbpod -- psql -U $app -c "\
      select pg_terminate_backend(pid) \
      from pg_stat_activity \
      where client_addr is not null \
      and pid <> pg_backend_pid()"
    echo "Drop DB: $app"
    kubectl exec -it $dbpod -- dropdb -U $app $app
    echo "Create DB: $app"
    kubectl exec -it $dbpod -- createdb -U $app $app
    if [[ "$1" == "" ]]; then
      echo "Done, the database is empty now and you may need to execute migration and seed data"
    else
      echo "Import data"
      cat "$1" | k exec -it $dbpod -- psql -U $app $app
      echo "Done"
    fi
  fi
}

# view logs of pods in console
function klogs {
  k logs --tail=1000 -f $@
}

function kal {
  pod=`kubectl get pods | grep -- '-app-' | awk '{print $1}' | sed "${1}q;d"`
  if [[ "$pod" != "" ]]; then
    klogs "$pod"
  else
    echo "pod #$1 not found"
  fi
}

function kapperrs {
  pod=`kubectl get pods | grep -- '-app-' | awk '{print $1}' | sed "${1}q;d"`
  if [[ "$pod" != "" ]]; then
    k logs "$pod" | grep error -A 10
  else
    echo "pod #$1 not found"
  fi
}

function ingress-logs() {
  klogs `kls -n ingress-nginx --no-headers | cut -d' ' -f1` -n ingress-nginx
}

function kdelapps {
  app=`k config get-contexts --no-headers | awk '{print $NF}' | sed -e 's/2$//'`
  echo kubectl delete pod -l app=${app}-app
  kubectl delete pod -l app=${app}-app
}

# port forward grafana to view logs in a local browser
function connect-logging() {
  kubectl get secret -n logging loki-grafana -o jsonpath="{.data.admin-password}" | base64 -d
  echo
  echo "copy this password and login http://localhost:8999 in your browser"
  kubectl port-forward service/loki-grafana -n logging --address 0.0.0.0 8999:80
}

source /etc/profile
source <(kubectl completion bash)

echo 'bashrc loaded'
